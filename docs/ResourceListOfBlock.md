# ResourceListOfBlock

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[Block]**](Block.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_block import ResourceListOfBlock

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfBlock from a JSON string
resource_list_of_block_instance = ResourceListOfBlock.from_json(json)
# print the JSON string representation of the object
print ResourceListOfBlock.to_json()

# convert the object into a dict
resource_list_of_block_dict = resource_list_of_block_instance.to_dict()
# create an instance of ResourceListOfBlock from a dict
resource_list_of_block_form_dict = resource_list_of_block.from_dict(resource_list_of_block_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


