# PagedResourceListOfDataTypeSummary

A paginated list of resource that can be returned from a request.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 
**values** | [**List[DataTypeSummary]**](DataTypeSummary.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.paged_resource_list_of_data_type_summary import PagedResourceListOfDataTypeSummary

# TODO update the JSON string below
json = "{}"
# create an instance of PagedResourceListOfDataTypeSummary from a JSON string
paged_resource_list_of_data_type_summary_instance = PagedResourceListOfDataTypeSummary.from_json(json)
# print the JSON string representation of the object
print PagedResourceListOfDataTypeSummary.to_json()

# convert the object into a dict
paged_resource_list_of_data_type_summary_dict = paged_resource_list_of_data_type_summary_instance.to_dict()
# create an instance of PagedResourceListOfDataTypeSummary from a dict
paged_resource_list_of_data_type_summary_form_dict = paged_resource_list_of_data_type_summary.from_dict(paged_resource_list_of_data_type_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


