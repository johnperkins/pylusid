# ResourceListOfPerformanceReturn

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[PerformanceReturn]**](PerformanceReturn.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_performance_return import ResourceListOfPerformanceReturn

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfPerformanceReturn from a JSON string
resource_list_of_performance_return_instance = ResourceListOfPerformanceReturn.from_json(json)
# print the JSON string representation of the object
print ResourceListOfPerformanceReturn.to_json()

# convert the object into a dict
resource_list_of_performance_return_dict = resource_list_of_performance_return_instance.to_dict()
# create an instance of ResourceListOfPerformanceReturn from a dict
resource_list_of_performance_return_form_dict = resource_list_of_performance_return.from_dict(resource_list_of_performance_return_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


