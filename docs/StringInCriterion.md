# StringInCriterion


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | The property key whose value will form the left-hand side of the operation | 
**value** | **List[str]** | The value to be compared against | 
**op** | **str** | The available values are: Equals, In | 

## Example

```python
from pylusid.models.string_in_criterion import StringInCriterion

# TODO update the JSON string below
json = "{}"
# create an instance of StringInCriterion from a JSON string
string_in_criterion_instance = StringInCriterion.from_json(json)
# print the JSON string representation of the object
print StringInCriterion.to_json()

# convert the object into a dict
string_in_criterion_dict = string_in_criterion_instance.to_dict()
# create an instance of StringInCriterion from a dict
string_in_criterion_form_dict = string_in_criterion.from_dict(string_in_criterion_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


