# IdentifierPartSchema

The schema of an contributing part of a valid LUSID resource identifier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | The typical index in the identifier in which this part appears | 
**name** | **str** | The name of the identifier part that can/should be provided for this resource type | 
**display_name** | **str** | The display name of the identifier part | 
**description** | **str** | A brief description of the point of this identifier part | 
**required** | **bool** | Whether a value is required to be provided | 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.identifier_part_schema import IdentifierPartSchema

# TODO update the JSON string below
json = "{}"
# create an instance of IdentifierPartSchema from a JSON string
identifier_part_schema_instance = IdentifierPartSchema.from_json(json)
# print the JSON string representation of the object
print IdentifierPartSchema.to_json()

# convert the object into a dict
identifier_part_schema_dict = identifier_part_schema_instance.to_dict()
# create an instance of IdentifierPartSchema from a dict
identifier_part_schema_form_dict = identifier_part_schema.from_dict(identifier_part_schema_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


