# VersionedResourceListOfA2BMovementRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | [**Version**](Version.md) |  | 
**values** | [**List[A2BMovementRecord]**](A2BMovementRecord.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.versioned_resource_list_of_a2_b_movement_record import VersionedResourceListOfA2BMovementRecord

# TODO update the JSON string below
json = "{}"
# create an instance of VersionedResourceListOfA2BMovementRecord from a JSON string
versioned_resource_list_of_a2_b_movement_record_instance = VersionedResourceListOfA2BMovementRecord.from_json(json)
# print the JSON string representation of the object
print VersionedResourceListOfA2BMovementRecord.to_json()

# convert the object into a dict
versioned_resource_list_of_a2_b_movement_record_dict = versioned_resource_list_of_a2_b_movement_record_instance.to_dict()
# create an instance of VersionedResourceListOfA2BMovementRecord from a dict
versioned_resource_list_of_a2_b_movement_record_form_dict = versioned_resource_list_of_a2_b_movement_record.from_dict(versioned_resource_list_of_a2_b_movement_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


