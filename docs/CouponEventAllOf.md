# CouponEventAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**anchor_date** | **datetime** | Date on which the coupon payment occurs. | [optional] 
**instrument_event_type** | **str** | The Type of Event. The available values are: TransitionEvent, InternalEvent, CouponEvent, OpenEvent, CloseEvent, StockSplitEvent, BondDefaultEvent, CashDividendEvent | 

## Example

```python
from pylusid.models.coupon_event_all_of import CouponEventAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of CouponEventAllOf from a JSON string
coupon_event_all_of_instance = CouponEventAllOf.from_json(json)
# print the JSON string representation of the object
print CouponEventAllOf.to_json()

# convert the object into a dict
coupon_event_all_of_dict = coupon_event_all_of_instance.to_dict()
# create an instance of CouponEventAllOf from a dict
coupon_event_all_of_form_dict = coupon_event_all_of.from_dict(coupon_event_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


