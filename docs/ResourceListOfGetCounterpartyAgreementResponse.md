# ResourceListOfGetCounterpartyAgreementResponse

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[GetCounterpartyAgreementResponse]**](GetCounterpartyAgreementResponse.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_get_counterparty_agreement_response import ResourceListOfGetCounterpartyAgreementResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfGetCounterpartyAgreementResponse from a JSON string
resource_list_of_get_counterparty_agreement_response_instance = ResourceListOfGetCounterpartyAgreementResponse.from_json(json)
# print the JSON string representation of the object
print ResourceListOfGetCounterpartyAgreementResponse.to_json()

# convert the object into a dict
resource_list_of_get_counterparty_agreement_response_dict = resource_list_of_get_counterparty_agreement_response_instance.to_dict()
# create an instance of ResourceListOfGetCounterpartyAgreementResponse from a dict
resource_list_of_get_counterparty_agreement_response_form_dict = resource_list_of_get_counterparty_agreement_response.from_dict(resource_list_of_get_counterparty_agreement_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


