# ResourceListOfGetIndexConventionResponse

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[GetIndexConventionResponse]**](GetIndexConventionResponse.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_get_index_convention_response import ResourceListOfGetIndexConventionResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfGetIndexConventionResponse from a JSON string
resource_list_of_get_index_convention_response_instance = ResourceListOfGetIndexConventionResponse.from_json(json)
# print the JSON string representation of the object
print ResourceListOfGetIndexConventionResponse.to_json()

# convert the object into a dict
resource_list_of_get_index_convention_response_dict = resource_list_of_get_index_convention_response_instance.to_dict()
# create an instance of ResourceListOfGetIndexConventionResponse from a dict
resource_list_of_get_index_convention_response_form_dict = resource_list_of_get_index_convention_response.from_dict(resource_list_of_get_index_convention_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


