# ResourceListOfFeeRule

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[FeeRule]**](FeeRule.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_fee_rule import ResourceListOfFeeRule

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfFeeRule from a JSON string
resource_list_of_fee_rule_instance = ResourceListOfFeeRule.from_json(json)
# print the JSON string representation of the object
print ResourceListOfFeeRule.to_json()

# convert the object into a dict
resource_list_of_fee_rule_dict = resource_list_of_fee_rule_instance.to_dict()
# create an instance of ResourceListOfFeeRule from a dict
resource_list_of_fee_rule_form_dict = resource_list_of_fee_rule.from_dict(resource_list_of_fee_rule_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


