# AccessControlledResource

A resource to which access can be controlled

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application** | **str** | The application to which this resource belongs | [optional] 
**name** | **str** | The display name of the resource | [optional] 
**description** | **str** | The description of the resource | 
**actions** | [**List[AccessControlledAction]**](AccessControlledAction.md) | The actions acceptable for this type of resource | 
**identifier_parts** | [**List[IdentifierPartSchema]**](IdentifierPartSchema.md) | The constituent parts of a valid identifier for this resource | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.access_controlled_resource import AccessControlledResource

# TODO update the JSON string below
json = "{}"
# create an instance of AccessControlledResource from a JSON string
access_controlled_resource_instance = AccessControlledResource.from_json(json)
# print the JSON string representation of the object
print AccessControlledResource.to_json()

# convert the object into a dict
access_controlled_resource_dict = access_controlled_resource_instance.to_dict()
# create an instance of AccessControlledResource from a dict
access_controlled_resource_form_dict = access_controlled_resource.from_dict(access_controlled_resource_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


