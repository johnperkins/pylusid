# PagedResourceListOfPortfolioGroupSearchResult

A paginated list of resource that can be returned from a request.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 
**values** | [**List[PortfolioGroupSearchResult]**](PortfolioGroupSearchResult.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.paged_resource_list_of_portfolio_group_search_result import PagedResourceListOfPortfolioGroupSearchResult

# TODO update the JSON string below
json = "{}"
# create an instance of PagedResourceListOfPortfolioGroupSearchResult from a JSON string
paged_resource_list_of_portfolio_group_search_result_instance = PagedResourceListOfPortfolioGroupSearchResult.from_json(json)
# print the JSON string representation of the object
print PagedResourceListOfPortfolioGroupSearchResult.to_json()

# convert the object into a dict
paged_resource_list_of_portfolio_group_search_result_dict = paged_resource_list_of_portfolio_group_search_result_instance.to_dict()
# create an instance of PagedResourceListOfPortfolioGroupSearchResult from a dict
paged_resource_list_of_portfolio_group_search_result_form_dict = paged_resource_list_of_portfolio_group_search_result.from_dict(paged_resource_list_of_portfolio_group_search_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


