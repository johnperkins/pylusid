# StringEqualsCriterion


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | The property key whose value will form the left-hand side of the operation | 
**value** | **str** | The value to be compared against | 
**op** | **str** | The available values are: Equals, In | 

## Example

```python
from pylusid.models.string_equals_criterion import StringEqualsCriterion

# TODO update the JSON string below
json = "{}"
# create an instance of StringEqualsCriterion from a JSON string
string_equals_criterion_instance = StringEqualsCriterion.from_json(json)
# print the JSON string representation of the object
print StringEqualsCriterion.to_json()

# convert the object into a dict
string_equals_criterion_dict = string_equals_criterion_instance.to_dict()
# create an instance of StringEqualsCriterion from a dict
string_equals_criterion_form_dict = string_equals_criterion.from_dict(string_equals_criterion_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


