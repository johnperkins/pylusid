# PagedResourceListOfSequenceDefinition

A paginated list of resource that can be returned from a request.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 
**values** | [**List[SequenceDefinition]**](SequenceDefinition.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.paged_resource_list_of_sequence_definition import PagedResourceListOfSequenceDefinition

# TODO update the JSON string below
json = "{}"
# create an instance of PagedResourceListOfSequenceDefinition from a JSON string
paged_resource_list_of_sequence_definition_instance = PagedResourceListOfSequenceDefinition.from_json(json)
# print the JSON string representation of the object
print PagedResourceListOfSequenceDefinition.to_json()

# convert the object into a dict
paged_resource_list_of_sequence_definition_dict = paged_resource_list_of_sequence_definition_instance.to_dict()
# create an instance of PagedResourceListOfSequenceDefinition from a dict
paged_resource_list_of_sequence_definition_form_dict = paged_resource_list_of_sequence_definition.from_dict(paged_resource_list_of_sequence_definition_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


