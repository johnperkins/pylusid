# ResourceListOfString

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | **List[str]** | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_string import ResourceListOfString

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfString from a JSON string
resource_list_of_string_instance = ResourceListOfString.from_json(json)
# print the JSON string representation of the object
print ResourceListOfString.to_json()

# convert the object into a dict
resource_list_of_string_dict = resource_list_of_string_instance.to_dict()
# create an instance of ResourceListOfString from a dict
resource_list_of_string_form_dict = resource_list_of_string.from_dict(resource_list_of_string_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


