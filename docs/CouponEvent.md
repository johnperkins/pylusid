# CouponEvent

An unfilled cashflow event. Derived from the economic definition of an instrument.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**anchor_date** | **datetime** | Date on which the coupon payment occurs. | [optional] 
**instrument_event_type** | **str** | The Type of Event. The available values are: TransitionEvent, InternalEvent, CouponEvent, OpenEvent, CloseEvent, StockSplitEvent, BondDefaultEvent, CashDividendEvent | 

## Example

```python
from pylusid.models.coupon_event import CouponEvent

# TODO update the JSON string below
json = "{}"
# create an instance of CouponEvent from a JSON string
coupon_event_instance = CouponEvent.from_json(json)
# print the JSON string representation of the object
print CouponEvent.to_json()

# convert the object into a dict
coupon_event_dict = coupon_event_instance.to_dict()
# create an instance of CouponEvent from a dict
coupon_event_form_dict = coupon_event.from_dict(coupon_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


