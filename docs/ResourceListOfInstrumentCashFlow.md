# ResourceListOfInstrumentCashFlow

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[InstrumentCashFlow]**](InstrumentCashFlow.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_instrument_cash_flow import ResourceListOfInstrumentCashFlow

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfInstrumentCashFlow from a JSON string
resource_list_of_instrument_cash_flow_instance = ResourceListOfInstrumentCashFlow.from_json(json)
# print the JSON string representation of the object
print ResourceListOfInstrumentCashFlow.to_json()

# convert the object into a dict
resource_list_of_instrument_cash_flow_dict = resource_list_of_instrument_cash_flow_instance.to_dict()
# create an instance of ResourceListOfInstrumentCashFlow from a dict
resource_list_of_instrument_cash_flow_form_dict = resource_list_of_instrument_cash_flow.from_dict(resource_list_of_instrument_cash_flow_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


