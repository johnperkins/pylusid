# ResourceListOfComplianceRuleResult

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[ComplianceRuleResult]**](ComplianceRuleResult.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_compliance_rule_result import ResourceListOfComplianceRuleResult

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfComplianceRuleResult from a JSON string
resource_list_of_compliance_rule_result_instance = ResourceListOfComplianceRuleResult.from_json(json)
# print the JSON string representation of the object
print ResourceListOfComplianceRuleResult.to_json()

# convert the object into a dict
resource_list_of_compliance_rule_result_dict = resource_list_of_compliance_rule_result_instance.to_dict()
# create an instance of ResourceListOfComplianceRuleResult from a dict
resource_list_of_compliance_rule_result_form_dict = resource_list_of_compliance_rule_result.from_dict(resource_list_of_compliance_rule_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


