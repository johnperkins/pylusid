# ResourceListOfChangeHistory

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[ChangeHistory]**](ChangeHistory.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_change_history import ResourceListOfChangeHistory

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfChangeHistory from a JSON string
resource_list_of_change_history_instance = ResourceListOfChangeHistory.from_json(json)
# print the JSON string representation of the object
print ResourceListOfChangeHistory.to_json()

# convert the object into a dict
resource_list_of_change_history_dict = resource_list_of_change_history_instance.to_dict()
# create an instance of ResourceListOfChangeHistory from a dict
resource_list_of_change_history_form_dict = resource_list_of_change_history.from_dict(resource_list_of_change_history_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


