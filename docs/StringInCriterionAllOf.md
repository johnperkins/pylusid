# StringInCriterionAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | The property key whose value will form the left-hand side of the operation | 
**value** | **List[str]** | The value to be compared against | 
**op** | **str** | The available values are: Equals, In | 

## Example

```python
from pylusid.models.string_in_criterion_all_of import StringInCriterionAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of StringInCriterionAllOf from a JSON string
string_in_criterion_all_of_instance = StringInCriterionAllOf.from_json(json)
# print the JSON string representation of the object
print StringInCriterionAllOf.to_json()

# convert the object into a dict
string_in_criterion_all_of_dict = string_in_criterion_all_of_instance.to_dict()
# create an instance of StringInCriterionAllOf from a dict
string_in_criterion_all_of_form_dict = string_in_criterion_all_of.from_dict(string_in_criterion_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


