# ResourceListOfCalendarDate

A collection of resources that can be returned from requests.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List[CalendarDate]**](CalendarDate.md) | The resources to list. | 
**href** | **str** | The URI of the resource list. | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 
**next_page** | **str** | The next page of results. | [optional] 
**previous_page** | **str** | The previous page of results. | [optional] 

## Example

```python
from pylusid.models.resource_list_of_calendar_date import ResourceListOfCalendarDate

# TODO update the JSON string below
json = "{}"
# create an instance of ResourceListOfCalendarDate from a JSON string
resource_list_of_calendar_date_instance = ResourceListOfCalendarDate.from_json(json)
# print the JSON string representation of the object
print ResourceListOfCalendarDate.to_json()

# convert the object into a dict
resource_list_of_calendar_date_dict = resource_list_of_calendar_date_instance.to_dict()
# create an instance of ResourceListOfCalendarDate from a dict
resource_list_of_calendar_date_form_dict = resource_list_of_calendar_date.from_dict(resource_list_of_calendar_date_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


