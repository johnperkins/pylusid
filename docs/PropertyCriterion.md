# PropertyCriterion


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**op** | **str** | The available values are: Equals, In | 

## Example

```python
from pylusid.models.property_criterion import PropertyCriterion

# TODO update the JSON string below
json = "{}"
# create an instance of PropertyCriterion from a JSON string
property_criterion_instance = PropertyCriterion.from_json(json)
# print the JSON string representation of the object
print PropertyCriterion.to_json()

# convert the object into a dict
property_criterion_dict = property_criterion_instance.to_dict()
# create an instance of PropertyCriterion from a dict
property_criterion_form_dict = property_criterion.from_dict(property_criterion_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


