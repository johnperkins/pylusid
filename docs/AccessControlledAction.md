# AccessControlledAction

An action that can be access controlled

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | The description of the action | 
**action** | [**ActionId**](ActionId.md) |  | 
**limited_set** | [**List[IdSelectorDefinition]**](IdSelectorDefinition.md) | When populated, the provided values are the limited set of resources that are allowed to be specified for  access control for this action | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.access_controlled_action import AccessControlledAction

# TODO update the JSON string below
json = "{}"
# create an instance of AccessControlledAction from a JSON string
access_controlled_action_instance = AccessControlledAction.from_json(json)
# print the JSON string representation of the object
print AccessControlledAction.to_json()

# convert the object into a dict
access_controlled_action_dict = access_controlled_action_instance.to_dict()
# create an instance of AccessControlledAction from a dict
access_controlled_action_form_dict = access_controlled_action.from_dict(access_controlled_action_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


