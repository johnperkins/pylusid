# CloseEvent

The termination of an instrument.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**anchor_date** | **datetime** | The date on which the instrument closes/matures | [optional] 
**instrument_event_type** | **str** | The Type of Event. The available values are: TransitionEvent, InternalEvent, CouponEvent, OpenEvent, CloseEvent, StockSplitEvent, BondDefaultEvent, CashDividendEvent | 

## Example

```python
from pylusid.models.close_event import CloseEvent

# TODO update the JSON string below
json = "{}"
# create an instance of CloseEvent from a JSON string
close_event_instance = CloseEvent.from_json(json)
# print the JSON string representation of the object
print CloseEvent.to_json()

# convert the object into a dict
close_event_dict = close_event_instance.to_dict()
# create an instance of CloseEvent from a dict
close_event_form_dict = close_event.from_dict(close_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


