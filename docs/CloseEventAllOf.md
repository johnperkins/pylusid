# CloseEventAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**anchor_date** | **datetime** | The date on which the instrument closes/matures | [optional] 
**instrument_event_type** | **str** | The Type of Event. The available values are: TransitionEvent, InternalEvent, CouponEvent, OpenEvent, CloseEvent, StockSplitEvent, BondDefaultEvent, CashDividendEvent | 

## Example

```python
from pylusid.models.close_event_all_of import CloseEventAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of CloseEventAllOf from a JSON string
close_event_all_of_instance = CloseEventAllOf.from_json(json)
# print the JSON string representation of the object
print CloseEventAllOf.to_json()

# convert the object into a dict
close_event_all_of_dict = close_event_all_of_instance.to_dict()
# create an instance of CloseEventAllOf from a dict
close_event_all_of_form_dict = close_event_all_of.from_dict(close_event_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


