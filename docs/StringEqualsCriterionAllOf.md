# StringEqualsCriterionAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | The property key whose value will form the left-hand side of the operation | 
**value** | **str** | The value to be compared against | 
**op** | **str** | The available values are: Equals, In | 

## Example

```python
from pylusid.models.string_equals_criterion_all_of import StringEqualsCriterionAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of StringEqualsCriterionAllOf from a JSON string
string_equals_criterion_all_of_instance = StringEqualsCriterionAllOf.from_json(json)
# print the JSON string representation of the object
print StringEqualsCriterionAllOf.to_json()

# convert the object into a dict
string_equals_criterion_all_of_dict = string_equals_criterion_all_of_instance.to_dict()
# create an instance of StringEqualsCriterionAllOf from a dict
string_equals_criterion_all_of_form_dict = string_equals_criterion_all_of.from_dict(string_equals_criterion_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


