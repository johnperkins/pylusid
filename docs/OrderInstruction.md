# OrderInstruction

Record of an order instruction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**ResourceId**](ResourceId.md) |  | 
**properties** | [**Dict[str, PerpetualProperty]**](PerpetualProperty.md) | Client-defined properties associated with this execution. | [optional] 
**version** | [**Version**](Version.md) |  | [optional] 
**links** | [**List[Link]**](Link.md) | Collection of links. | [optional] 

## Example

```python
from pylusid.models.order_instruction import OrderInstruction

# TODO update the JSON string below
json = "{}"
# create an instance of OrderInstruction from a JSON string
order_instruction_instance = OrderInstruction.from_json(json)
# print the JSON string representation of the object
print OrderInstruction.to_json()

# convert the object into a dict
order_instruction_dict = order_instruction_instance.to_dict()
# create an instance of OrderInstruction from a dict
order_instruction_form_dict = order_instruction.from_dict(order_instruction_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


