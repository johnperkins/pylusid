# coding: utf-8

"""
    LUSID API

    # Introduction  This page documents the [LUSID APIs](https://www.lusid.com/api/swagger), which allows authorised clients to query and update their data within the LUSID platform.  SDKs to interact with the LUSID APIs are available in the following languages and frameworks:  * [C#](https://github.com/finbourne/lusid-sdk-csharp) * [Java](https://github.com/finbourne/lusid-sdk-java) * [JavaScript](https://github.com/finbourne/lusid-sdk-js) * [Python](https://github.com/finbourne/lusid-sdk-python) * [Angular](https://github.com/finbourne/lusid-sdk-angular)  The LUSID platform is made up of a number of sub-applications. You can find the API / swagger documentation by following the links in the table below.   | Application | Description | API / Swagger Documentation | | ----- | ----- | ---- | | LUSID | Open, API-first, developer-friendly investment data platform. | [Swagger](https://www.lusid.com/api/swagger/index.html) | | Web app | User-facing front end for LUSID. | [Swagger](https://www.lusid.com/app/swagger/index.html) | | Scheduler | Automated job scheduler. | [Swagger](https://www.lusid.com/scheduler2/swagger/index.html) | | Insights |Monitoring and troubleshooting service. | [Swagger](https://www.lusid.com/insights/swagger/index.html) | | Identity | Identity management for LUSID (in conjuction with Access) | [Swagger](https://www.lusid.com/identity/swagger/index.html) | | Access | Access control for LUSID (in conjunction with Identity) | [Swagger](https://www.lusid.com/access/swagger/index.html) | | Drive | Secure file repository and manager for collaboration. | [Swagger](https://www.lusid.com/drive/swagger/index.html) | | Luminesce | Data virtualisation service (query data from multiple providers, including LUSID) | [Swagger](https://www.lusid.com/honeycomb/swagger/index.html) | | Notification | Notification service. | [Swagger](https://www.lusid.com/notifications/swagger/index.html) | | Configuration | File store for secrets and other sensitive information. | [Swagger](https://www.lusid.com/configuration/swagger/index.html) |   # Error Codes  | Code|Name|Description | | ---|---|--- | | <a name=\"-10\">-10</a>|Server Configuration Error|  | | <a name=\"-1\">-1</a>|Unknown error|An unexpected error was encountered on our side. | | <a name=\"102\">102</a>|Version Not Found|  | | <a name=\"103\">103</a>|Api Rate Limit Violation|  | | <a name=\"104\">104</a>|Instrument Not Found|  | | <a name=\"105\">105</a>|Property Not Found|  | | <a name=\"106\">106</a>|Portfolio Recursion Depth|  | | <a name=\"108\">108</a>|Group Not Found|  | | <a name=\"109\">109</a>|Portfolio Not Found|  | | <a name=\"110\">110</a>|Property Schema Not Found|  | | <a name=\"111\">111</a>|Portfolio Ancestry Not Found|  | | <a name=\"112\">112</a>|Portfolio With Id Already Exists|  | | <a name=\"113\">113</a>|Orphaned Portfolio|  | | <a name=\"119\">119</a>|Missing Base Claims|  | | <a name=\"121\">121</a>|Property Not Defined|  | | <a name=\"122\">122</a>|Cannot Delete System Property|  | | <a name=\"123\">123</a>|Cannot Modify Immutable Property Field|  | | <a name=\"124\">124</a>|Property Already Exists|  | | <a name=\"125\">125</a>|Invalid Property Life Time|  | | <a name=\"126\">126</a>|Property Constraint Style Excludes Properties|  | | <a name=\"127\">127</a>|Cannot Modify Default Data Type|  | | <a name=\"128\">128</a>|Group Already Exists|  | | <a name=\"129\">129</a>|No Such Data Type|  | | <a name=\"130\">130</a>|Undefined Value For Data Type|  | | <a name=\"131\">131</a>|Unsupported Value Type Defined On Data Type|  | | <a name=\"132\">132</a>|Validation Error|  | | <a name=\"133\">133</a>|Loop Detected In Group Hierarchy|  | | <a name=\"134\">134</a>|Undefined Acceptable Values|  | | <a name=\"135\">135</a>|Sub Group Already Exists|  | | <a name=\"138\">138</a>|Price Source Not Found|  | | <a name=\"139\">139</a>|Analytic Store Not Found|  | | <a name=\"141\">141</a>|Analytic Store Already Exists|  | | <a name=\"143\">143</a>|Client Instrument Already Exists|  | | <a name=\"144\">144</a>|Duplicate In Parameter Set|  | | <a name=\"147\">147</a>|Results Not Found|  | | <a name=\"148\">148</a>|Order Field Not In Result Set|  | | <a name=\"149\">149</a>|Operation Failed|  | | <a name=\"150\">150</a>|Elastic Search Error|  | | <a name=\"151\">151</a>|Invalid Parameter Value|  | | <a name=\"153\">153</a>|Command Processing Failure|  | | <a name=\"154\">154</a>|Entity State Construction Failure|  | | <a name=\"155\">155</a>|Entity Timeline Does Not Exist|  | | <a name=\"156\">156</a>|Concurrency Conflict Failure|  | | <a name=\"157\">157</a>|Invalid Request|  | | <a name=\"158\">158</a>|Event Publish Unknown|  | | <a name=\"159\">159</a>|Event Query Failure|  | | <a name=\"160\">160</a>|Blob Did Not Exist|  | | <a name=\"162\">162</a>|Sub System Request Failure|  | | <a name=\"163\">163</a>|Sub System Configuration Failure|  | | <a name=\"165\">165</a>|Failed To Delete|  | | <a name=\"166\">166</a>|Upsert Client Instrument Failure|  | | <a name=\"167\">167</a>|Illegal As At Interval|  | | <a name=\"168\">168</a>|Illegal Bitemporal Query|  | | <a name=\"169\">169</a>|Invalid Alternate Id|  | | <a name=\"170\">170</a>|Cannot Add Source Portfolio Property Explicitly|  | | <a name=\"171\">171</a>|Entity Already Exists In Group|  | | <a name=\"173\">173</a>|Entity With Id Already Exists|  | | <a name=\"174\">174</a>|Derived Portfolio Details Do Not Exist|  | | <a name=\"175\">175</a>|Entity Not In Group|  | | <a name=\"176\">176</a>|Portfolio With Name Already Exists|  | | <a name=\"177\">177</a>|Invalid Transactions|  | | <a name=\"178\">178</a>|Reference Portfolio Not Found|  | | <a name=\"179\">179</a>|Duplicate Id|  | | <a name=\"180\">180</a>|Command Retrieval Failure|  | | <a name=\"181\">181</a>|Data Filter Application Failure|  | | <a name=\"182\">182</a>|Search Failed|  | | <a name=\"183\">183</a>|Movements Engine Configuration Key Failure|  | | <a name=\"184\">184</a>|Fx Rate Source Not Found|  | | <a name=\"185\">185</a>|Accrual Source Not Found|  | | <a name=\"186\">186</a>|Access Denied|  | | <a name=\"187\">187</a>|Invalid Identity Token|  | | <a name=\"188\">188</a>|Invalid Request Headers|  | | <a name=\"189\">189</a>|Price Not Found|  | | <a name=\"190\">190</a>|Invalid Sub Holding Keys Provided|  | | <a name=\"191\">191</a>|Duplicate Sub Holding Keys Provided|  | | <a name=\"192\">192</a>|Cut Definition Not Found|  | | <a name=\"193\">193</a>|Cut Definition Invalid|  | | <a name=\"194\">194</a>|Time Variant Property Deletion Date Unspecified|  | | <a name=\"195\">195</a>|Perpetual Property Deletion Date Specified|  | | <a name=\"196\">196</a>|Time Variant Property Upsert Date Unspecified|  | | <a name=\"197\">197</a>|Perpetual Property Upsert Date Specified|  | | <a name=\"200\">200</a>|Invalid Unit For Data Type|  | | <a name=\"201\">201</a>|Invalid Type For Data Type|  | | <a name=\"202\">202</a>|Invalid Value For Data Type|  | | <a name=\"203\">203</a>|Unit Not Defined For Data Type|  | | <a name=\"204\">204</a>|Units Not Supported On Data Type|  | | <a name=\"205\">205</a>|Cannot Specify Units On Data Type|  | | <a name=\"206\">206</a>|Unit Schema Inconsistent With Data Type|  | | <a name=\"207\">207</a>|Unit Definition Not Specified|  | | <a name=\"208\">208</a>|Duplicate Unit Definitions Specified|  | | <a name=\"209\">209</a>|Invalid Units Definition|  | | <a name=\"210\">210</a>|Invalid Instrument Identifier Unit|  | | <a name=\"211\">211</a>|Holdings Adjustment Does Not Exist|  | | <a name=\"212\">212</a>|Could Not Build Excel Url|  | | <a name=\"213\">213</a>|Could Not Get Excel Version|  | | <a name=\"214\">214</a>|Instrument By Code Not Found|  | | <a name=\"215\">215</a>|Entity Schema Does Not Exist|  | | <a name=\"216\">216</a>|Feature Not Supported On Portfolio Type|  | | <a name=\"217\">217</a>|Quote Not Found|  | | <a name=\"218\">218</a>|Invalid Quote Identifier|  | | <a name=\"219\">219</a>|Invalid Metric For Data Type|  | | <a name=\"220\">220</a>|Invalid Instrument Definition|  | | <a name=\"221\">221</a>|Instrument Upsert Failure|  | | <a name=\"222\">222</a>|Reference Portfolio Request Not Supported|  | | <a name=\"223\">223</a>|Transaction Portfolio Request Not Supported|  | | <a name=\"224\">224</a>|Invalid Property Value Assignment|  | | <a name=\"230\">230</a>|Transaction Type Not Found|  | | <a name=\"231\">231</a>|Transaction Type Duplication|  | | <a name=\"232\">232</a>|Portfolio Does Not Exist At Given Date|  | | <a name=\"233\">233</a>|Query Parser Failure|  | | <a name=\"234\">234</a>|Duplicate Constituent|  | | <a name=\"235\">235</a>|Unresolved Instrument Constituent|  | | <a name=\"236\">236</a>|Unresolved Instrument In Transition|  | | <a name=\"237\">237</a>|Missing Side Definitions|  | | <a name=\"299\">299</a>|Invalid Recipe|  | | <a name=\"300\">300</a>|Missing Recipe|  | | <a name=\"301\">301</a>|Dependencies|  | | <a name=\"304\">304</a>|Portfolio Preprocess Failure|  | | <a name=\"310\">310</a>|Valuation Engine Failure|  | | <a name=\"311\">311</a>|Task Factory Failure|  | | <a name=\"312\">312</a>|Task Evaluation Failure|  | | <a name=\"313\">313</a>|Task Generation Failure|  | | <a name=\"314\">314</a>|Engine Configuration Failure|  | | <a name=\"315\">315</a>|Model Specification Failure|  | | <a name=\"320\">320</a>|Market Data Key Failure|  | | <a name=\"321\">321</a>|Market Resolver Failure|  | | <a name=\"322\">322</a>|Market Data Failure|  | | <a name=\"330\">330</a>|Curve Failure|  | | <a name=\"331\">331</a>|Volatility Surface Failure|  | | <a name=\"332\">332</a>|Volatility Cube Failure|  | | <a name=\"350\">350</a>|Instrument Failure|  | | <a name=\"351\">351</a>|Cash Flows Failure|  | | <a name=\"352\">352</a>|Reference Data Failure|  | | <a name=\"360\">360</a>|Aggregation Failure|  | | <a name=\"361\">361</a>|Aggregation Measure Failure|  | | <a name=\"370\">370</a>|Result Retrieval Failure|  | | <a name=\"371\">371</a>|Result Processing Failure|  | | <a name=\"372\">372</a>|Vendor Result Processing Failure|  | | <a name=\"373\">373</a>|Vendor Result Mapping Failure|  | | <a name=\"374\">374</a>|Vendor Library Unauthorised|  | | <a name=\"375\">375</a>|Vendor Connectivity Error|  | | <a name=\"376\">376</a>|Vendor Interface Error|  | | <a name=\"377\">377</a>|Vendor Pricing Failure|  | | <a name=\"378\">378</a>|Vendor Translation Failure|  | | <a name=\"379\">379</a>|Vendor Key Mapping Failure|  | | <a name=\"380\">380</a>|Vendor Reflection Failure|  | | <a name=\"381\">381</a>|Vendor Process Failure|  | | <a name=\"382\">382</a>|Vendor System Failure|  | | <a name=\"390\">390</a>|Attempt To Upsert Duplicate Quotes|  | | <a name=\"391\">391</a>|Corporate Action Source Does Not Exist|  | | <a name=\"392\">392</a>|Corporate Action Source Already Exists|  | | <a name=\"393\">393</a>|Instrument Identifier Already In Use|  | | <a name=\"394\">394</a>|Properties Not Found|  | | <a name=\"395\">395</a>|Batch Operation Aborted|  | | <a name=\"400\">400</a>|Invalid Iso4217 Currency Code|  | | <a name=\"401\">401</a>|Cannot Assign Instrument Identifier To Currency|  | | <a name=\"402\">402</a>|Cannot Assign Currency Identifier To Non Currency|  | | <a name=\"403\">403</a>|Currency Instrument Cannot Be Deleted|  | | <a name=\"404\">404</a>|Currency Instrument Cannot Have Economic Definition|  | | <a name=\"405\">405</a>|Currency Instrument Cannot Have Lookthrough Portfolio|  | | <a name=\"406\">406</a>|Cannot Create Currency Instrument With Multiple Identifiers|  | | <a name=\"407\">407</a>|Specified Currency Is Undefined|  | | <a name=\"410\">410</a>|Index Does Not Exist|  | | <a name=\"411\">411</a>|Sort Field Does Not Exist|  | | <a name=\"413\">413</a>|Negative Pagination Parameters|  | | <a name=\"414\">414</a>|Invalid Search Syntax|  | | <a name=\"415\">415</a>|Filter Execution Timeout|  | | <a name=\"420\">420</a>|Side Definition Inconsistent|  | | <a name=\"450\">450</a>|Invalid Quote Access Metadata Rule|  | | <a name=\"451\">451</a>|Access Metadata Not Found|  | | <a name=\"452\">452</a>|Invalid Access Metadata Identifier|  | | <a name=\"460\">460</a>|Standard Resource Not Found|  | | <a name=\"461\">461</a>|Standard Resource Conflict|  | | <a name=\"462\">462</a>|Calendar Not Found|  | | <a name=\"463\">463</a>|Date In A Calendar Not Found|  | | <a name=\"464\">464</a>|Invalid Date Source Data|  | | <a name=\"465\">465</a>|Invalid Timezone|  | | <a name=\"601\">601</a>|Person Identifier Already In Use|  | | <a name=\"602\">602</a>|Person Not Found|  | | <a name=\"603\">603</a>|Cannot Set Identifier|  | | <a name=\"617\">617</a>|Invalid Recipe Specification In Request|  | | <a name=\"618\">618</a>|Inline Recipe Deserialisation Failure|  | | <a name=\"619\">619</a>|Identifier Types Not Set For Entity|  | | <a name=\"620\">620</a>|Cannot Delete All Client Defined Identifiers|  | | <a name=\"650\">650</a>|The Order requested was not found.|  | | <a name=\"654\">654</a>|The Allocation requested was not found.|  | | <a name=\"655\">655</a>|Cannot build the fx forward target with the given holdings.|  | | <a name=\"656\">656</a>|Group does not contain expected entities.|  | | <a name=\"665\">665</a>|Destination directory not found|  | | <a name=\"667\">667</a>|Relation definition already exists|  | | <a name=\"672\">672</a>|Could not retrieve file contents|  | | <a name=\"673\">673</a>|Missing entitlements for entities in Group|  | | <a name=\"674\">674</a>|Next Best Action not found|  | | <a name=\"676\">676</a>|Relation definition not defined|  | | <a name=\"677\">677</a>|Invalid entity identifier for relation|  | | <a name=\"681\">681</a>|Sorting by specified field not supported|One or more of the provided fields to order by were either invalid or not supported. | | <a name=\"682\">682</a>|Too many fields to sort by|The number of fields to sort the data by exceeds the number allowed by the endpoint | | <a name=\"684\">684</a>|Sequence Not Found|  | | <a name=\"685\">685</a>|Sequence Already Exists|  | | <a name=\"686\">686</a>|Non-cycling sequence has been exhausted|  | | <a name=\"687\">687</a>|Legal Entity Identifier Already In Use|  | | <a name=\"688\">688</a>|Legal Entity Not Found|  | | <a name=\"689\">689</a>|The supplied pagination token is invalid|  | | <a name=\"690\">690</a>|Property Type Is Not Supported|  | | <a name=\"691\">691</a>|Multiple Tax-lots For Currency Type Is Not Supported|  | | <a name=\"692\">692</a>|This endpoint does not support impersonation|  | | <a name=\"693\">693</a>|Entity type is not supported for Relationship|  | | <a name=\"694\">694</a>|Relationship Validation Failure|  | | <a name=\"695\">695</a>|Relationship Not Found|  | | <a name=\"697\">697</a>|Derived Property Formula No Longer Valid|  | | <a name=\"698\">698</a>|Story is not available|  | | <a name=\"703\">703</a>|Corporate Action Does Not Exist|  | | <a name=\"720\">720</a>|The provided sort and filter combination is not valid|  | | <a name=\"721\">721</a>|A2B generation failed|  | | <a name=\"722\">722</a>|Aggregated Return Calculation Failure|  | | <a name=\"723\">723</a>|Custom Entity Definition Identifier Already In Use|  | | <a name=\"724\">724</a>|Custom Entity Definition Not Found|  | | <a name=\"725\">725</a>|The Placement requested was not found.|  | | <a name=\"726\">726</a>|The Execution requested was not found.|  | | <a name=\"727\">727</a>|The Block requested was not found.|  | | <a name=\"728\">728</a>|The Participation requested was not found.|  | | <a name=\"729\">729</a>|The Package requested was not found.|  | | <a name=\"730\">730</a>|The OrderInstruction requested was not found.|  | | <a name=\"732\">732</a>|Custom Entity not found.|  | | <a name=\"733\">733</a>|Custom Entity Identifier already in use.|  | | <a name=\"735\">735</a>|Calculation Failed.|  | | <a name=\"736\">736</a>|An expected key on HttpResponse is missing.|  | | <a name=\"737\">737</a>|A required fee detail is missing.|  | | <a name=\"738\">738</a>|Zero rows were returned from Luminesce|  | | <a name=\"739\">739</a>|Provided Weekend Mask was invalid|  | | <a name=\"742\">742</a>|Custom Entity fields do not match the definition|  | | <a name=\"746\">746</a>|The provided sequence is not valid.|  | | <a name=\"751\">751</a>|The type of the Custom Entity is different than the type provided in the definition.|  | | <a name=\"752\">752</a>|Luminesce process returned an error.|  | | <a name=\"753\">753</a>|File name or content incompatible with operation.|  | | <a name=\"755\">755</a>|Schema of response from Drive is not as expected.|  | | <a name=\"757\">757</a>|Schema of response from Luminesce is not as expected.|  | | <a name=\"758\">758</a>|Luminesce timed out.|  | | <a name=\"763\">763</a>|Invalid Lusid Entity Identifier Unit|  | | <a name=\"768\">768</a>|Fee rule not found.|  | | <a name=\"769\">769</a>|Cannot update the base currency of a portfolio with transactions loaded|  | | <a name=\"771\">771</a>|Transaction configuration source not found|  | | <a name=\"774\">774</a>|Compliance rule not found.|  | | <a name=\"775\">775</a>|Fund accounting document cannot be processed.|  | | <a name=\"778\">778</a>|Unable to look up FX rate from trade ccy to portfolio ccy for some of the trades.|  | | <a name=\"782\">782</a>|The Property definition dataType is not matching the derivation formula dataType|  | | <a name=\"783\">783</a>|The Property definition domain is not supported for derived properties|  | | <a name=\"788\">788</a>|Compliance run not found failure.|  | | <a name=\"790\">790</a>|Custom Entity has missing or invalid identifiers|  | | <a name=\"791\">791</a>|Custom Entity definition already exists|  | | <a name=\"792\">792</a>|Compliance PropertyKey is missing.|  | | <a name=\"793\">793</a>|Compliance Criteria Value for matching is missing.|  | | <a name=\"795\">795</a>|Cannot delete identifier definition|  | | <a name=\"796\">796</a>|Tax rule set not found.|  | | <a name=\"797\">797</a>|A tax rule set with this id already exists.|  | | <a name=\"798\">798</a>|Multiple rule sets for the same property key are applicable.|  | | <a name=\"800\">800</a>|Can not upsert an instrument event of this type.|  | | <a name=\"801\">801</a>|The instrument event does not exist.|  | | <a name=\"802\">802</a>|The Instrument event is missing salient information.|  | | <a name=\"803\">803</a>|The Instrument event could not be processed.|  | | <a name=\"804\">804</a>|Some data requested does not follow the order graph assumptions.|  | | <a name=\"811\">811</a>|A price could not be found for an order.|  | | <a name=\"812\">812</a>|A price could not be found for an allocation.|  | | <a name=\"813\">813</a>|Chart of Accounts not found.|  | | <a name=\"814\">814</a>|Account not found.|  |   # noqa: E501

    The version of the OpenAPI document: 0.11.4956
    Contact: info@finbourne.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import re  # noqa: F401

from pydantic import validate_arguments, ValidationError
from typing_extensions import Annotated

from datetime import datetime

from pydantic import Field, StrictBool, StrictInt, StrictStr, conint, constr, validator

from typing import Dict, List, Optional

from pylusid.models.access_metadata_operation import AccessMetadataOperation
from pylusid.models.access_metadata_value import AccessMetadataValue
from pylusid.models.create_portfolio_group_request import CreatePortfolioGroupRequest
from pylusid.models.deleted_entity_response import DeletedEntityResponse
from pylusid.models.expanded_group import ExpandedGroup
from pylusid.models.model_property import ModelProperty
from pylusid.models.portfolio_group import PortfolioGroup
from pylusid.models.portfolio_group_properties import PortfolioGroupProperties
from pylusid.models.resource_id import ResourceId
from pylusid.models.resource_list_of_access_metadata_value_of import ResourceListOfAccessMetadataValueOf
from pylusid.models.resource_list_of_portfolio_group import ResourceListOfPortfolioGroup
from pylusid.models.resource_list_of_processed_command import ResourceListOfProcessedCommand
from pylusid.models.resource_list_of_property_interval import ResourceListOfPropertyInterval
from pylusid.models.resource_list_of_relation import ResourceListOfRelation
from pylusid.models.resource_list_of_relationship import ResourceListOfRelationship
from pylusid.models.transaction_query_parameters import TransactionQueryParameters
from pylusid.models.update_portfolio_group_request import UpdatePortfolioGroupRequest
from pylusid.models.upsert_portfolio_group_access_metadata_request import UpsertPortfolioGroupAccessMetadataRequest
from pylusid.models.versioned_resource_list_of_a2_b_data_record import VersionedResourceListOfA2BDataRecord
from pylusid.models.versioned_resource_list_of_output_transaction import VersionedResourceListOfOutputTransaction
from pylusid.models.versioned_resource_list_of_portfolio_holding import VersionedResourceListOfPortfolioHolding
from pylusid.models.versioned_resource_list_of_transaction import VersionedResourceListOfTransaction

from pylusid.api_client import ApiClient
from pylusid.exceptions import (  # noqa: F401
    ApiTypeError,
    ApiValueError
)


class PortfolioGroupsApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient.get_default()
        self.api_client = api_client

    @validate_arguments
    def add_portfolio_to_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to add a portfolio to.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to add a portfolio to. Together with the scope this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the portfolio will be added to the group.")] = None, resource_id : Annotated[Optional[ResourceId], Field(description="The resource identifier of the portfolio to add to the portfolio group.")] = None, **kwargs) -> PortfolioGroup:  # noqa: E501
        """[EARLY ACCESS] AddPortfolioToGroup: Add portfolio to group  # noqa: E501

        Add a single portfolio to a portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.add_portfolio_to_group(scope, code, effective_at, resource_id, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to add a portfolio to. (required)
        :type scope: str
        :param code: The code of the portfolio group to add a portfolio to. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label from which the portfolio will be added to the group.
        :type effective_at: str
        :param resource_id: The resource identifier of the portfolio to add to the portfolio group.
        :type resource_id: ResourceId
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.add_portfolio_to_group_with_http_info(scope, code, effective_at, resource_id, **kwargs)  # noqa: E501

    @validate_arguments
    def add_portfolio_to_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to add a portfolio to.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to add a portfolio to. Together with the scope this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the portfolio will be added to the group.")] = None, resource_id : Annotated[Optional[ResourceId], Field(description="The resource identifier of the portfolio to add to the portfolio group.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] AddPortfolioToGroup: Add portfolio to group  # noqa: E501

        Add a single portfolio to a portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.add_portfolio_to_group_with_http_info(scope, code, effective_at, resource_id, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to add a portfolio to. (required)
        :type scope: str
        :param code: The code of the portfolio group to add a portfolio to. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label from which the portfolio will be added to the group.
        :type effective_at: str
        :param resource_id: The resource identifier of the portfolio to add to the portfolio group.
        :type resource_id: ResourceId
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'resource_id'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method add_portfolio_to_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['resource_id']:
            _body_params = _params['resource_id']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            201: "PortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/portfolios', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def add_sub_group_to_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to add a portfolio group to.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to add a portfolio group to. Together with the scope this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the sub group will be added to the group.")] = None, resource_id : Annotated[Optional[ResourceId], Field(description="The resource identifier of the portfolio group to add to the portfolio group as a sub group.")] = None, **kwargs) -> PortfolioGroup:  # noqa: E501
        """[EARLY ACCESS] AddSubGroupToGroup: Add sub group to group  # noqa: E501

        Add a portfolio group to a portfolio group as a sub group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.add_sub_group_to_group(scope, code, effective_at, resource_id, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to add a portfolio group to. (required)
        :type scope: str
        :param code: The code of the portfolio group to add a portfolio group to. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label from which the sub group will be added to the group.
        :type effective_at: str
        :param resource_id: The resource identifier of the portfolio group to add to the portfolio group as a sub group.
        :type resource_id: ResourceId
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.add_sub_group_to_group_with_http_info(scope, code, effective_at, resource_id, **kwargs)  # noqa: E501

    @validate_arguments
    def add_sub_group_to_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to add a portfolio group to.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to add a portfolio group to. Together with the scope this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the sub group will be added to the group.")] = None, resource_id : Annotated[Optional[ResourceId], Field(description="The resource identifier of the portfolio group to add to the portfolio group as a sub group.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] AddSubGroupToGroup: Add sub group to group  # noqa: E501

        Add a portfolio group to a portfolio group as a sub group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.add_sub_group_to_group_with_http_info(scope, code, effective_at, resource_id, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to add a portfolio group to. (required)
        :type scope: str
        :param code: The code of the portfolio group to add a portfolio group to. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label from which the sub group will be added to the group.
        :type effective_at: str
        :param resource_id: The resource identifier of the portfolio group to add to the portfolio group as a sub group.
        :type resource_id: ResourceId
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'resource_id'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method add_sub_group_to_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['resource_id']:
            _body_params = _params['resource_id']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            201: "PortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/subgroups', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def build_transactions_for_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group.")], transaction_query_parameters : Annotated[TransactionQueryParameters, Field(..., description="The query queryParameters which control how the output transactions are built.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to build the transactions. Defaults to return the latest               version of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to BuildTransactions.")] = None, **kwargs) -> VersionedResourceListOfOutputTransaction:  # noqa: E501
        """BuildTransactionsForPortfolioGroup: Build transactions for transaction portfolios in a portfolio group  # noqa: E501

        Build transactions for transaction portfolios in a portfolio group over a given interval of effective time.                When the specified portfolio in a portfolio group is a derived transaction portfolio, the returned set of transactions is the  union set of all transactions of the parent (and any grandparents etc.) and the specified derived transaction portfolio itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.build_transactions_for_portfolio_group(scope, code, transaction_query_parameters, as_at, filter, property_keys, limit, page, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group. (required)
        :type code: str
        :param transaction_query_parameters: The query queryParameters which control how the output transactions are built. (required)
        :type transaction_query_parameters: TransactionQueryParameters
        :param as_at: The asAt datetime at which to build the transactions. Defaults to return the latest               version of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".
        :type property_keys: List[str]
        :param limit: When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.
        :type limit: int
        :param page: The pagination token to use to continue listing transactions from a previous call to BuildTransactions.
        :type page: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfOutputTransaction
        """
        kwargs['_return_http_data_only'] = True
        return self.build_transactions_for_portfolio_group_with_http_info(scope, code, transaction_query_parameters, as_at, filter, property_keys, limit, page, **kwargs)  # noqa: E501

    @validate_arguments
    def build_transactions_for_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group.")], transaction_query_parameters : Annotated[TransactionQueryParameters, Field(..., description="The query queryParameters which control how the output transactions are built.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to build the transactions. Defaults to return the latest               version of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to BuildTransactions.")] = None, **kwargs):  # noqa: E501
        """BuildTransactionsForPortfolioGroup: Build transactions for transaction portfolios in a portfolio group  # noqa: E501

        Build transactions for transaction portfolios in a portfolio group over a given interval of effective time.                When the specified portfolio in a portfolio group is a derived transaction portfolio, the returned set of transactions is the  union set of all transactions of the parent (and any grandparents etc.) and the specified derived transaction portfolio itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.build_transactions_for_portfolio_group_with_http_info(scope, code, transaction_query_parameters, as_at, filter, property_keys, limit, page, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group. (required)
        :type code: str
        :param transaction_query_parameters: The query queryParameters which control how the output transactions are built. (required)
        :type transaction_query_parameters: TransactionQueryParameters
        :param as_at: The asAt datetime at which to build the transactions. Defaults to return the latest               version of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".
        :type property_keys: List[str]
        :param limit: When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.
        :type limit: int
        :param page: The pagination token to use to continue listing transactions from a previous call to BuildTransactions.
        :type page: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfOutputTransaction, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'transaction_query_parameters',
            'as_at',
            'filter',
            'property_keys',
            'limit',
            'page'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method build_transactions_for_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['transaction_query_parameters']:
            _body_params = _params['transaction_query_parameters']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfOutputTransaction",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/transactions/$build', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def create_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope that the portfolio group will be created in.")], create_portfolio_group_request : Annotated[Optional[CreatePortfolioGroupRequest], Field(description="The definition and details of the portfolio group.")] = None, **kwargs) -> PortfolioGroup:  # noqa: E501
        """CreatePortfolioGroup: Create portfolio group  # noqa: E501

        Create a portfolio group in a specific scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_portfolio_group(scope, create_portfolio_group_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope that the portfolio group will be created in. (required)
        :type scope: str
        :param create_portfolio_group_request: The definition and details of the portfolio group.
        :type create_portfolio_group_request: CreatePortfolioGroupRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.create_portfolio_group_with_http_info(scope, create_portfolio_group_request, **kwargs)  # noqa: E501

    @validate_arguments
    def create_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope that the portfolio group will be created in.")], create_portfolio_group_request : Annotated[Optional[CreatePortfolioGroupRequest], Field(description="The definition and details of the portfolio group.")] = None, **kwargs):  # noqa: E501
        """CreatePortfolioGroup: Create portfolio group  # noqa: E501

        Create a portfolio group in a specific scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_portfolio_group_with_http_info(scope, create_portfolio_group_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope that the portfolio group will be created in. (required)
        :type scope: str
        :param create_portfolio_group_request: The definition and details of the portfolio group.
        :type create_portfolio_group_request: CreatePortfolioGroupRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'create_portfolio_group_request'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['create_portfolio_group_request']:
            _body_params = _params['create_portfolio_group_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            201: "PortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_group_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to delete properties from.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to delete properties from. Together with the scope this uniquely identifies the group.")], request_body : Annotated[List[StrictStr], Field(..., description="The property keys of the properties to delete. These take the format              {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\". Each property must be from the \"PortfolioGroup\" domain.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.")] = None, **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """[EARLY ACCESS] DeleteGroupProperties: Delete group properties  # noqa: E501

        Delete one or more properties from a single portfolio group. If the properties are time variant then an effective date time from which the properties  will be deleted must be specified. If the properties are perpetual then it is invalid to specify an effective date time for deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_group_properties(scope, code, request_body, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to delete properties from. (required)
        :type scope: str
        :param code: The code of the group to delete properties from. Together with the scope this uniquely identifies the group. (required)
        :type code: str
        :param request_body: The property keys of the properties to delete. These take the format              {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\". Each property must be from the \"PortfolioGroup\" domain. (required)
        :type request_body: List[str]
        :param effective_at: The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_group_properties_with_http_info(scope, code, request_body, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_group_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to delete properties from.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to delete properties from. Together with the scope this uniquely identifies the group.")], request_body : Annotated[List[StrictStr], Field(..., description="The property keys of the properties to delete. These take the format              {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\". Each property must be from the \"PortfolioGroup\" domain.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] DeleteGroupProperties: Delete group properties  # noqa: E501

        Delete one or more properties from a single portfolio group. If the properties are time variant then an effective date time from which the properties  will be deleted must be specified. If the properties are perpetual then it is invalid to specify an effective date time for deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_group_properties_with_http_info(scope, code, request_body, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to delete properties from. (required)
        :type scope: str
        :param code: The code of the group to delete properties from. Together with the scope this uniquely identifies the group. (required)
        :type code: str
        :param request_body: The property keys of the properties to delete. These take the format              {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\". Each property must be from the \"PortfolioGroup\" domain. (required)
        :type request_body: List[str]
        :param effective_at: The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'request_body',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_group_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['request_body']:
            _body_params = _params['request_body']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/properties/$delete', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_key_from_portfolio_group_access_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the Access Metadata entry to delete")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date to delete at, if this is not supplied, it will delete all data found")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """[EARLY ACCESS] DeleteKeyFromPortfolioGroupAccessMetadata: Delete a Portfolio Group Access Metadata entry  # noqa: E501

        Deletes the Portfolio Group Access Metadata entry that exactly matches the provided identifier parts.    It is important to always check to verify success (or failure).  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_key_from_portfolio_group_access_metadata(scope, code, metadata_key, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param metadata_key: Key of the Access Metadata entry to delete (required)
        :type metadata_key: str
        :param effective_at: The effective date to delete at, if this is not supplied, it will delete all data found
        :type effective_at: str
        :param effective_until: The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_key_from_portfolio_group_access_metadata_with_http_info(scope, code, metadata_key, effective_at, effective_until, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_key_from_portfolio_group_access_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the Access Metadata entry to delete")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date to delete at, if this is not supplied, it will delete all data found")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] DeleteKeyFromPortfolioGroupAccessMetadata: Delete a Portfolio Group Access Metadata entry  # noqa: E501

        Deletes the Portfolio Group Access Metadata entry that exactly matches the provided identifier parts.    It is important to always check to verify success (or failure).  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_key_from_portfolio_group_access_metadata_with_http_info(scope, code, metadata_key, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param metadata_key: Key of the Access Metadata entry to delete (required)
        :type metadata_key: str
        :param effective_at: The effective date to delete at, if this is not supplied, it will delete all data found
        :type effective_at: str
        :param effective_until: The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'metadata_key',
            'effective_at',
            'effective_until'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_key_from_portfolio_group_access_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['metadata_key']:
            _path_params['metadataKey'] = _params['metadata_key']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('effective_until') is not None:  # noqa: E501
            _query_params.append(('effectiveUntil', _params['effective_until']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/metadata/{metadataKey}', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_portfolio_from_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to remove the portfolio from.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to remove the portfolio from. Together with the scope this uniquely identifies the portfolio group.")], portfolio_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio being removed from the portfolio group.")], portfolio_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio being removed from the portfolio group. Together with the scope this uniquely identifies the portfolio to remove.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the portfolio will be removed from the portfolio group.")] = None, **kwargs) -> PortfolioGroup:  # noqa: E501
        """[EARLY ACCESS] DeletePortfolioFromGroup: Delete portfolio from group  # noqa: E501

        Remove a single portfolio from a portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_from_group(scope, code, portfolio_scope, portfolio_code, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to remove the portfolio from. (required)
        :type scope: str
        :param code: The code of the portfolio group to remove the portfolio from. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param portfolio_scope: The scope of the portfolio being removed from the portfolio group. (required)
        :type portfolio_scope: str
        :param portfolio_code: The code of the portfolio being removed from the portfolio group. Together with the scope this uniquely identifies the portfolio to remove. (required)
        :type portfolio_code: str
        :param effective_at: The effective datetime or cut label from which the portfolio will be removed from the portfolio group.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_portfolio_from_group_with_http_info(scope, code, portfolio_scope, portfolio_code, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_portfolio_from_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to remove the portfolio from.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to remove the portfolio from. Together with the scope this uniquely identifies the portfolio group.")], portfolio_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio being removed from the portfolio group.")], portfolio_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio being removed from the portfolio group. Together with the scope this uniquely identifies the portfolio to remove.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the portfolio will be removed from the portfolio group.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] DeletePortfolioFromGroup: Delete portfolio from group  # noqa: E501

        Remove a single portfolio from a portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_from_group_with_http_info(scope, code, portfolio_scope, portfolio_code, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to remove the portfolio from. (required)
        :type scope: str
        :param code: The code of the portfolio group to remove the portfolio from. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param portfolio_scope: The scope of the portfolio being removed from the portfolio group. (required)
        :type portfolio_scope: str
        :param portfolio_code: The code of the portfolio being removed from the portfolio group. Together with the scope this uniquely identifies the portfolio to remove. (required)
        :type portfolio_code: str
        :param effective_at: The effective datetime or cut label from which the portfolio will be removed from the portfolio group.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'portfolio_scope',
            'portfolio_code',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_portfolio_from_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['portfolio_scope']:
            _path_params['portfolioScope'] = _params['portfolio_scope']
        if _params['portfolio_code']:
            _path_params['portfolioCode'] = _params['portfolio_code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/portfolios/{portfolioScope}/{portfolioCode}', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to delete.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to delete. Together with the scope this uniquely identifies the portfolio group to delete.")], **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """[EARLY ACCESS] DeletePortfolioGroup: Delete portfolio group  # noqa: E501

        Delete a single portfolio group. A portfolio group can be deleted while it still contains portfolios or sub groups.  In this case any portfolios or sub groups contained in this group will not be deleted, however they will no longer be grouped together by this portfolio group.  The deletion will be valid from the portfolio group's creation datetime, ie. the portfolio group will no longer exist at any effective datetime from the asAt datetime of deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_group(scope, code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to delete. (required)
        :type scope: str
        :param code: The code of the portfolio group to delete. Together with the scope this uniquely identifies the portfolio group to delete. (required)
        :type code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_portfolio_group_with_http_info(scope, code, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to delete.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to delete. Together with the scope this uniquely identifies the portfolio group to delete.")], **kwargs):  # noqa: E501
        """[EARLY ACCESS] DeletePortfolioGroup: Delete portfolio group  # noqa: E501

        Delete a single portfolio group. A portfolio group can be deleted while it still contains portfolios or sub groups.  In this case any portfolios or sub groups contained in this group will not be deleted, however they will no longer be grouped together by this portfolio group.  The deletion will be valid from the portfolio group's creation datetime, ie. the portfolio group will no longer exist at any effective datetime from the asAt datetime of deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_group_with_http_info(scope, code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to delete. (required)
        :type scope: str
        :param code: The code of the portfolio group to delete. Together with the scope this uniquely identifies the portfolio group to delete. (required)
        :type code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_sub_group_from_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to remove the sub group from.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to remove the sub group from. Together with the scope this uniquely identifies the portfolio group.")], subgroup_scope : Annotated[Optional[StrictStr], Field(..., description="The scope of the sub group to remove from the portfolio group.")], subgroup_code : Annotated[Optional[StrictStr], Field(..., description="The code of the sub group to remove from the portfolio group. Together with the scope this uniquely identifies the sub group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the sub group will be removed from the portfolio group.")] = None, **kwargs) -> PortfolioGroup:  # noqa: E501
        """[EARLY ACCESS] DeleteSubGroupFromGroup: Delete sub group from group  # noqa: E501

        Remove a single portfolio group (sub group) from a portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_sub_group_from_group(scope, code, subgroup_scope, subgroup_code, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to remove the sub group from. (required)
        :type scope: str
        :param code: The code of the portfolio group to remove the sub group from. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param subgroup_scope: The scope of the sub group to remove from the portfolio group. (required)
        :type subgroup_scope: str
        :param subgroup_code: The code of the sub group to remove from the portfolio group. Together with the scope this uniquely identifies the sub group. (required)
        :type subgroup_code: str
        :param effective_at: The effective datetime or cut label from which the sub group will be removed from the portfolio group.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_sub_group_from_group_with_http_info(scope, code, subgroup_scope, subgroup_code, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_sub_group_from_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to remove the sub group from.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to remove the sub group from. Together with the scope this uniquely identifies the portfolio group.")], subgroup_scope : Annotated[Optional[StrictStr], Field(..., description="The scope of the sub group to remove from the portfolio group.")], subgroup_code : Annotated[Optional[StrictStr], Field(..., description="The code of the sub group to remove from the portfolio group. Together with the scope this uniquely identifies the sub group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label from which the sub group will be removed from the portfolio group.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] DeleteSubGroupFromGroup: Delete sub group from group  # noqa: E501

        Remove a single portfolio group (sub group) from a portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_sub_group_from_group_with_http_info(scope, code, subgroup_scope, subgroup_code, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to remove the sub group from. (required)
        :type scope: str
        :param code: The code of the portfolio group to remove the sub group from. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param subgroup_scope: The scope of the sub group to remove from the portfolio group. (required)
        :type subgroup_scope: str
        :param subgroup_code: The code of the sub group to remove from the portfolio group. Together with the scope this uniquely identifies the sub group. (required)
        :type subgroup_code: str
        :param effective_at: The effective datetime or cut label from which the sub group will be removed from the portfolio group.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'subgroup_scope',
            'subgroup_code',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_sub_group_from_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['subgroup_scope']:
            _path_params['subgroupScope'] = _params['subgroup_scope']
        if _params['subgroup_code']:
            _path_params['subgroupCode'] = _params['subgroup_code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/subgroups/{subgroupScope}/{subgroupCode}', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_a2_b_data_for_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to retrieve the A2B report for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio group.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs) -> VersionedResourceListOfA2BDataRecord:  # noqa: E501
        """[EARLY ACCESS] GetA2BDataForPortfolioGroup: Get A2B data for a Portfolio Group  # noqa: E501

        Get an A2B report for all Transaction Portfolios within the given portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_a2_b_data_for_portfolio_group(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to retrieve the A2B report for. (required)
        :type scope: str
        :param code: The code of the group to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio group. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param property_keys: A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".
        :type property_keys: List[str]
        :param filter: Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfA2BDataRecord
        """
        kwargs['_return_http_data_only'] = True
        return self.get_a2_b_data_for_portfolio_group_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, **kwargs)  # noqa: E501

    @validate_arguments
    def get_a2_b_data_for_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to retrieve the A2B report for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio group.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetA2BDataForPortfolioGroup: Get A2B data for a Portfolio Group  # noqa: E501

        Get an A2B report for all Transaction Portfolios within the given portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_a2_b_data_for_portfolio_group_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to retrieve the A2B report for. (required)
        :type scope: str
        :param code: The code of the group to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio group. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param property_keys: A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".
        :type property_keys: List[str]
        :param filter: Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfA2BDataRecord, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_effective_at',
            'to_effective_at',
            'as_at',
            'recipe_id_scope',
            'recipe_id_code',
            'property_keys',
            'filter'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_a2_b_data_for_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfA2BDataRecord",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/a2b', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_group_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to list the properties for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to list the properties for. Together with the scope this uniquely identifies the group.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date time or cut label at which to list the group's properties. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt date time at which to list the group's properties. Defaults to return the latest version of each property if not specified.")] = None, **kwargs) -> PortfolioGroupProperties:  # noqa: E501
        """[EARLY ACCESS] GetGroupProperties: Get group properties  # noqa: E501

        List all the properties of a single portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_group_properties(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to list the properties for. (required)
        :type scope: str
        :param code: The code of the group to list the properties for. Together with the scope this uniquely identifies the group. (required)
        :type code: str
        :param effective_at: The effective date time or cut label at which to list the group's properties. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt date time at which to list the group's properties. Defaults to return the latest version of each property if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroupProperties
        """
        kwargs['_return_http_data_only'] = True
        return self.get_group_properties_with_http_info(scope, code, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_group_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to list the properties for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to list the properties for. Together with the scope this uniquely identifies the group.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date time or cut label at which to list the group's properties. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt date time at which to list the group's properties. Defaults to return the latest version of each property if not specified.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetGroupProperties: Get group properties  # noqa: E501

        List all the properties of a single portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_group_properties_with_http_info(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to list the properties for. (required)
        :type scope: str
        :param code: The code of the group to list the properties for. Together with the scope this uniquely identifies the group. (required)
        :type code: str
        :param effective_at: The effective date time or cut label at which to list the group's properties. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt date time at which to list the group's properties. Defaults to return the latest version of each property if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroupProperties, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_group_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioGroupProperties",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/properties', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_holdings_for_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of transaction              portfolios in the portfolio group. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of transaction portfolios in the portfolio group. Defaults              to return the latest version of the holdings if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, by_taxlots : Annotated[Optional[StrictBool], Field(description="Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.")] = None, **kwargs) -> VersionedResourceListOfPortfolioHolding:  # noqa: E501
        """GetHoldingsForPortfolioGroup: Get holdings for transaction portfolios in portfolio group  # noqa: E501

        Get the holdings of transaction portfolios in specified portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings_for_portfolio_group(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of transaction              portfolios in the portfolio group. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of transaction portfolios in the portfolio group. Defaults              to return the latest version of the holdings if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param by_taxlots: Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.
        :type by_taxlots: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfPortfolioHolding
        """
        kwargs['_return_http_data_only'] = True
        return self.get_holdings_for_portfolio_group_with_http_info(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, **kwargs)  # noqa: E501

    @validate_arguments
    def get_holdings_for_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of transaction              portfolios in the portfolio group. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of transaction portfolios in the portfolio group. Defaults              to return the latest version of the holdings if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, by_taxlots : Annotated[Optional[StrictBool], Field(description="Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.")] = None, **kwargs):  # noqa: E501
        """GetHoldingsForPortfolioGroup: Get holdings for transaction portfolios in portfolio group  # noqa: E501

        Get the holdings of transaction portfolios in specified portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings_for_portfolio_group_with_http_info(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of transaction              portfolios in the portfolio group. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of transaction portfolios in the portfolio group. Defaults              to return the latest version of the holdings if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param by_taxlots: Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.
        :type by_taxlots: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfPortfolioHolding, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'property_keys',
            'by_taxlots'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_holdings_for_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('by_taxlots') is not None:  # noqa: E501
            _query_params.append(('byTaxlots', _params['by_taxlots']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfPortfolioHolding",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/holdings', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to retrieve the definition for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to retrieve the definition for. Together with the scope              this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to retrieve the portfolio group definition. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio group definition. Defaults to return              the latest version of the portfolio group definition if not specified.")] = None, **kwargs) -> PortfolioGroup:  # noqa: E501
        """GetPortfolioGroup: Get portfolio group  # noqa: E501

        Retrieve the definition of a single portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to retrieve the definition for. (required)
        :type scope: str
        :param code: The code of the portfolio group to retrieve the definition for. Together with the scope              this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the portfolio group definition. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio group definition. Defaults to return              the latest version of the portfolio group definition if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_with_http_info(scope, code, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to retrieve the definition for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to retrieve the definition for. Together with the scope              this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to retrieve the portfolio group definition. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio group definition. Defaults to return              the latest version of the portfolio group definition if not specified.")] = None, **kwargs):  # noqa: E501
        """GetPortfolioGroup: Get portfolio group  # noqa: E501

        Retrieve the definition of a single portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_with_http_info(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to retrieve the definition for. (required)
        :type scope: str
        :param code: The code of the portfolio group to retrieve the definition for. Together with the scope              this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the portfolio group definition. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio group definition. Defaults to return              the latest version of the portfolio group definition if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group_access_metadata_by_key(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the metadata entry to retrieve")], effective_at : Annotated[Optional[StrictStr], Field(description="The effectiveAt datetime at which to retrieve the access metadata")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the access metadata")] = None, **kwargs) -> List[AccessMetadataValue]:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupAccessMetadataByKey: Get an entry identified by a metadataKey in the Access Metadata of a Portfolio Group  # noqa: E501

        Get a specific Portfolio Group access metadata by specifying the corresponding identifier parts                No matching will be performed through this endpoint. To retrieve a rule, it is necessary to specify, exactly, the identifier of the rule  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_access_metadata_by_key(scope, code, metadata_key, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param metadata_key: Key of the metadata entry to retrieve (required)
        :type metadata_key: str
        :param effective_at: The effectiveAt datetime at which to retrieve the access metadata
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the access metadata
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: List[AccessMetadataValue]
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_access_metadata_by_key_with_http_info(scope, code, metadata_key, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_access_metadata_by_key_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the metadata entry to retrieve")], effective_at : Annotated[Optional[StrictStr], Field(description="The effectiveAt datetime at which to retrieve the access metadata")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the access metadata")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupAccessMetadataByKey: Get an entry identified by a metadataKey in the Access Metadata of a Portfolio Group  # noqa: E501

        Get a specific Portfolio Group access metadata by specifying the corresponding identifier parts                No matching will be performed through this endpoint. To retrieve a rule, it is necessary to specify, exactly, the identifier of the rule  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_access_metadata_by_key_with_http_info(scope, code, metadata_key, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param metadata_key: Key of the metadata entry to retrieve (required)
        :type metadata_key: str
        :param effective_at: The effectiveAt datetime at which to retrieve the access metadata
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the access metadata
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(List[AccessMetadataValue], status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'metadata_key',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group_access_metadata_by_key" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['metadata_key']:
            _path_params['metadataKey'] = _params['metadata_key']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "List[AccessMetadataValue]",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/metadata/{metadataKey}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group_commands(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to retrieve the commands for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to retrieve the commands for. Together with the scope this uniquely identifies the portfolio group.")], from_as_at : Annotated[Optional[datetime], Field(description="The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.")] = None, to_as_at : Annotated[Optional[datetime], Field(description="The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to filter on the User ID, use \"userId.id eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs) -> ResourceListOfProcessedCommand:  # noqa: E501
        """GetPortfolioGroupCommands: Get portfolio group commands  # noqa: E501

        Gets all the commands that modified a single portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_commands(scope, code, from_as_at, to_as_at, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to retrieve the commands for. (required)
        :type scope: str
        :param code: The code of the portfolio group to retrieve the commands for. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param from_as_at: The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.
        :type from_as_at: datetime
        :param to_as_at: The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.
        :type to_as_at: datetime
        :param filter: Expression to filter the result set.               For example, to filter on the User ID, use \"userId.id eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfProcessedCommand
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_commands_with_http_info(scope, code, from_as_at, to_as_at, filter, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_commands_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to retrieve the commands for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to retrieve the commands for. Together with the scope this uniquely identifies the portfolio group.")], from_as_at : Annotated[Optional[datetime], Field(description="The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.")] = None, to_as_at : Annotated[Optional[datetime], Field(description="The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to filter on the User ID, use \"userId.id eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs):  # noqa: E501
        """GetPortfolioGroupCommands: Get portfolio group commands  # noqa: E501

        Gets all the commands that modified a single portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_commands_with_http_info(scope, code, from_as_at, to_as_at, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to retrieve the commands for. (required)
        :type scope: str
        :param code: The code of the portfolio group to retrieve the commands for. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param from_as_at: The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.
        :type from_as_at: datetime
        :param to_as_at: The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.
        :type to_as_at: datetime
        :param filter: Expression to filter the result set.               For example, to filter on the User ID, use \"userId.id eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfProcessedCommand, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_as_at',
            'to_as_at',
            'filter'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group_commands" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_as_at') is not None:  # noqa: E501
            _query_params.append(('fromAsAt', _params['from_as_at']))
        if _params.get('to_as_at') is not None:  # noqa: E501
            _query_params.append(('toAsAt', _params['to_as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfProcessedCommand",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/commands', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group_expansion(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to expand.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to expand. Together with the scope this uniquely identifies the portfolio              group to expand.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to expand the portfolio group. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to expand the portfolio group. Defaults to return the latest version of each portfolio in the group if not specified.")] = None, property_filter : Annotated[Optional[List[StrictStr]], Field(description="The restricted list of property keys from the \"Portfolio\" domain which will be decorated onto each portfolio. These take the format {domain}/{scope}/{code} e.g. \"Portfolio/Manager/Id\".")] = None, **kwargs) -> ExpandedGroup:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupExpansion: Get portfolio group expansion  # noqa: E501

        List all the portfolios in a group, including all portfolios within sub groups in the group. Each portfolio will be decorated with all of its properties unless a property filter is specified.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_expansion(scope, code, effective_at, as_at, property_filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to expand. (required)
        :type scope: str
        :param code: The code of the portfolio group to expand. Together with the scope this uniquely identifies the portfolio              group to expand. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to expand the portfolio group. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to expand the portfolio group. Defaults to return the latest version of each portfolio in the group if not specified.
        :type as_at: datetime
        :param property_filter: The restricted list of property keys from the \"Portfolio\" domain which will be decorated onto each portfolio. These take the format {domain}/{scope}/{code} e.g. \"Portfolio/Manager/Id\".
        :type property_filter: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ExpandedGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_expansion_with_http_info(scope, code, effective_at, as_at, property_filter, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_expansion_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to expand.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to expand. Together with the scope this uniquely identifies the portfolio              group to expand.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to expand the portfolio group. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to expand the portfolio group. Defaults to return the latest version of each portfolio in the group if not specified.")] = None, property_filter : Annotated[Optional[List[StrictStr]], Field(description="The restricted list of property keys from the \"Portfolio\" domain which will be decorated onto each portfolio. These take the format {domain}/{scope}/{code} e.g. \"Portfolio/Manager/Id\".")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupExpansion: Get portfolio group expansion  # noqa: E501

        List all the portfolios in a group, including all portfolios within sub groups in the group. Each portfolio will be decorated with all of its properties unless a property filter is specified.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_expansion_with_http_info(scope, code, effective_at, as_at, property_filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to expand. (required)
        :type scope: str
        :param code: The code of the portfolio group to expand. Together with the scope this uniquely identifies the portfolio              group to expand. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to expand the portfolio group. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to expand the portfolio group. Defaults to return the latest version of each portfolio in the group if not specified.
        :type as_at: datetime
        :param property_filter: The restricted list of property keys from the \"Portfolio\" domain which will be decorated onto each portfolio. These take the format {domain}/{scope}/{code} e.g. \"Portfolio/Manager/Id\".
        :type property_filter: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ExpandedGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'property_filter'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group_expansion" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('property_filter') is not None:  # noqa: E501
            _query_params.append(('propertyFilter', _params['property_filter']))
            _collection_formats['propertyFilter'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ExpandedGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/expansion', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], effective_at : Annotated[Optional[StrictStr], Field(description="The effectiveAt datetime at which to retrieve the Access Metadata")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Access Metadata")] = None, **kwargs) -> Dict[str, List[AccessMetadataValue]]:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupMetadata: Get Access Metadata rules for Portfolio Group  # noqa: E501

        Pass the scope and Portfolio Group code parameters to retrieve the associated Access Metadata  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_metadata(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param effective_at: The effectiveAt datetime at which to retrieve the Access Metadata
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the Access Metadata
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: Dict[str, List[AccessMetadataValue]]
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_metadata_with_http_info(scope, code, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], effective_at : Annotated[Optional[StrictStr], Field(description="The effectiveAt datetime at which to retrieve the Access Metadata")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Access Metadata")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupMetadata: Get Access Metadata rules for Portfolio Group  # noqa: E501

        Pass the scope and Portfolio Group code parameters to retrieve the associated Access Metadata  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_metadata_with_http_info(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param effective_at: The effectiveAt datetime at which to retrieve the Access Metadata
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the Access Metadata
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(Dict[str, List[AccessMetadataValue]], status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "Dict[str, List[AccessMetadataValue]]",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/metadata', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group_property_time_series(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group. Together with the scope this uniquely identifies              the portfolio group.")], property_key : Annotated[StrictStr, Field(..., description="The property key of the property that will have its history shown. These must be in the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".              Each property must be from the \"PortfolioGroup\" domain.")], portfolio_group_effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime used to resolve the portfolio group. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio group's property history. Defaults to return the current datetime if not supplied.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing properties from a previous call to get property time series.              This value is returned from the previous call. If a pagination token is provided the filter, effectiveAt, and asAt fields              must not have changed since the original request.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the number of returned results to this many.")] = None, **kwargs) -> ResourceListOfPropertyInterval:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupPropertyTimeSeries: Get the time series of a portfolio group property  # noqa: E501

        List the complete time series of a portfolio group property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_property_time_series(scope, code, property_key, portfolio_group_effective_at, as_at, filter, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group. (required)
        :type scope: str
        :param code: The code of the group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param property_key: The property key of the property that will have its history shown. These must be in the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".              Each property must be from the \"PortfolioGroup\" domain. (required)
        :type property_key: str
        :param portfolio_group_effective_at: The effective datetime used to resolve the portfolio group. Defaults to the current LUSID system datetime if not specified.
        :type portfolio_group_effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio group's property history. Defaults to return the current datetime if not supplied.
        :type as_at: datetime
        :param filter: Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param page: The pagination token to use to continue listing properties from a previous call to get property time series.              This value is returned from the previous call. If a pagination token is provided the filter, effectiveAt, and asAt fields              must not have changed since the original request.
        :type page: str
        :param limit: When paginating, limit the number of returned results to this many.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPropertyInterval
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_property_time_series_with_http_info(scope, code, property_key, portfolio_group_effective_at, as_at, filter, page, limit, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_property_time_series_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group. Together with the scope this uniquely identifies              the portfolio group.")], property_key : Annotated[StrictStr, Field(..., description="The property key of the property that will have its history shown. These must be in the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".              Each property must be from the \"PortfolioGroup\" domain.")], portfolio_group_effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime used to resolve the portfolio group. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio group's property history. Defaults to return the current datetime if not supplied.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing properties from a previous call to get property time series.              This value is returned from the previous call. If a pagination token is provided the filter, effectiveAt, and asAt fields              must not have changed since the original request.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the number of returned results to this many.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupPropertyTimeSeries: Get the time series of a portfolio group property  # noqa: E501

        List the complete time series of a portfolio group property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_property_time_series_with_http_info(scope, code, property_key, portfolio_group_effective_at, as_at, filter, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group. (required)
        :type scope: str
        :param code: The code of the group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param property_key: The property key of the property that will have its history shown. These must be in the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".              Each property must be from the \"PortfolioGroup\" domain. (required)
        :type property_key: str
        :param portfolio_group_effective_at: The effective datetime used to resolve the portfolio group. Defaults to the current LUSID system datetime if not specified.
        :type portfolio_group_effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio group's property history. Defaults to return the current datetime if not supplied.
        :type as_at: datetime
        :param filter: Expression to filter the result set. Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param page: The pagination token to use to continue listing properties from a previous call to get property time series.              This value is returned from the previous call. If a pagination token is provided the filter, effectiveAt, and asAt fields              must not have changed since the original request.
        :type page: str
        :param limit: When paginating, limit the number of returned results to this many.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPropertyInterval, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'property_key',
            'portfolio_group_effective_at',
            'as_at',
            'filter',
            'page',
            'limit'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group_property_time_series" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('property_key') is not None:  # noqa: E501
            _query_params.append(('propertyKey', _params['property_key']))
        if _params.get('portfolio_group_effective_at') is not None:  # noqa: E501
            _query_params.append(('portfolioGroupEffectiveAt', _params['portfolio_group_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPropertyInterval",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/properties/time-series', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group_relations(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relations. Defaults to return the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the relations. Users should provide null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifiers types (as property keys) used for referencing Persons or Legal Entities. These take the format              {domain}/{scope}/{code} e.g. \"Person/CompanyDetails/Role\". They must be from the \"Person\" or \"LegalEntity\" domain.              Only identifier types stated will be used to look up relevant entities in relations. If not applicable, provide an empty array.")] = None, **kwargs) -> ResourceListOfRelation:  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioGroupRelations: Get Relations for Portfolio Group  # noqa: E501

        Get relations for the specified Portfolio Group  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_relations(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relations. Defaults to return the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the relations. Users should provide null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifiers types (as property keys) used for referencing Persons or Legal Entities. These take the format              {domain}/{scope}/{code} e.g. \"Person/CompanyDetails/Role\". They must be from the \"Person\" or \"LegalEntity\" domain.              Only identifier types stated will be used to look up relevant entities in relations. If not applicable, provide an empty array.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfRelation
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_relations_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_relations_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relations. Defaults to return the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the relations. Users should provide null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifiers types (as property keys) used for referencing Persons or Legal Entities. These take the format              {domain}/{scope}/{code} e.g. \"Person/CompanyDetails/Role\". They must be from the \"Person\" or \"LegalEntity\" domain.              Only identifier types stated will be used to look up relevant entities in relations. If not applicable, provide an empty array.")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioGroupRelations: Get Relations for Portfolio Group  # noqa: E501

        Get relations for the specified Portfolio Group  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_relations_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relations. Defaults to return the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the relations. Users should provide null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifiers types (as property keys) used for referencing Persons or Legal Entities. These take the format              {domain}/{scope}/{code} e.g. \"Person/CompanyDetails/Role\". They must be from the \"Person\" or \"LegalEntity\" domain.              Only identifier types stated will be used to look up relevant entities in relations. If not applicable, provide an empty array.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfRelation, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'identifier_types'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group_relations" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('identifier_types') is not None:  # noqa: E501
            _query_params.append(('identifierTypes', _params['identifier_types']))
            _collection_formats['identifierTypes'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfRelation",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/relations', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_group_relationships(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to retrieve relationship. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relationships. Defaults to return the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter relationships. Users should provide null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.")] = None, **kwargs) -> ResourceListOfRelationship:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupRelationships: Get Relationships for Portfolio Group  # noqa: E501

        Get relationships for the specified Portfolio Group  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_relationships(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relationship. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relationships. Defaults to return the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter relationships. Users should provide null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfRelationship
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_group_relationships_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_group_relationships_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to retrieve relationship. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relationships. Defaults to return the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter relationships. Users should provide null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioGroupRelationships: Get Relationships for Portfolio Group  # noqa: E501

        Get relationships for the specified Portfolio Group  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_group_relationships_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies              the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relationship. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relationships. Defaults to return the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter relationships. Users should provide null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfRelationship, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'identifier_types'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_group_relationships" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('identifier_types') is not None:  # noqa: E501
            _query_params.append(('identifierTypes', _params['identifier_types']))
            _collection_formats['identifierTypes'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfRelationship",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/relationships', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_transactions_for_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group.")], from_transaction_date : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the transactions.               There is no lower bound if this is not specified.")] = None, to_transaction_date : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the transactions. Defaults to return the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to GetTransactions.")] = None, show_cancelled_transactions : Annotated[Optional[StrictBool], Field(description="Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.")] = None, **kwargs) -> VersionedResourceListOfTransaction:  # noqa: E501
        """GetTransactionsForPortfolioGroup: Get transactions for transaction portfolios in a portfolio group  # noqa: E501

        Get transactions for transaction portfolios in a portfolio group over a given interval of effective time.                When the specified portfolio in a portfolio group is a derived transaction portfolio, the returned set of transactions is the  union set of all transactions of the parent (and any grandparents etc.) and the specified derived transaction portfolio itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_transactions_for_portfolio_group(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, limit, page, show_cancelled_transactions, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group. (required)
        :type code: str
        :param from_transaction_date: The lower bound effective datetime or cut label (inclusive) from which to retrieve the transactions.               There is no lower bound if this is not specified.
        :type from_transaction_date: str
        :param to_transaction_date: The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.
        :type to_transaction_date: str
        :param as_at: The asAt datetime at which to retrieve the transactions. Defaults to return the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".
        :type property_keys: List[str]
        :param limit: When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.
        :type limit: int
        :param page: The pagination token to use to continue listing transactions from a previous call to GetTransactions.
        :type page: str
        :param show_cancelled_transactions: Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.
        :type show_cancelled_transactions: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfTransaction
        """
        kwargs['_return_http_data_only'] = True
        return self.get_transactions_for_portfolio_group_with_http_info(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, limit, page, show_cancelled_transactions, **kwargs)  # noqa: E501

    @validate_arguments
    def get_transactions_for_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group.")], from_transaction_date : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the transactions.               There is no lower bound if this is not specified.")] = None, to_transaction_date : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the transactions. Defaults to return the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to GetTransactions.")] = None, show_cancelled_transactions : Annotated[Optional[StrictBool], Field(description="Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.")] = None, **kwargs):  # noqa: E501
        """GetTransactionsForPortfolioGroup: Get transactions for transaction portfolios in a portfolio group  # noqa: E501

        Get transactions for transaction portfolios in a portfolio group over a given interval of effective time.                When the specified portfolio in a portfolio group is a derived transaction portfolio, the returned set of transactions is the  union set of all transactions of the parent (and any grandparents etc.) and the specified derived transaction portfolio itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_transactions_for_portfolio_group_with_http_info(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, limit, page, show_cancelled_transactions, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group. (required)
        :type scope: str
        :param code: The code of the portfolio group. Together with the scope this uniquely identifies               the portfolio group. (required)
        :type code: str
        :param from_transaction_date: The lower bound effective datetime or cut label (inclusive) from which to retrieve the transactions.               There is no lower bound if this is not specified.
        :type from_transaction_date: str
        :param to_transaction_date: The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.
        :type to_transaction_date: str
        :param as_at: The asAt datetime at which to retrieve the transactions. Defaults to return the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to filter on the Transaction Type, use \"type eq 'Buy'\"               Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto               the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or               \"Transaction/strategy/quantsignal\".
        :type property_keys: List[str]
        :param limit: When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.
        :type limit: int
        :param page: The pagination token to use to continue listing transactions from a previous call to GetTransactions.
        :type page: str
        :param show_cancelled_transactions: Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.
        :type show_cancelled_transactions: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfTransaction, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_transaction_date',
            'to_transaction_date',
            'as_at',
            'filter',
            'property_keys',
            'limit',
            'page',
            'show_cancelled_transactions'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_transactions_for_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_transaction_date') is not None:  # noqa: E501
            _query_params.append(('fromTransactionDate', _params['from_transaction_date']))
        if _params.get('to_transaction_date') is not None:  # noqa: E501
            _query_params.append(('toTransactionDate', _params['to_transaction_date']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('show_cancelled_transactions') is not None:  # noqa: E501
            _query_params.append(('showCancelledTransactions', _params['show_cancelled_transactions']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfTransaction",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/transactions', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def list_portfolio_groups(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope to list the portfolio groups in.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to list the portfolio groups. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio groups. Defaults to return the latest version of each portfolio group if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Display Name, use \"displayName eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs) -> ResourceListOfPortfolioGroup:  # noqa: E501
        """[EARLY ACCESS] ListPortfolioGroups: List portfolio groups  # noqa: E501

        List all the portfolio groups in a single scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolio_groups(scope, effective_at, as_at, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope to list the portfolio groups in. (required)
        :type scope: str
        :param effective_at: The effective datetime or cut label at which to list the portfolio groups. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio groups. Defaults to return the latest version of each portfolio group if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Display Name, use \"displayName eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.list_portfolio_groups_with_http_info(scope, effective_at, as_at, filter, **kwargs)  # noqa: E501

    @validate_arguments
    def list_portfolio_groups_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope to list the portfolio groups in.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to list the portfolio groups. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio groups. Defaults to return the latest version of each portfolio group if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Display Name, use \"displayName eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] ListPortfolioGroups: List portfolio groups  # noqa: E501

        List all the portfolio groups in a single scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolio_groups_with_http_info(scope, effective_at, as_at, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope to list the portfolio groups in. (required)
        :type scope: str
        :param effective_at: The effective datetime or cut label at which to list the portfolio groups. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio groups. Defaults to return the latest version of each portfolio group if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Display Name, use \"displayName eq 'string'\"              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'effective_at',
            'as_at',
            'filter'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_portfolio_groups" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def patch_portfolio_group_access_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], access_metadata_operation : Annotated[List[AccessMetadataOperation], Field(..., description="The Json patch document")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will be effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs) -> Dict[str, List[AccessMetadataValue]]:  # noqa: E501
        """[EXPERIMENTAL] PatchPortfolioGroupAccessMetadata: Patch Access Metadata rules for a Portfolio Group.  # noqa: E501

        Patch Portfolio Group Access Metadata Rules in a single scope.  The behaviour is defined by the JSON Patch specification.                Currently only 'add' is a supported operation on the patch document.    Currently only valid metadata keys are supported paths on the patch document.                The response will return any affected Portfolio Group Access Metadata rules or a failure message if unsuccessful.                It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exist with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio_group_access_metadata(scope, code, access_metadata_operation, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param access_metadata_operation: The Json patch document (required)
        :type access_metadata_operation: List[AccessMetadataOperation]
        :param effective_at: The date this rule will be effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: Dict[str, List[AccessMetadataValue]]
        """
        kwargs['_return_http_data_only'] = True
        return self.patch_portfolio_group_access_metadata_with_http_info(scope, code, access_metadata_operation, effective_at, effective_until, **kwargs)  # noqa: E501

    @validate_arguments
    def patch_portfolio_group_access_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], access_metadata_operation : Annotated[List[AccessMetadataOperation], Field(..., description="The Json patch document")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will be effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] PatchPortfolioGroupAccessMetadata: Patch Access Metadata rules for a Portfolio Group.  # noqa: E501

        Patch Portfolio Group Access Metadata Rules in a single scope.  The behaviour is defined by the JSON Patch specification.                Currently only 'add' is a supported operation on the patch document.    Currently only valid metadata keys are supported paths on the patch document.                The response will return any affected Portfolio Group Access Metadata rules or a failure message if unsuccessful.                It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exist with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio_group_access_metadata_with_http_info(scope, code, access_metadata_operation, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param access_metadata_operation: The Json patch document (required)
        :type access_metadata_operation: List[AccessMetadataOperation]
        :param effective_at: The date this rule will be effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(Dict[str, List[AccessMetadataValue]], status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'access_metadata_operation',
            'effective_at',
            'effective_until'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_portfolio_group_access_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('effective_until') is not None:  # noqa: E501
            _query_params.append(('effectiveUntil', _params['effective_until']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['access_metadata_operation']:
            _body_params = _params['access_metadata_operation']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "Dict[str, List[AccessMetadataValue]]",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/metadata', 'PATCH',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def update_portfolio_group(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to update the definition for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to update the definition for. Together with the scope this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to update the definition.")] = None, update_portfolio_group_request : Annotated[Optional[UpdatePortfolioGroupRequest], Field(description="The updated portfolio group definition.")] = None, **kwargs) -> PortfolioGroup:  # noqa: E501
        """[EARLY ACCESS] UpdatePortfolioGroup: Update portfolio group  # noqa: E501

        Update the definition of a single portfolio group. Not all elements within a portfolio group definition are modifiable  due to the potential implications for data already stored against the portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.update_portfolio_group(scope, code, effective_at, update_portfolio_group_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to update the definition for. (required)
        :type scope: str
        :param code: The code of the portfolio group to update the definition for. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to update the definition.
        :type effective_at: str
        :param update_portfolio_group_request: The updated portfolio group definition.
        :type update_portfolio_group_request: UpdatePortfolioGroupRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroup
        """
        kwargs['_return_http_data_only'] = True
        return self.update_portfolio_group_with_http_info(scope, code, effective_at, update_portfolio_group_request, **kwargs)  # noqa: E501

    @validate_arguments
    def update_portfolio_group_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio group to update the definition for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio group to update the definition for. Together with the scope this uniquely identifies the portfolio group.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to update the definition.")] = None, update_portfolio_group_request : Annotated[Optional[UpdatePortfolioGroupRequest], Field(description="The updated portfolio group definition.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] UpdatePortfolioGroup: Update portfolio group  # noqa: E501

        Update the definition of a single portfolio group. Not all elements within a portfolio group definition are modifiable  due to the potential implications for data already stored against the portfolio group.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.update_portfolio_group_with_http_info(scope, code, effective_at, update_portfolio_group_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio group to update the definition for. (required)
        :type scope: str
        :param code: The code of the portfolio group to update the definition for. Together with the scope this uniquely identifies the portfolio group. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to update the definition.
        :type effective_at: str
        :param update_portfolio_group_request: The updated portfolio group definition.
        :type update_portfolio_group_request: UpdatePortfolioGroupRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroup, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'update_portfolio_group_request'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method update_portfolio_group" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['update_portfolio_group_request']:
            _body_params = _params['update_portfolio_group_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioGroup",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}', 'PUT',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_group_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to update or insert the properties onto.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to update or insert the properties onto. Together with the scope this uniquely identifies the group.")], request_body : Annotated[Optional[Dict[str, ModelProperty]], Field(description="The properties to be updated or inserted onto the group. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".")] = None, **kwargs) -> PortfolioGroupProperties:  # noqa: E501
        """[EARLY ACCESS] UpsertGroupProperties: Upsert group properties  # noqa: E501

        Update or insert one or more properties onto a single group. A property will be updated if it  already exists and inserted if it does not. All properties must be of the domain 'PortfolioGroup'.                Upserting a property that exists for a group, with a null value, will delete the instance of the property for that group.                Properties have an <i>effectiveFrom</i> datetime for which the property is valid, and an <i>effectiveUntil</i>  datetime until which the property is valid. Not supplying an <i>effectiveUntil</i> datetime results in the property being  valid indefinitely, or until the next <i>effectiveFrom</i> datetime of the property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_group_properties(scope, code, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to update or insert the properties onto. (required)
        :type scope: str
        :param code: The code of the group to update or insert the properties onto. Together with the scope this uniquely identifies the group. (required)
        :type code: str
        :param request_body: The properties to be updated or inserted onto the group. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".
        :type request_body: Dict[str, ModelProperty]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioGroupProperties
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_group_properties_with_http_info(scope, code, request_body, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_group_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the group to update or insert the properties onto.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the group to update or insert the properties onto. Together with the scope this uniquely identifies the group.")], request_body : Annotated[Optional[Dict[str, ModelProperty]], Field(description="The properties to be updated or inserted onto the group. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] UpsertGroupProperties: Upsert group properties  # noqa: E501

        Update or insert one or more properties onto a single group. A property will be updated if it  already exists and inserted if it does not. All properties must be of the domain 'PortfolioGroup'.                Upserting a property that exists for a group, with a null value, will delete the instance of the property for that group.                Properties have an <i>effectiveFrom</i> datetime for which the property is valid, and an <i>effectiveUntil</i>  datetime until which the property is valid. Not supplying an <i>effectiveUntil</i> datetime results in the property being  valid indefinitely, or until the next <i>effectiveFrom</i> datetime of the property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_group_properties_with_http_info(scope, code, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the group to update or insert the properties onto. (required)
        :type scope: str
        :param code: The code of the group to update or insert the properties onto. Together with the scope this uniquely identifies the group. (required)
        :type code: str
        :param request_body: The properties to be updated or inserted onto the group. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code} e.g. \"PortfolioGroup/Manager/Id\".
        :type request_body: Dict[str, ModelProperty]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioGroupProperties, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'request_body'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_group_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['request_body']:
            _body_params = _params['request_body']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioGroupProperties",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/properties/$upsert', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_portfolio_group_access_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the access metadata entry to upsert")], upsert_portfolio_group_access_metadata_request : Annotated[UpsertPortfolioGroupAccessMetadataRequest, Field(..., description="The Portfolio Group Access Metadata rule to upsert")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will be effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs) -> ResourceListOfAccessMetadataValueOf:  # noqa: E501
        """[EARLY ACCESS] UpsertPortfolioGroupAccessMetadata: Upsert a Portfolio Group Access Metadata entry associated with a specific metadataKey. This creates or updates the data in LUSID.  # noqa: E501

        Update or insert one Portfolio Group Access Metadata Entry in a single scope. An item will be updated if it already exists  and inserted if it does not.                The response will return the successfully updated or inserted Portfolio Group Access Metadata rule or failure message if unsuccessful.                It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exist with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_group_access_metadata(scope, code, metadata_key, upsert_portfolio_group_access_metadata_request, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param metadata_key: Key of the access metadata entry to upsert (required)
        :type metadata_key: str
        :param upsert_portfolio_group_access_metadata_request: The Portfolio Group Access Metadata rule to upsert (required)
        :type upsert_portfolio_group_access_metadata_request: UpsertPortfolioGroupAccessMetadataRequest
        :param effective_at: The date this rule will be effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfAccessMetadataValueOf
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_portfolio_group_access_metadata_with_http_info(scope, code, metadata_key, upsert_portfolio_group_access_metadata_request, effective_at, effective_until, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_portfolio_group_access_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Group")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The Portfolio Group code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the access metadata entry to upsert")], upsert_portfolio_group_access_metadata_request : Annotated[UpsertPortfolioGroupAccessMetadataRequest, Field(..., description="The Portfolio Group Access Metadata rule to upsert")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will be effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] UpsertPortfolioGroupAccessMetadata: Upsert a Portfolio Group Access Metadata entry associated with a specific metadataKey. This creates or updates the data in LUSID.  # noqa: E501

        Update or insert one Portfolio Group Access Metadata Entry in a single scope. An item will be updated if it already exists  and inserted if it does not.                The response will return the successfully updated or inserted Portfolio Group Access Metadata rule or failure message if unsuccessful.                It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exist with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_group_access_metadata_with_http_info(scope, code, metadata_key, upsert_portfolio_group_access_metadata_request, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Group (required)
        :type scope: str
        :param code: The Portfolio Group code (required)
        :type code: str
        :param metadata_key: Key of the access metadata entry to upsert (required)
        :type metadata_key: str
        :param upsert_portfolio_group_access_metadata_request: The Portfolio Group Access Metadata rule to upsert (required)
        :type upsert_portfolio_group_access_metadata_request: UpsertPortfolioGroupAccessMetadataRequest
        :param effective_at: The date this rule will be effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfAccessMetadataValueOf, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'metadata_key',
            'upsert_portfolio_group_access_metadata_request',
            'effective_at',
            'effective_until'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_portfolio_group_access_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['metadata_key']:
            _path_params['metadataKey'] = _params['metadata_key']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('effective_until') is not None:  # noqa: E501
            _query_params.append(('effectiveUntil', _params['effective_until']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['upsert_portfolio_group_access_metadata_request']:
            _body_params = _params['upsert_portfolio_group_access_metadata_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfAccessMetadataValueOf",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfoliogroups/{scope}/{code}/metadata/{metadataKey}', 'PUT',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))
