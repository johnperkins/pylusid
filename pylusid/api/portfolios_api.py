# coding: utf-8

"""
    LUSID API

    # Introduction  This page documents the [LUSID APIs](https://www.lusid.com/api/swagger), which allows authorised clients to query and update their data within the LUSID platform.  SDKs to interact with the LUSID APIs are available in the following languages and frameworks:  * [C#](https://github.com/finbourne/lusid-sdk-csharp) * [Java](https://github.com/finbourne/lusid-sdk-java) * [JavaScript](https://github.com/finbourne/lusid-sdk-js) * [Python](https://github.com/finbourne/lusid-sdk-python) * [Angular](https://github.com/finbourne/lusid-sdk-angular)  The LUSID platform is made up of a number of sub-applications. You can find the API / swagger documentation by following the links in the table below.   | Application | Description | API / Swagger Documentation | | ----- | ----- | ---- | | LUSID | Open, API-first, developer-friendly investment data platform. | [Swagger](https://www.lusid.com/api/swagger/index.html) | | Web app | User-facing front end for LUSID. | [Swagger](https://www.lusid.com/app/swagger/index.html) | | Scheduler | Automated job scheduler. | [Swagger](https://www.lusid.com/scheduler2/swagger/index.html) | | Insights |Monitoring and troubleshooting service. | [Swagger](https://www.lusid.com/insights/swagger/index.html) | | Identity | Identity management for LUSID (in conjuction with Access) | [Swagger](https://www.lusid.com/identity/swagger/index.html) | | Access | Access control for LUSID (in conjunction with Identity) | [Swagger](https://www.lusid.com/access/swagger/index.html) | | Drive | Secure file repository and manager for collaboration. | [Swagger](https://www.lusid.com/drive/swagger/index.html) | | Luminesce | Data virtualisation service (query data from multiple providers, including LUSID) | [Swagger](https://www.lusid.com/honeycomb/swagger/index.html) | | Notification | Notification service. | [Swagger](https://www.lusid.com/notifications/swagger/index.html) | | Configuration | File store for secrets and other sensitive information. | [Swagger](https://www.lusid.com/configuration/swagger/index.html) |   # Error Codes  | Code|Name|Description | | ---|---|--- | | <a name=\"-10\">-10</a>|Server Configuration Error|  | | <a name=\"-1\">-1</a>|Unknown error|An unexpected error was encountered on our side. | | <a name=\"102\">102</a>|Version Not Found|  | | <a name=\"103\">103</a>|Api Rate Limit Violation|  | | <a name=\"104\">104</a>|Instrument Not Found|  | | <a name=\"105\">105</a>|Property Not Found|  | | <a name=\"106\">106</a>|Portfolio Recursion Depth|  | | <a name=\"108\">108</a>|Group Not Found|  | | <a name=\"109\">109</a>|Portfolio Not Found|  | | <a name=\"110\">110</a>|Property Schema Not Found|  | | <a name=\"111\">111</a>|Portfolio Ancestry Not Found|  | | <a name=\"112\">112</a>|Portfolio With Id Already Exists|  | | <a name=\"113\">113</a>|Orphaned Portfolio|  | | <a name=\"119\">119</a>|Missing Base Claims|  | | <a name=\"121\">121</a>|Property Not Defined|  | | <a name=\"122\">122</a>|Cannot Delete System Property|  | | <a name=\"123\">123</a>|Cannot Modify Immutable Property Field|  | | <a name=\"124\">124</a>|Property Already Exists|  | | <a name=\"125\">125</a>|Invalid Property Life Time|  | | <a name=\"126\">126</a>|Property Constraint Style Excludes Properties|  | | <a name=\"127\">127</a>|Cannot Modify Default Data Type|  | | <a name=\"128\">128</a>|Group Already Exists|  | | <a name=\"129\">129</a>|No Such Data Type|  | | <a name=\"130\">130</a>|Undefined Value For Data Type|  | | <a name=\"131\">131</a>|Unsupported Value Type Defined On Data Type|  | | <a name=\"132\">132</a>|Validation Error|  | | <a name=\"133\">133</a>|Loop Detected In Group Hierarchy|  | | <a name=\"134\">134</a>|Undefined Acceptable Values|  | | <a name=\"135\">135</a>|Sub Group Already Exists|  | | <a name=\"138\">138</a>|Price Source Not Found|  | | <a name=\"139\">139</a>|Analytic Store Not Found|  | | <a name=\"141\">141</a>|Analytic Store Already Exists|  | | <a name=\"143\">143</a>|Client Instrument Already Exists|  | | <a name=\"144\">144</a>|Duplicate In Parameter Set|  | | <a name=\"147\">147</a>|Results Not Found|  | | <a name=\"148\">148</a>|Order Field Not In Result Set|  | | <a name=\"149\">149</a>|Operation Failed|  | | <a name=\"150\">150</a>|Elastic Search Error|  | | <a name=\"151\">151</a>|Invalid Parameter Value|  | | <a name=\"153\">153</a>|Command Processing Failure|  | | <a name=\"154\">154</a>|Entity State Construction Failure|  | | <a name=\"155\">155</a>|Entity Timeline Does Not Exist|  | | <a name=\"156\">156</a>|Concurrency Conflict Failure|  | | <a name=\"157\">157</a>|Invalid Request|  | | <a name=\"158\">158</a>|Event Publish Unknown|  | | <a name=\"159\">159</a>|Event Query Failure|  | | <a name=\"160\">160</a>|Blob Did Not Exist|  | | <a name=\"162\">162</a>|Sub System Request Failure|  | | <a name=\"163\">163</a>|Sub System Configuration Failure|  | | <a name=\"165\">165</a>|Failed To Delete|  | | <a name=\"166\">166</a>|Upsert Client Instrument Failure|  | | <a name=\"167\">167</a>|Illegal As At Interval|  | | <a name=\"168\">168</a>|Illegal Bitemporal Query|  | | <a name=\"169\">169</a>|Invalid Alternate Id|  | | <a name=\"170\">170</a>|Cannot Add Source Portfolio Property Explicitly|  | | <a name=\"171\">171</a>|Entity Already Exists In Group|  | | <a name=\"173\">173</a>|Entity With Id Already Exists|  | | <a name=\"174\">174</a>|Derived Portfolio Details Do Not Exist|  | | <a name=\"175\">175</a>|Entity Not In Group|  | | <a name=\"176\">176</a>|Portfolio With Name Already Exists|  | | <a name=\"177\">177</a>|Invalid Transactions|  | | <a name=\"178\">178</a>|Reference Portfolio Not Found|  | | <a name=\"179\">179</a>|Duplicate Id|  | | <a name=\"180\">180</a>|Command Retrieval Failure|  | | <a name=\"181\">181</a>|Data Filter Application Failure|  | | <a name=\"182\">182</a>|Search Failed|  | | <a name=\"183\">183</a>|Movements Engine Configuration Key Failure|  | | <a name=\"184\">184</a>|Fx Rate Source Not Found|  | | <a name=\"185\">185</a>|Accrual Source Not Found|  | | <a name=\"186\">186</a>|Access Denied|  | | <a name=\"187\">187</a>|Invalid Identity Token|  | | <a name=\"188\">188</a>|Invalid Request Headers|  | | <a name=\"189\">189</a>|Price Not Found|  | | <a name=\"190\">190</a>|Invalid Sub Holding Keys Provided|  | | <a name=\"191\">191</a>|Duplicate Sub Holding Keys Provided|  | | <a name=\"192\">192</a>|Cut Definition Not Found|  | | <a name=\"193\">193</a>|Cut Definition Invalid|  | | <a name=\"194\">194</a>|Time Variant Property Deletion Date Unspecified|  | | <a name=\"195\">195</a>|Perpetual Property Deletion Date Specified|  | | <a name=\"196\">196</a>|Time Variant Property Upsert Date Unspecified|  | | <a name=\"197\">197</a>|Perpetual Property Upsert Date Specified|  | | <a name=\"200\">200</a>|Invalid Unit For Data Type|  | | <a name=\"201\">201</a>|Invalid Type For Data Type|  | | <a name=\"202\">202</a>|Invalid Value For Data Type|  | | <a name=\"203\">203</a>|Unit Not Defined For Data Type|  | | <a name=\"204\">204</a>|Units Not Supported On Data Type|  | | <a name=\"205\">205</a>|Cannot Specify Units On Data Type|  | | <a name=\"206\">206</a>|Unit Schema Inconsistent With Data Type|  | | <a name=\"207\">207</a>|Unit Definition Not Specified|  | | <a name=\"208\">208</a>|Duplicate Unit Definitions Specified|  | | <a name=\"209\">209</a>|Invalid Units Definition|  | | <a name=\"210\">210</a>|Invalid Instrument Identifier Unit|  | | <a name=\"211\">211</a>|Holdings Adjustment Does Not Exist|  | | <a name=\"212\">212</a>|Could Not Build Excel Url|  | | <a name=\"213\">213</a>|Could Not Get Excel Version|  | | <a name=\"214\">214</a>|Instrument By Code Not Found|  | | <a name=\"215\">215</a>|Entity Schema Does Not Exist|  | | <a name=\"216\">216</a>|Feature Not Supported On Portfolio Type|  | | <a name=\"217\">217</a>|Quote Not Found|  | | <a name=\"218\">218</a>|Invalid Quote Identifier|  | | <a name=\"219\">219</a>|Invalid Metric For Data Type|  | | <a name=\"220\">220</a>|Invalid Instrument Definition|  | | <a name=\"221\">221</a>|Instrument Upsert Failure|  | | <a name=\"222\">222</a>|Reference Portfolio Request Not Supported|  | | <a name=\"223\">223</a>|Transaction Portfolio Request Not Supported|  | | <a name=\"224\">224</a>|Invalid Property Value Assignment|  | | <a name=\"230\">230</a>|Transaction Type Not Found|  | | <a name=\"231\">231</a>|Transaction Type Duplication|  | | <a name=\"232\">232</a>|Portfolio Does Not Exist At Given Date|  | | <a name=\"233\">233</a>|Query Parser Failure|  | | <a name=\"234\">234</a>|Duplicate Constituent|  | | <a name=\"235\">235</a>|Unresolved Instrument Constituent|  | | <a name=\"236\">236</a>|Unresolved Instrument In Transition|  | | <a name=\"237\">237</a>|Missing Side Definitions|  | | <a name=\"299\">299</a>|Invalid Recipe|  | | <a name=\"300\">300</a>|Missing Recipe|  | | <a name=\"301\">301</a>|Dependencies|  | | <a name=\"304\">304</a>|Portfolio Preprocess Failure|  | | <a name=\"310\">310</a>|Valuation Engine Failure|  | | <a name=\"311\">311</a>|Task Factory Failure|  | | <a name=\"312\">312</a>|Task Evaluation Failure|  | | <a name=\"313\">313</a>|Task Generation Failure|  | | <a name=\"314\">314</a>|Engine Configuration Failure|  | | <a name=\"315\">315</a>|Model Specification Failure|  | | <a name=\"320\">320</a>|Market Data Key Failure|  | | <a name=\"321\">321</a>|Market Resolver Failure|  | | <a name=\"322\">322</a>|Market Data Failure|  | | <a name=\"330\">330</a>|Curve Failure|  | | <a name=\"331\">331</a>|Volatility Surface Failure|  | | <a name=\"332\">332</a>|Volatility Cube Failure|  | | <a name=\"350\">350</a>|Instrument Failure|  | | <a name=\"351\">351</a>|Cash Flows Failure|  | | <a name=\"352\">352</a>|Reference Data Failure|  | | <a name=\"360\">360</a>|Aggregation Failure|  | | <a name=\"361\">361</a>|Aggregation Measure Failure|  | | <a name=\"370\">370</a>|Result Retrieval Failure|  | | <a name=\"371\">371</a>|Result Processing Failure|  | | <a name=\"372\">372</a>|Vendor Result Processing Failure|  | | <a name=\"373\">373</a>|Vendor Result Mapping Failure|  | | <a name=\"374\">374</a>|Vendor Library Unauthorised|  | | <a name=\"375\">375</a>|Vendor Connectivity Error|  | | <a name=\"376\">376</a>|Vendor Interface Error|  | | <a name=\"377\">377</a>|Vendor Pricing Failure|  | | <a name=\"378\">378</a>|Vendor Translation Failure|  | | <a name=\"379\">379</a>|Vendor Key Mapping Failure|  | | <a name=\"380\">380</a>|Vendor Reflection Failure|  | | <a name=\"381\">381</a>|Vendor Process Failure|  | | <a name=\"382\">382</a>|Vendor System Failure|  | | <a name=\"390\">390</a>|Attempt To Upsert Duplicate Quotes|  | | <a name=\"391\">391</a>|Corporate Action Source Does Not Exist|  | | <a name=\"392\">392</a>|Corporate Action Source Already Exists|  | | <a name=\"393\">393</a>|Instrument Identifier Already In Use|  | | <a name=\"394\">394</a>|Properties Not Found|  | | <a name=\"395\">395</a>|Batch Operation Aborted|  | | <a name=\"400\">400</a>|Invalid Iso4217 Currency Code|  | | <a name=\"401\">401</a>|Cannot Assign Instrument Identifier To Currency|  | | <a name=\"402\">402</a>|Cannot Assign Currency Identifier To Non Currency|  | | <a name=\"403\">403</a>|Currency Instrument Cannot Be Deleted|  | | <a name=\"404\">404</a>|Currency Instrument Cannot Have Economic Definition|  | | <a name=\"405\">405</a>|Currency Instrument Cannot Have Lookthrough Portfolio|  | | <a name=\"406\">406</a>|Cannot Create Currency Instrument With Multiple Identifiers|  | | <a name=\"407\">407</a>|Specified Currency Is Undefined|  | | <a name=\"410\">410</a>|Index Does Not Exist|  | | <a name=\"411\">411</a>|Sort Field Does Not Exist|  | | <a name=\"413\">413</a>|Negative Pagination Parameters|  | | <a name=\"414\">414</a>|Invalid Search Syntax|  | | <a name=\"415\">415</a>|Filter Execution Timeout|  | | <a name=\"420\">420</a>|Side Definition Inconsistent|  | | <a name=\"450\">450</a>|Invalid Quote Access Metadata Rule|  | | <a name=\"451\">451</a>|Access Metadata Not Found|  | | <a name=\"452\">452</a>|Invalid Access Metadata Identifier|  | | <a name=\"460\">460</a>|Standard Resource Not Found|  | | <a name=\"461\">461</a>|Standard Resource Conflict|  | | <a name=\"462\">462</a>|Calendar Not Found|  | | <a name=\"463\">463</a>|Date In A Calendar Not Found|  | | <a name=\"464\">464</a>|Invalid Date Source Data|  | | <a name=\"465\">465</a>|Invalid Timezone|  | | <a name=\"601\">601</a>|Person Identifier Already In Use|  | | <a name=\"602\">602</a>|Person Not Found|  | | <a name=\"603\">603</a>|Cannot Set Identifier|  | | <a name=\"617\">617</a>|Invalid Recipe Specification In Request|  | | <a name=\"618\">618</a>|Inline Recipe Deserialisation Failure|  | | <a name=\"619\">619</a>|Identifier Types Not Set For Entity|  | | <a name=\"620\">620</a>|Cannot Delete All Client Defined Identifiers|  | | <a name=\"650\">650</a>|The Order requested was not found.|  | | <a name=\"654\">654</a>|The Allocation requested was not found.|  | | <a name=\"655\">655</a>|Cannot build the fx forward target with the given holdings.|  | | <a name=\"656\">656</a>|Group does not contain expected entities.|  | | <a name=\"665\">665</a>|Destination directory not found|  | | <a name=\"667\">667</a>|Relation definition already exists|  | | <a name=\"672\">672</a>|Could not retrieve file contents|  | | <a name=\"673\">673</a>|Missing entitlements for entities in Group|  | | <a name=\"674\">674</a>|Next Best Action not found|  | | <a name=\"676\">676</a>|Relation definition not defined|  | | <a name=\"677\">677</a>|Invalid entity identifier for relation|  | | <a name=\"681\">681</a>|Sorting by specified field not supported|One or more of the provided fields to order by were either invalid or not supported. | | <a name=\"682\">682</a>|Too many fields to sort by|The number of fields to sort the data by exceeds the number allowed by the endpoint | | <a name=\"684\">684</a>|Sequence Not Found|  | | <a name=\"685\">685</a>|Sequence Already Exists|  | | <a name=\"686\">686</a>|Non-cycling sequence has been exhausted|  | | <a name=\"687\">687</a>|Legal Entity Identifier Already In Use|  | | <a name=\"688\">688</a>|Legal Entity Not Found|  | | <a name=\"689\">689</a>|The supplied pagination token is invalid|  | | <a name=\"690\">690</a>|Property Type Is Not Supported|  | | <a name=\"691\">691</a>|Multiple Tax-lots For Currency Type Is Not Supported|  | | <a name=\"692\">692</a>|This endpoint does not support impersonation|  | | <a name=\"693\">693</a>|Entity type is not supported for Relationship|  | | <a name=\"694\">694</a>|Relationship Validation Failure|  | | <a name=\"695\">695</a>|Relationship Not Found|  | | <a name=\"697\">697</a>|Derived Property Formula No Longer Valid|  | | <a name=\"698\">698</a>|Story is not available|  | | <a name=\"703\">703</a>|Corporate Action Does Not Exist|  | | <a name=\"720\">720</a>|The provided sort and filter combination is not valid|  | | <a name=\"721\">721</a>|A2B generation failed|  | | <a name=\"722\">722</a>|Aggregated Return Calculation Failure|  | | <a name=\"723\">723</a>|Custom Entity Definition Identifier Already In Use|  | | <a name=\"724\">724</a>|Custom Entity Definition Not Found|  | | <a name=\"725\">725</a>|The Placement requested was not found.|  | | <a name=\"726\">726</a>|The Execution requested was not found.|  | | <a name=\"727\">727</a>|The Block requested was not found.|  | | <a name=\"728\">728</a>|The Participation requested was not found.|  | | <a name=\"729\">729</a>|The Package requested was not found.|  | | <a name=\"730\">730</a>|The OrderInstruction requested was not found.|  | | <a name=\"732\">732</a>|Custom Entity not found.|  | | <a name=\"733\">733</a>|Custom Entity Identifier already in use.|  | | <a name=\"735\">735</a>|Calculation Failed.|  | | <a name=\"736\">736</a>|An expected key on HttpResponse is missing.|  | | <a name=\"737\">737</a>|A required fee detail is missing.|  | | <a name=\"738\">738</a>|Zero rows were returned from Luminesce|  | | <a name=\"739\">739</a>|Provided Weekend Mask was invalid|  | | <a name=\"742\">742</a>|Custom Entity fields do not match the definition|  | | <a name=\"746\">746</a>|The provided sequence is not valid.|  | | <a name=\"751\">751</a>|The type of the Custom Entity is different than the type provided in the definition.|  | | <a name=\"752\">752</a>|Luminesce process returned an error.|  | | <a name=\"753\">753</a>|File name or content incompatible with operation.|  | | <a name=\"755\">755</a>|Schema of response from Drive is not as expected.|  | | <a name=\"757\">757</a>|Schema of response from Luminesce is not as expected.|  | | <a name=\"758\">758</a>|Luminesce timed out.|  | | <a name=\"763\">763</a>|Invalid Lusid Entity Identifier Unit|  | | <a name=\"768\">768</a>|Fee rule not found.|  | | <a name=\"769\">769</a>|Cannot update the base currency of a portfolio with transactions loaded|  | | <a name=\"771\">771</a>|Transaction configuration source not found|  | | <a name=\"774\">774</a>|Compliance rule not found.|  | | <a name=\"775\">775</a>|Fund accounting document cannot be processed.|  | | <a name=\"778\">778</a>|Unable to look up FX rate from trade ccy to portfolio ccy for some of the trades.|  | | <a name=\"782\">782</a>|The Property definition dataType is not matching the derivation formula dataType|  | | <a name=\"783\">783</a>|The Property definition domain is not supported for derived properties|  | | <a name=\"788\">788</a>|Compliance run not found failure.|  | | <a name=\"790\">790</a>|Custom Entity has missing or invalid identifiers|  | | <a name=\"791\">791</a>|Custom Entity definition already exists|  | | <a name=\"792\">792</a>|Compliance PropertyKey is missing.|  | | <a name=\"793\">793</a>|Compliance Criteria Value for matching is missing.|  | | <a name=\"795\">795</a>|Cannot delete identifier definition|  | | <a name=\"796\">796</a>|Tax rule set not found.|  | | <a name=\"797\">797</a>|A tax rule set with this id already exists.|  | | <a name=\"798\">798</a>|Multiple rule sets for the same property key are applicable.|  | | <a name=\"800\">800</a>|Can not upsert an instrument event of this type.|  | | <a name=\"801\">801</a>|The instrument event does not exist.|  | | <a name=\"802\">802</a>|The Instrument event is missing salient information.|  | | <a name=\"803\">803</a>|The Instrument event could not be processed.|  | | <a name=\"804\">804</a>|Some data requested does not follow the order graph assumptions.|  | | <a name=\"811\">811</a>|A price could not be found for an order.|  | | <a name=\"812\">812</a>|A price could not be found for an allocation.|  | | <a name=\"813\">813</a>|Chart of Accounts not found.|  | | <a name=\"814\">814</a>|Account not found.|  |   # noqa: E501

    The version of the OpenAPI document: 0.11.4956
    Contact: info@finbourne.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import re  # noqa: F401

from pydantic import validate_arguments, ValidationError
from typing_extensions import Annotated

from datetime import datetime

from pydantic import Field, StrictInt, StrictStr, conint, constr, validator

from typing import Dict, List, Optional

from pylusid.models.access_metadata_operation import AccessMetadataOperation
from pylusid.models.access_metadata_value import AccessMetadataValue
from pylusid.models.action_result_of_portfolio import ActionResultOfPortfolio
from pylusid.models.aggregated_returns_request import AggregatedReturnsRequest
from pylusid.models.aggregated_returns_response import AggregatedReturnsResponse
from pylusid.models.deleted_entity_response import DeletedEntityResponse
from pylusid.models.model_property import ModelProperty
from pylusid.models.operation import Operation
from pylusid.models.performance_return import PerformanceReturn
from pylusid.models.portfolio import Portfolio
from pylusid.models.portfolio_properties import PortfolioProperties
from pylusid.models.resource_list_of_access_metadata_value_of import ResourceListOfAccessMetadataValueOf
from pylusid.models.resource_list_of_aggregated_return import ResourceListOfAggregatedReturn
from pylusid.models.resource_list_of_performance_return import ResourceListOfPerformanceReturn
from pylusid.models.resource_list_of_portfolio import ResourceListOfPortfolio
from pylusid.models.resource_list_of_processed_command import ResourceListOfProcessedCommand
from pylusid.models.resource_list_of_property import ResourceListOfProperty
from pylusid.models.resource_list_of_property_interval import ResourceListOfPropertyInterval
from pylusid.models.resource_list_of_relation import ResourceListOfRelation
from pylusid.models.resource_list_of_relationship import ResourceListOfRelationship
from pylusid.models.update_portfolio_request import UpdatePortfolioRequest
from pylusid.models.upsert_portfolio_access_metadata_request import UpsertPortfolioAccessMetadataRequest
from pylusid.models.upsert_returns_response import UpsertReturnsResponse

from pylusid.api_client import ApiClient
from pylusid.exceptions import (  # noqa: F401
    ApiTypeError,
    ApiValueError
)


class PortfoliosApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient.get_default()
        self.api_client = api_client

    @validate_arguments
    def delete_key_from_portfolio_access_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Quote Access Metadata Rule to retrieve.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="The metadataKey identifying the access metadata entry to delete")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date to delete at, if this is not supplied, it will delete all data found")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """[EARLY ACCESS] DeleteKeyFromPortfolioAccessMetadata: Delete a Portfolio Access Metadata Rule  # noqa: E501

        Delete the Portfolio Access Metadata Rule that exactly matches the provided identifier parts  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_key_from_portfolio_access_metadata(scope, code, metadata_key, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Quote Access Metadata Rule to retrieve. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param metadata_key: The metadataKey identifying the access metadata entry to delete (required)
        :type metadata_key: str
        :param effective_at: The effective date to delete at, if this is not supplied, it will delete all data found
        :type effective_at: str
        :param effective_until: The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_key_from_portfolio_access_metadata_with_http_info(scope, code, metadata_key, effective_at, effective_until, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_key_from_portfolio_access_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Quote Access Metadata Rule to retrieve.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="The metadataKey identifying the access metadata entry to delete")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date to delete at, if this is not supplied, it will delete all data found")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] DeleteKeyFromPortfolioAccessMetadata: Delete a Portfolio Access Metadata Rule  # noqa: E501

        Delete the Portfolio Access Metadata Rule that exactly matches the provided identifier parts  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_key_from_portfolio_access_metadata_with_http_info(scope, code, metadata_key, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Quote Access Metadata Rule to retrieve. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param metadata_key: The metadataKey identifying the access metadata entry to delete (required)
        :type metadata_key: str
        :param effective_at: The effective date to delete at, if this is not supplied, it will delete all data found
        :type effective_at: str
        :param effective_until: The effective date until which the delete is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'metadata_key',
            'effective_at',
            'effective_until'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_key_from_portfolio_access_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['metadata_key']:
            _path_params['metadataKey'] = _params['metadata_key']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('effective_until') is not None:  # noqa: E501
            _query_params.append(('effectiveUntil', _params['effective_until']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/metadata/{metadataKey}', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_portfolio(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """DeletePortfolio: Delete portfolio  # noqa: E501

        Delete a particular portfolio.                The deletion will take effect from the portfolio's creation datetime. This means that the portfolio will no longer exist at any effective datetime, as per the asAt datetime of deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio(scope, code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_portfolio_with_http_info(scope, code, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_portfolio_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], **kwargs):  # noqa: E501
        """DeletePortfolio: Delete portfolio  # noqa: E501

        Delete a particular portfolio.                The deletion will take effect from the portfolio's creation datetime. This means that the portfolio will no longer exist at any effective datetime, as per the asAt datetime of deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_with_http_info(scope, code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_portfolio" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_portfolio_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], property_keys : Annotated[List[StrictStr], Field(..., description="The property keys of the properties to delete. These must take the format              {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'. Each property must be from the 'Portfolio' domain.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.")] = None, **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """DeletePortfolioProperties: Delete portfolio properties  # noqa: E501

        Delete one or more properties from a particular portfolio. If the properties are time-variant then an effective datetime from which  to delete properties must be specified. If the properties are perpetual then it is invalid to specify an effective datetime for deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_properties(scope, code, property_keys, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param property_keys: The property keys of the properties to delete. These must take the format              {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'. Each property must be from the 'Portfolio' domain. (required)
        :type property_keys: List[str]
        :param effective_at: The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_portfolio_properties_with_http_info(scope, code, property_keys, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_portfolio_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], property_keys : Annotated[List[StrictStr], Field(..., description="The property keys of the properties to delete. These must take the format              {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'. Each property must be from the 'Portfolio' domain.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.")] = None, **kwargs):  # noqa: E501
        """DeletePortfolioProperties: Delete portfolio properties  # noqa: E501

        Delete one or more properties from a particular portfolio. If the properties are time-variant then an effective datetime from which  to delete properties must be specified. If the properties are perpetual then it is invalid to specify an effective datetime for deletion.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_properties_with_http_info(scope, code, property_keys, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param property_keys: The property keys of the properties to delete. These must take the format              {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'. Each property must be from the 'Portfolio' domain. (required)
        :type property_keys: List[str]
        :param effective_at: The effective datetime or cut label at which to delete time-variant properties from.              The property must exist at the specified 'effectiveAt' datetime. If the 'effectiveAt' is not provided or is              before the time-variant property exists then a failure is returned. Do not specify this parameter if any of              the properties to delete are perpetual.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'property_keys',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_portfolio_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/properties', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_portfolio_returns(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], from_effective_at : Annotated[StrictStr, Field(..., description="The start date from which to delete the Returns.")], to_effective_at : Annotated[StrictStr, Field(..., description="The end date from which to delete the Returns.")], period : Annotated[Optional[StrictStr], Field(description="The Period (Daily or Monthly) of the Returns to be deleted. Defaults to Daily.")] = None, **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """[EARLY ACCESS] DeletePortfolioReturns: Delete Returns  # noqa: E501

        Cancel one or more Returns which exist into the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_returns(scope, code, return_scope, return_code, from_effective_at, to_effective_at, period, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param from_effective_at: The start date from which to delete the Returns. (required)
        :type from_effective_at: str
        :param to_effective_at: The end date from which to delete the Returns. (required)
        :type to_effective_at: str
        :param period: The Period (Daily or Monthly) of the Returns to be deleted. Defaults to Daily.
        :type period: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_portfolio_returns_with_http_info(scope, code, return_scope, return_code, from_effective_at, to_effective_at, period, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_portfolio_returns_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], from_effective_at : Annotated[StrictStr, Field(..., description="The start date from which to delete the Returns.")], to_effective_at : Annotated[StrictStr, Field(..., description="The end date from which to delete the Returns.")], period : Annotated[Optional[StrictStr], Field(description="The Period (Daily or Monthly) of the Returns to be deleted. Defaults to Daily.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] DeletePortfolioReturns: Delete Returns  # noqa: E501

        Cancel one or more Returns which exist into the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_portfolio_returns_with_http_info(scope, code, return_scope, return_code, from_effective_at, to_effective_at, period, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param from_effective_at: The start date from which to delete the Returns. (required)
        :type from_effective_at: str
        :param to_effective_at: The end date from which to delete the Returns. (required)
        :type to_effective_at: str
        :param period: The Period (Daily or Monthly) of the Returns to be deleted. Defaults to Daily.
        :type period: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'return_scope',
            'return_code',
            'from_effective_at',
            'to_effective_at',
            'period'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_portfolio_returns" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['return_scope']:
            _path_params['returnScope'] = _params['return_scope']
        if _params['return_code']:
            _path_params['returnCode'] = _params['return_code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('period') is not None:  # noqa: E501
            _query_params.append(('period', _params['period']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/returns/{returnScope}/{returnCode}/$delete', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the portfolio definition. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio definition. Defaults to returning the latest version of the portfolio definition if not specified.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Portfolio' domain to decorate onto the portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")] = None, **kwargs) -> Portfolio:  # noqa: E501
        """GetPortfolio: Get portfolio  # noqa: E501

        Retrieve the definition of a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio(scope, code, effective_at, as_at, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the portfolio definition. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio definition. Defaults to returning the latest version of the portfolio definition if not specified.
        :type as_at: datetime
        :param property_keys: A list of property keys from the 'Portfolio' domain to decorate onto the portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: Portfolio
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_with_http_info(scope, code, effective_at, as_at, property_keys, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the portfolio definition. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio definition. Defaults to returning the latest version of the portfolio definition if not specified.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Portfolio' domain to decorate onto the portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")] = None, **kwargs):  # noqa: E501
        """GetPortfolio: Get portfolio  # noqa: E501

        Retrieve the definition of a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_with_http_info(scope, code, effective_at, as_at, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the portfolio definition. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio definition. Defaults to returning the latest version of the portfolio definition if not specified.
        :type as_at: datetime
        :param property_keys: A list of property keys from the 'Portfolio' domain to decorate onto the portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(Portfolio, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'property_keys'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "Portfolio",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_aggregate_returns(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], recipe_id_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The Recipe Scope for getting the fx rates")] = None, recipe_id_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The Recipe Code for getting the fx rates")] = None, from_effective_at : Annotated[Optional[StrictStr], Field(description="The start date from which to calculate the Returns.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The end date for which to calculate the Returns.")] = None, composite_method : Annotated[Optional[StrictStr], Field(description="The method used to calculate the Portfolio performance:              Equal/Asset.")] = None, period : Annotated[Optional[StrictStr], Field(description="The type of the returns used to calculate the aggregation result: Daily/Monthly.")] = None, output_frequency : Annotated[Optional[StrictStr], Field(description="The type of calculated output: Daily/Weekly/Monthly/Quarterly/Half-Yearly/Yearly.")] = None, metrics : Annotated[Optional[List[StrictStr]], Field(description="Determines what type of returns should be calculated, see https://support.lusid.com/knowledgebase/article/KA-01675/en-us for a list of available metrics.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Returns. Defaults to the latest.")] = None, alternative_inc_date : Annotated[Optional[StrictStr], Field(description="The date from which to consider the Returns on the Portfolio, if this is different from the date when Returns begin. Can be a date string or Portfolio property.")] = None, **kwargs) -> ResourceListOfAggregatedReturn:  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioAggregateReturns: Aggregate Returns (This is a deprecated endpoint).  # noqa: E501

        Aggregate Returns which are on the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_aggregate_returns(scope, code, return_scope, return_code, recipe_id_scope, recipe_id_code, from_effective_at, to_effective_at, composite_method, period, output_frequency, metrics, as_at, alternative_inc_date, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param recipe_id_scope: The Recipe Scope for getting the fx rates
        :type recipe_id_scope: str
        :param recipe_id_code: The Recipe Code for getting the fx rates
        :type recipe_id_code: str
        :param from_effective_at: The start date from which to calculate the Returns.
        :type from_effective_at: str
        :param to_effective_at: The end date for which to calculate the Returns.
        :type to_effective_at: str
        :param composite_method: The method used to calculate the Portfolio performance:              Equal/Asset.
        :type composite_method: str
        :param period: The type of the returns used to calculate the aggregation result: Daily/Monthly.
        :type period: str
        :param output_frequency: The type of calculated output: Daily/Weekly/Monthly/Quarterly/Half-Yearly/Yearly.
        :type output_frequency: str
        :param metrics: Determines what type of returns should be calculated, see https://support.lusid.com/knowledgebase/article/KA-01675/en-us for a list of available metrics.
        :type metrics: List[str]
        :param as_at: The asAt datetime at which to retrieve the Returns. Defaults to the latest.
        :type as_at: datetime
        :param alternative_inc_date: The date from which to consider the Returns on the Portfolio, if this is different from the date when Returns begin. Can be a date string or Portfolio property.
        :type alternative_inc_date: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfAggregatedReturn
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_aggregate_returns_with_http_info(scope, code, return_scope, return_code, recipe_id_scope, recipe_id_code, from_effective_at, to_effective_at, composite_method, period, output_frequency, metrics, as_at, alternative_inc_date, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_aggregate_returns_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], recipe_id_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The Recipe Scope for getting the fx rates")] = None, recipe_id_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The Recipe Code for getting the fx rates")] = None, from_effective_at : Annotated[Optional[StrictStr], Field(description="The start date from which to calculate the Returns.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The end date for which to calculate the Returns.")] = None, composite_method : Annotated[Optional[StrictStr], Field(description="The method used to calculate the Portfolio performance:              Equal/Asset.")] = None, period : Annotated[Optional[StrictStr], Field(description="The type of the returns used to calculate the aggregation result: Daily/Monthly.")] = None, output_frequency : Annotated[Optional[StrictStr], Field(description="The type of calculated output: Daily/Weekly/Monthly/Quarterly/Half-Yearly/Yearly.")] = None, metrics : Annotated[Optional[List[StrictStr]], Field(description="Determines what type of returns should be calculated, see https://support.lusid.com/knowledgebase/article/KA-01675/en-us for a list of available metrics.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Returns. Defaults to the latest.")] = None, alternative_inc_date : Annotated[Optional[StrictStr], Field(description="The date from which to consider the Returns on the Portfolio, if this is different from the date when Returns begin. Can be a date string or Portfolio property.")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioAggregateReturns: Aggregate Returns (This is a deprecated endpoint).  # noqa: E501

        Aggregate Returns which are on the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_aggregate_returns_with_http_info(scope, code, return_scope, return_code, recipe_id_scope, recipe_id_code, from_effective_at, to_effective_at, composite_method, period, output_frequency, metrics, as_at, alternative_inc_date, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param recipe_id_scope: The Recipe Scope for getting the fx rates
        :type recipe_id_scope: str
        :param recipe_id_code: The Recipe Code for getting the fx rates
        :type recipe_id_code: str
        :param from_effective_at: The start date from which to calculate the Returns.
        :type from_effective_at: str
        :param to_effective_at: The end date for which to calculate the Returns.
        :type to_effective_at: str
        :param composite_method: The method used to calculate the Portfolio performance:              Equal/Asset.
        :type composite_method: str
        :param period: The type of the returns used to calculate the aggregation result: Daily/Monthly.
        :type period: str
        :param output_frequency: The type of calculated output: Daily/Weekly/Monthly/Quarterly/Half-Yearly/Yearly.
        :type output_frequency: str
        :param metrics: Determines what type of returns should be calculated, see https://support.lusid.com/knowledgebase/article/KA-01675/en-us for a list of available metrics.
        :type metrics: List[str]
        :param as_at: The asAt datetime at which to retrieve the Returns. Defaults to the latest.
        :type as_at: datetime
        :param alternative_inc_date: The date from which to consider the Returns on the Portfolio, if this is different from the date when Returns begin. Can be a date string or Portfolio property.
        :type alternative_inc_date: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfAggregatedReturn, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'return_scope',
            'return_code',
            'recipe_id_scope',
            'recipe_id_code',
            'from_effective_at',
            'to_effective_at',
            'composite_method',
            'period',
            'output_frequency',
            'metrics',
            'as_at',
            'alternative_inc_date'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_aggregate_returns" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['return_scope']:
            _path_params['returnScope'] = _params['return_scope']
        if _params['return_code']:
            _path_params['returnCode'] = _params['return_code']

        # process the query parameters
        _query_params = []
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('composite_method') is not None:  # noqa: E501
            _query_params.append(('compositeMethod', _params['composite_method']))
        if _params.get('period') is not None:  # noqa: E501
            _query_params.append(('period', _params['period']))
        if _params.get('output_frequency') is not None:  # noqa: E501
            _query_params.append(('outputFrequency', _params['output_frequency']))
        if _params.get('metrics') is not None:  # noqa: E501
            _query_params.append(('metrics', _params['metrics']))
            _collection_formats['metrics'] = 'multi'
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('alternative_inc_date') is not None:  # noqa: E501
            _query_params.append(('alternativeIncDate', _params['alternative_inc_date']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfAggregatedReturn",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/returns/{returnScope}/{returnCode}/aggregated', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_aggregated_returns(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], aggregated_returns_request : Annotated[AggregatedReturnsRequest, Field(..., description="The request used in the AggregatedReturns.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The start date from which to calculate the Returns.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The end date for which to calculate the Returns.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Returns. Defaults to the latest.")] = None, **kwargs) -> AggregatedReturnsResponse:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioAggregatedReturns: Aggregated Returns  # noqa: E501

        Aggregate Returns which are on the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_aggregated_returns(scope, code, aggregated_returns_request, from_effective_at, to_effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param aggregated_returns_request: The request used in the AggregatedReturns. (required)
        :type aggregated_returns_request: AggregatedReturnsRequest
        :param from_effective_at: The start date from which to calculate the Returns.
        :type from_effective_at: str
        :param to_effective_at: The end date for which to calculate the Returns.
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the Returns. Defaults to the latest.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: AggregatedReturnsResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_aggregated_returns_with_http_info(scope, code, aggregated_returns_request, from_effective_at, to_effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_aggregated_returns_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], aggregated_returns_request : Annotated[AggregatedReturnsRequest, Field(..., description="The request used in the AggregatedReturns.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The start date from which to calculate the Returns.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The end date for which to calculate the Returns.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Returns. Defaults to the latest.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioAggregatedReturns: Aggregated Returns  # noqa: E501

        Aggregate Returns which are on the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_aggregated_returns_with_http_info(scope, code, aggregated_returns_request, from_effective_at, to_effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param aggregated_returns_request: The request used in the AggregatedReturns. (required)
        :type aggregated_returns_request: AggregatedReturnsRequest
        :param from_effective_at: The start date from which to calculate the Returns.
        :type from_effective_at: str
        :param to_effective_at: The end date for which to calculate the Returns.
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the Returns. Defaults to the latest.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(AggregatedReturnsResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'aggregated_returns_request',
            'from_effective_at',
            'to_effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_aggregated_returns" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['aggregated_returns_request']:
            _body_params = _params['aggregated_returns_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "AggregatedReturnsResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/returns/$aggregated', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_commands(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], from_as_at : Annotated[Optional[datetime], Field(description="The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.")] = None, to_as_at : Annotated[Optional[datetime], Field(description="The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results.              For example, to filter on the User ID, specify \"userId.id eq 'string'\".              For more information about filtering, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing commands; this value is returned from the previous call.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number. Defaults to 500 if not specified.")] = None, **kwargs) -> ResourceListOfProcessedCommand:  # noqa: E501
        """GetPortfolioCommands: Get portfolio commands  # noqa: E501

        Get all the commands that modified a particular portfolio, including any input transactions.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_commands(scope, code, from_as_at, to_as_at, filter, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param from_as_at: The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.
        :type from_as_at: datetime
        :param to_as_at: The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.
        :type to_as_at: datetime
        :param filter: Expression to filter the results.              For example, to filter on the User ID, specify \"userId.id eq 'string'\".              For more information about filtering, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param page: The pagination token to use to continue listing commands; this value is returned from the previous call.
        :type page: str
        :param limit: When paginating, limit the results to this number. Defaults to 500 if not specified.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfProcessedCommand
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_commands_with_http_info(scope, code, from_as_at, to_as_at, filter, page, limit, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_commands_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], from_as_at : Annotated[Optional[datetime], Field(description="The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.")] = None, to_as_at : Annotated[Optional[datetime], Field(description="The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results.              For example, to filter on the User ID, specify \"userId.id eq 'string'\".              For more information about filtering, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing commands; this value is returned from the previous call.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number. Defaults to 500 if not specified.")] = None, **kwargs):  # noqa: E501
        """GetPortfolioCommands: Get portfolio commands  # noqa: E501

        Get all the commands that modified a particular portfolio, including any input transactions.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_commands_with_http_info(scope, code, from_as_at, to_as_at, filter, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param from_as_at: The lower bound asAt datetime (inclusive) from which to retrieve commands. There is no lower bound if this is not specified.
        :type from_as_at: datetime
        :param to_as_at: The upper bound asAt datetime (inclusive) from which to retrieve commands. There is no upper bound if this is not specified.
        :type to_as_at: datetime
        :param filter: Expression to filter the results.              For example, to filter on the User ID, specify \"userId.id eq 'string'\".              For more information about filtering, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param page: The pagination token to use to continue listing commands; this value is returned from the previous call.
        :type page: str
        :param limit: When paginating, limit the results to this number. Defaults to 500 if not specified.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfProcessedCommand, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_as_at',
            'to_as_at',
            'filter',
            'page',
            'limit'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_commands" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_as_at') is not None:  # noqa: E501
            _query_params.append(('fromAsAt', _params['from_as_at']))
        if _params.get('to_as_at') is not None:  # noqa: E501
            _query_params.append(('toAsAt', _params['to_as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfProcessedCommand",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/commands', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Access Metadata Rule to retrieve.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], effective_at : Annotated[Optional[StrictStr], Field(description="The effectiveAt datetime at which to retrieve the access metadata rule.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio access metadata.")] = None, **kwargs) -> Dict[str, List[AccessMetadataValue]]:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioMetadata: Get access metadata rules for a portfolio  # noqa: E501

        Pass the scope and portfolio code parameters to retrieve the AccessMetadata associated with a portfolio  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_metadata(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Access Metadata Rule to retrieve. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param effective_at: The effectiveAt datetime at which to retrieve the access metadata rule.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio access metadata.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: Dict[str, List[AccessMetadataValue]]
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_metadata_with_http_info(scope, code, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Access Metadata Rule to retrieve.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], effective_at : Annotated[Optional[StrictStr], Field(description="The effectiveAt datetime at which to retrieve the access metadata rule.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio access metadata.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioMetadata: Get access metadata rules for a portfolio  # noqa: E501

        Pass the scope and portfolio code parameters to retrieve the AccessMetadata associated with a portfolio  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_metadata_with_http_info(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Access Metadata Rule to retrieve. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param effective_at: The effectiveAt datetime at which to retrieve the access metadata rule.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio access metadata.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(Dict[str, List[AccessMetadataValue]], status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "Dict[str, List[AccessMetadataValue]]",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/metadata', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.")] = None, **kwargs) -> PortfolioProperties:  # noqa: E501
        """GetPortfolioProperties: Get portfolio properties  # noqa: E501

        List all the properties of a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_properties(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioProperties
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_properties_with_http_info(scope, code, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.")] = None, **kwargs):  # noqa: E501
        """GetPortfolioProperties: Get portfolio properties  # noqa: E501

        List all the properties of a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_properties_with_http_info(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioProperties, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioProperties",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/properties', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_property_time_series(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], property_key : Annotated[StrictStr, Field(..., description="The property key of the property whose history to show.              This must be from the 'Portfolio' domain and in the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")], portfolio_effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime used to resolve the portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to show the history. Defaults to returning the current datetime if not supplied.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results. For more information about filtering,              see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing properties; this value is returned from              the previous call. If a pagination token is provided, the filter, portfolioEffectiveAt, and asAt fields              must not have changed since the original request.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number.")] = None, **kwargs) -> ResourceListOfPropertyInterval:  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioPropertyTimeSeries: Get portfolio property time series  # noqa: E501

        Show the complete time series (history) for a particular portfolio property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_property_time_series(scope, code, property_key, portfolio_effective_at, as_at, filter, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param property_key: The property key of the property whose history to show.              This must be from the 'Portfolio' domain and in the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'. (required)
        :type property_key: str
        :param portfolio_effective_at: The effective datetime used to resolve the portfolio. Defaults to the current LUSID system datetime if not specified.
        :type portfolio_effective_at: str
        :param as_at: The asAt datetime at which to show the history. Defaults to returning the current datetime if not supplied.
        :type as_at: datetime
        :param filter: Expression to filter the results. For more information about filtering,              see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param page: The pagination token to use to continue listing properties; this value is returned from              the previous call. If a pagination token is provided, the filter, portfolioEffectiveAt, and asAt fields              must not have changed since the original request.
        :type page: str
        :param limit: When paginating, limit the results to this number.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPropertyInterval
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_property_time_series_with_http_info(scope, code, property_key, portfolio_effective_at, as_at, filter, page, limit, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_property_time_series_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], property_key : Annotated[StrictStr, Field(..., description="The property key of the property whose history to show.              This must be from the 'Portfolio' domain and in the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")], portfolio_effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime used to resolve the portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to show the history. Defaults to returning the current datetime if not supplied.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results. For more information about filtering,              see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing properties; this value is returned from              the previous call. If a pagination token is provided, the filter, portfolioEffectiveAt, and asAt fields              must not have changed since the original request.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number.")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioPropertyTimeSeries: Get portfolio property time series  # noqa: E501

        Show the complete time series (history) for a particular portfolio property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_property_time_series_with_http_info(scope, code, property_key, portfolio_effective_at, as_at, filter, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param property_key: The property key of the property whose history to show.              This must be from the 'Portfolio' domain and in the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'. (required)
        :type property_key: str
        :param portfolio_effective_at: The effective datetime used to resolve the portfolio. Defaults to the current LUSID system datetime if not specified.
        :type portfolio_effective_at: str
        :param as_at: The asAt datetime at which to show the history. Defaults to returning the current datetime if not supplied.
        :type as_at: datetime
        :param filter: Expression to filter the results. For more information about filtering,              see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param page: The pagination token to use to continue listing properties; this value is returned from              the previous call. If a pagination token is provided, the filter, portfolioEffectiveAt, and asAt fields              must not have changed since the original request.
        :type page: str
        :param limit: When paginating, limit the results to this number.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPropertyInterval, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'property_key',
            'portfolio_effective_at',
            'as_at',
            'filter',
            'page',
            'limit'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_property_time_series" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('property_key') is not None:  # noqa: E501
            _query_params.append(('propertyKey', _params['property_key']))
        if _params.get('portfolio_effective_at') is not None:  # noqa: E501
            _query_params.append(('portfolioEffectiveAt', _params['portfolio_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPropertyInterval",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/properties/time-series', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_relations(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relations. Defaults to returning the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the relations. Provide a null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifier types (as property keys) used for referencing Persons or Legal Entities.              These must be from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. Only identifier types provided will be used to look up relevant entities in relations. If not applicable, provide an empty array.")] = None, **kwargs) -> ResourceListOfRelation:  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioRelations: Get portfolio relations  # noqa: E501

        Get relations for a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_relations(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relations. Defaults to returning the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the relations. Provide a null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifier types (as property keys) used for referencing Persons or Legal Entities.              These must be from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. Only identifier types provided will be used to look up relevant entities in relations. If not applicable, provide an empty array.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfRelation
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_relations_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_relations_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relations. Defaults to returning the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the relations. Provide a null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifier types (as property keys) used for referencing Persons or Legal Entities.              These must be from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. Only identifier types provided will be used to look up relevant entities in relations. If not applicable, provide an empty array.")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] GetPortfolioRelations: Get portfolio relations  # noqa: E501

        Get relations for a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_relations_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relations. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relations. Defaults to returning the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the relations. Provide a null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifier types (as property keys) used for referencing Persons or Legal Entities.              These must be from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. Only identifier types provided will be used to look up relevant entities in relations. If not applicable, provide an empty array.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfRelation, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'identifier_types'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_relations" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('identifier_types') is not None:  # noqa: E501
            _query_params.append(('identifierTypes', _params['identifier_types']))
            _collection_formats['identifierTypes'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfRelation",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/relations', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_relationships(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to retrieve relationships. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relationships. Defaults to returning the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the relationships. Provide a null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.")] = None, **kwargs) -> ResourceListOfRelationship:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioRelationships: Get portfolio relationships  # noqa: E501

        Get relationships for a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_relationships(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relationships. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relationships. Defaults to returning the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the relationships. Provide a null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfRelationship
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_relationships_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_relationships_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to retrieve relationships. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve relationships. Defaults to returning the latest LUSID AsAt time if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the relationships. Provide a null or empty string for this field until further notice.")] = None, identifier_types : Annotated[Optional[List[StrictStr]], Field(description="Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioRelationships: Get portfolio relationships  # noqa: E501

        Get relationships for a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_relationships_with_http_info(scope, code, effective_at, as_at, filter, identifier_types, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve relationships. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve relationships. Defaults to returning the latest LUSID AsAt time if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the relationships. Provide a null or empty string for this field until further notice.
        :type filter: str
        :param identifier_types: Identifier types (as property keys) used for referencing Persons or Legal Entities.              These can be specified from the 'Person' or 'LegalEntity' domains and have the format {domain}/{scope}/{code}, for example              'Person/CompanyDetails/Role'. An Empty array may be used to return all related Entities.
        :type identifier_types: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfRelationship, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'identifier_types'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_relationships" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('identifier_types') is not None:  # noqa: E501
            _query_params.append(('identifierTypes', _params['identifier_types']))
            _collection_formats['identifierTypes'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfRelationship",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/relationships', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_returns(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The start date from which to get the Returns.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The end date from which to get the Returns.")] = None, period : Annotated[Optional[StrictStr], Field(description="Show the Returns on a Daily or Monthly period. Defaults to Daily.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Returns. Defaults to the latest.")] = None, **kwargs) -> ResourceListOfPerformanceReturn:  # noqa: E501
        """[EARLY ACCESS] GetPortfolioReturns: Get Returns  # noqa: E501

        Get Returns which are on the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_returns(scope, code, return_scope, return_code, from_effective_at, to_effective_at, period, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param from_effective_at: The start date from which to get the Returns.
        :type from_effective_at: str
        :param to_effective_at: The end date from which to get the Returns.
        :type to_effective_at: str
        :param period: Show the Returns on a Daily or Monthly period. Defaults to Daily.
        :type period: str
        :param as_at: The asAt datetime at which to retrieve the Returns. Defaults to the latest.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPerformanceReturn
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_returns_with_http_info(scope, code, return_scope, return_code, from_effective_at, to_effective_at, period, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_returns_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The start date from which to get the Returns.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The end date from which to get the Returns.")] = None, period : Annotated[Optional[StrictStr], Field(description="Show the Returns on a Daily or Monthly period. Defaults to Daily.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the Returns. Defaults to the latest.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfolioReturns: Get Returns  # noqa: E501

        Get Returns which are on the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_returns_with_http_info(scope, code, return_scope, return_code, from_effective_at, to_effective_at, period, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param from_effective_at: The start date from which to get the Returns.
        :type from_effective_at: str
        :param to_effective_at: The end date from which to get the Returns.
        :type to_effective_at: str
        :param period: Show the Returns on a Daily or Monthly period. Defaults to Daily.
        :type period: str
        :param as_at: The asAt datetime at which to retrieve the Returns. Defaults to the latest.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPerformanceReturn, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'return_scope',
            'return_code',
            'from_effective_at',
            'to_effective_at',
            'period',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_returns" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['return_scope']:
            _path_params['returnScope'] = _params['return_scope']
        if _params['return_code']:
            _path_params['returnCode'] = _params['return_code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('period') is not None:  # noqa: E501
            _query_params.append(('period', _params['period']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPerformanceReturn",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/returns/{returnScope}/{returnCode}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolios_access_metadata_by_key(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Access Metadata Rule to retrieve.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The code of the portfolio")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the metadata to retrieve")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date of the rule")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio access metadata.")] = None, **kwargs) -> List[AccessMetadataValue]:  # noqa: E501
        """[EARLY ACCESS] GetPortfoliosAccessMetadataByKey: Get an entry identified by a metadataKey in the access metadata object  # noqa: E501

        Get a specific portfolio access metadata rule by specifying the corresponding identifier parts                No matching will be performed through this endpoint. To retrieve a rule, it is necessary to specify, exactly, the identifier of the rule  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolios_access_metadata_by_key(scope, code, metadata_key, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Access Metadata Rule to retrieve. (required)
        :type scope: str
        :param code: The code of the portfolio (required)
        :type code: str
        :param metadata_key: Key of the metadata to retrieve (required)
        :type metadata_key: str
        :param effective_at: The effective date of the rule
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio access metadata.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: List[AccessMetadataValue]
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolios_access_metadata_by_key_with_http_info(scope, code, metadata_key, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolios_access_metadata_by_key_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Access Metadata Rule to retrieve.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The code of the portfolio")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the metadata to retrieve")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective date of the rule")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio access metadata.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetPortfoliosAccessMetadataByKey: Get an entry identified by a metadataKey in the access metadata object  # noqa: E501

        Get a specific portfolio access metadata rule by specifying the corresponding identifier parts                No matching will be performed through this endpoint. To retrieve a rule, it is necessary to specify, exactly, the identifier of the rule  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolios_access_metadata_by_key_with_http_info(scope, code, metadata_key, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Access Metadata Rule to retrieve. (required)
        :type scope: str
        :param code: The code of the portfolio (required)
        :type code: str
        :param metadata_key: Key of the metadata to retrieve (required)
        :type metadata_key: str
        :param effective_at: The effective date of the rule
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio access metadata.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(List[AccessMetadataValue], status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'metadata_key',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolios_access_metadata_by_key" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['metadata_key']:
            _path_params['metadataKey'] = _params['metadata_key']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "List[AccessMetadataValue]",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/metadata/{metadataKey}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def list_portfolio_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing commands; this value is returned from the previous call.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results per page to this number.")] = None, **kwargs) -> ResourceListOfProperty:  # noqa: E501
        """[EARLY ACCESS] ListPortfolioProperties: Get portfolio properties  # noqa: E501

        List all the properties of a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolio_properties(scope, code, effective_at, as_at, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.
        :type as_at: datetime
        :param page: The pagination token to use to continue listing commands; this value is returned from the previous call.
        :type page: str
        :param limit: When paginating, limit the results per page to this number.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfProperty
        """
        kwargs['_return_http_data_only'] = True
        return self.list_portfolio_properties_with_http_info(scope, code, effective_at, as_at, page, limit, **kwargs)  # noqa: E501

    @validate_arguments
    def list_portfolio_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], effective_at : Annotated[Optional[constr(strict=True, max_length=256, min_length=0)], Field(description="The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing commands; this value is returned from the previous call.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results per page to this number.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] ListPortfolioProperties: Get portfolio properties  # noqa: E501

        List all the properties of a particular portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolio_properties_with_http_info(scope, code, effective_at, as_at, page, limit, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to list the portfolio's properties. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolio's properties. Defaults to returning the latest version of each property if not specified.
        :type as_at: datetime
        :param page: The pagination token to use to continue listing commands; this value is returned from the previous call.
        :type page: str
        :param limit: When paginating, limit the results per page to this number.
        :type limit: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfProperty, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'page',
            'limit'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_portfolio_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfProperty",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/properties/list', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def list_portfolios(self, effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing portfolios; this              value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt              and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.")] = None, start : Annotated[Optional[StrictInt], Field(description="When paginating, skip this number of results.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number. Defaults to 100 if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results.              For example, to filter on the transaction type, specify \"type eq 'Transaction'\". For more information about filtering              results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, query : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression specifying the criteria that the returned portfolios must meet. For example, to see which              portfolios have holdings in instruments with a LusidInstrumentId (LUID) of 'LUID_PPA8HI6M' or a Figi of 'BBG000BLNNH6',              specify \"instrument.identifiers in (('LusidInstrumentId', 'LUID_PPA8HI6M'), ('Figi', 'BBG000BLNNH6'))\".")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")] = None, **kwargs) -> ResourceListOfPortfolio:  # noqa: E501
        """ListPortfolios: List portfolios  # noqa: E501

        List all the portfolios matching particular criteria.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolios(effective_at, as_at, page, start, limit, filter, query, property_keys, async_req=True)
        >>> result = thread.get()

        :param effective_at: The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.
        :type as_at: datetime
        :param page: The pagination token to use to continue listing portfolios; this              value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt              and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.
        :type page: str
        :param start: When paginating, skip this number of results.
        :type start: int
        :param limit: When paginating, limit the results to this number. Defaults to 100 if not specified.
        :type limit: int
        :param filter: Expression to filter the results.              For example, to filter on the transaction type, specify \"type eq 'Transaction'\". For more information about filtering              results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param query: Expression specifying the criteria that the returned portfolios must meet. For example, to see which              portfolios have holdings in instruments with a LusidInstrumentId (LUID) of 'LUID_PPA8HI6M' or a Figi of 'BBG000BLNNH6',              specify \"instrument.identifiers in (('LusidInstrumentId', 'LUID_PPA8HI6M'), ('Figi', 'BBG000BLNNH6'))\".
        :type query: str
        :param property_keys: A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPortfolio
        """
        kwargs['_return_http_data_only'] = True
        return self.list_portfolios_with_http_info(effective_at, as_at, page, start, limit, filter, query, property_keys, **kwargs)  # noqa: E501

    @validate_arguments
    def list_portfolios_with_http_info(self, effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing portfolios; this              value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt              and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.")] = None, start : Annotated[Optional[StrictInt], Field(description="When paginating, skip this number of results.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number. Defaults to 100 if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results.              For example, to filter on the transaction type, specify \"type eq 'Transaction'\". For more information about filtering              results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, query : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression specifying the criteria that the returned portfolios must meet. For example, to see which              portfolios have holdings in instruments with a LusidInstrumentId (LUID) of 'LUID_PPA8HI6M' or a Figi of 'BBG000BLNNH6',              specify \"instrument.identifiers in (('LusidInstrumentId', 'LUID_PPA8HI6M'), ('Figi', 'BBG000BLNNH6'))\".")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")] = None, **kwargs):  # noqa: E501
        """ListPortfolios: List portfolios  # noqa: E501

        List all the portfolios matching particular criteria.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolios_with_http_info(effective_at, as_at, page, start, limit, filter, query, property_keys, async_req=True)
        >>> result = thread.get()

        :param effective_at: The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.
        :type as_at: datetime
        :param page: The pagination token to use to continue listing portfolios; this              value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt              and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.
        :type page: str
        :param start: When paginating, skip this number of results.
        :type start: int
        :param limit: When paginating, limit the results to this number. Defaults to 100 if not specified.
        :type limit: int
        :param filter: Expression to filter the results.              For example, to filter on the transaction type, specify \"type eq 'Transaction'\". For more information about filtering              results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param query: Expression specifying the criteria that the returned portfolios must meet. For example, to see which              portfolios have holdings in instruments with a LusidInstrumentId (LUID) of 'LUID_PPA8HI6M' or a Figi of 'BBG000BLNNH6',              specify \"instrument.identifiers in (('LusidInstrumentId', 'LUID_PPA8HI6M'), ('Figi', 'BBG000BLNNH6'))\".
        :type query: str
        :param property_keys: A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPortfolio, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'effective_at',
            'as_at',
            'page',
            'start',
            'limit',
            'filter',
            'query',
            'property_keys'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_portfolios" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('start') is not None:  # noqa: E501
            _query_params.append(('start', _params['start']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('query') is not None:  # noqa: E501
            _query_params.append(('query', _params['query']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPortfolio",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def list_portfolios_for_scope(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope whose portfolios to list.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing portfolios. This  value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt  and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.")] = None, start : Annotated[Optional[StrictInt], Field(description="When paginating, skip this number of results.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number. Defaults to 100 if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")] = None, **kwargs) -> ResourceListOfPortfolio:  # noqa: E501
        """ListPortfoliosForScope: List portfolios for scope  # noqa: E501

        List all the portfolios in a particular scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolios_for_scope(scope, effective_at, as_at, page, start, limit, filter, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope whose portfolios to list. (required)
        :type scope: str
        :param effective_at: The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.
        :type as_at: datetime
        :param page: The pagination token to use to continue listing portfolios. This  value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt  and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.
        :type page: str
        :param start: When paginating, skip this number of results.
        :type start: int
        :param limit: When paginating, limit the results to this number. Defaults to 100 if not specified.
        :type limit: int
        :param filter: Expression to filter the results.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPortfolio
        """
        kwargs['_return_http_data_only'] = True
        return self.list_portfolios_for_scope_with_http_info(scope, effective_at, as_at, page, start, limit, filter, property_keys, **kwargs)  # noqa: E501

    @validate_arguments
    def list_portfolios_for_scope_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope whose portfolios to list.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing portfolios. This  value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt  and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.")] = None, start : Annotated[Optional[StrictInt], Field(description="When paginating, skip this number of results.")] = None, limit : Annotated[Optional[conint(strict=True, ge=5000, le=1)], Field(description="When paginating, limit the results to this number. Defaults to 100 if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the results.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.")] = None, **kwargs):  # noqa: E501
        """ListPortfoliosForScope: List portfolios for scope  # noqa: E501

        List all the portfolios in a particular scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_portfolios_for_scope_with_http_info(scope, effective_at, as_at, page, start, limit, filter, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope whose portfolios to list. (required)
        :type scope: str
        :param effective_at: The effective datetime or cut label at which to list the portfolios. Defaults to the current LUSID              system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to list the portfolios. Defaults to returning the latest version              of each portfolio if not specified.
        :type as_at: datetime
        :param page: The pagination token to use to continue listing portfolios. This  value is returned from the previous call. If a pagination token is provided, the filter, effectiveAt  and asAt fields must not have changed since the original request. Also, if set, a start value cannot be provided.
        :type page: str
        :param start: When paginating, skip this number of results.
        :type start: int
        :param limit: When paginating, limit the results to this number. Defaults to 100 if not specified.
        :type limit: int
        :param filter: Expression to filter the results.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the 'Portfolio' domain to decorate onto each portfolio.              These must take the format {domain}/{scope}/{code}, for example 'Portfolio/Manager/Id'.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPortfolio, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'effective_at',
            'as_at',
            'page',
            'start',
            'limit',
            'filter',
            'property_keys'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_portfolios_for_scope" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('start') is not None:  # noqa: E501
            _query_params.append(('start', _params['start']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPortfolio",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def patch_portfolio(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the               scope this uniquely identifies the portfolio.")], operation : Annotated[List[Operation], Field(..., description="The json patch document. For more check: https://datatracker.ietf.org/doc/html/rfc6902.")], **kwargs) -> ActionResultOfPortfolio:  # noqa: E501
        """[EARLY ACCESS] PatchPortfolio: Patch portfolio.  # noqa: E501

        Create or update certain fields for a particular  portfolio.  The behaviour is defined by the JSON Patch specification.                Currently supported are: CreationDate.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio(scope, code, operation, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the               scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param operation: The json patch document. For more check: https://datatracker.ietf.org/doc/html/rfc6902. (required)
        :type operation: List[Operation]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ActionResultOfPortfolio
        """
        kwargs['_return_http_data_only'] = True
        return self.patch_portfolio_with_http_info(scope, code, operation, **kwargs)  # noqa: E501

    @validate_arguments
    def patch_portfolio_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the               scope this uniquely identifies the portfolio.")], operation : Annotated[List[Operation], Field(..., description="The json patch document. For more check: https://datatracker.ietf.org/doc/html/rfc6902.")], **kwargs):  # noqa: E501
        """[EARLY ACCESS] PatchPortfolio: Patch portfolio.  # noqa: E501

        Create or update certain fields for a particular  portfolio.  The behaviour is defined by the JSON Patch specification.                Currently supported are: CreationDate.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio_with_http_info(scope, code, operation, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the               scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param operation: The json patch document. For more check: https://datatracker.ietf.org/doc/html/rfc6902. (required)
        :type operation: List[Operation]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ActionResultOfPortfolio, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'operation'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_portfolio" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['operation']:
            _body_params = _params['operation']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ActionResultOfPortfolio",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}', 'PATCH',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def patch_portfolio_access_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Access Metadata Rule.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], access_metadata_operation : Annotated[List[AccessMetadataOperation], Field(..., description="The Json Patch document")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs) -> Dict[str, List[AccessMetadataValue]]:  # noqa: E501
        """[EXPERIMENTAL] PatchPortfolioAccessMetadata: Patch Access Metadata rules for a Portfolio.  # noqa: E501

        Patch Portfolio Access Metadata Rules in a single scope.  The behaviour is defined by the JSON Patch specification.                Currently only 'add' is a supported operation on the patch document.    Currently only valid metadata keys are supported paths on the patch document.    The response will return any affected Portfolio Access Metadata rules or a failure message if unsuccessful.    It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exist with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio_access_metadata(scope, code, access_metadata_operation, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Access Metadata Rule. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param access_metadata_operation: The Json Patch document (required)
        :type access_metadata_operation: List[AccessMetadataOperation]
        :param effective_at: The date this rule will effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: Dict[str, List[AccessMetadataValue]]
        """
        kwargs['_return_http_data_only'] = True
        return self.patch_portfolio_access_metadata_with_http_info(scope, code, access_metadata_operation, effective_at, effective_until, **kwargs)  # noqa: E501

    @validate_arguments
    def patch_portfolio_access_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope of the Portfolio Access Metadata Rule.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], access_metadata_operation : Annotated[List[AccessMetadataOperation], Field(..., description="The Json Patch document")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] PatchPortfolioAccessMetadata: Patch Access Metadata rules for a Portfolio.  # noqa: E501

        Patch Portfolio Access Metadata Rules in a single scope.  The behaviour is defined by the JSON Patch specification.                Currently only 'add' is a supported operation on the patch document.    Currently only valid metadata keys are supported paths on the patch document.    The response will return any affected Portfolio Access Metadata rules or a failure message if unsuccessful.    It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exist with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio_access_metadata_with_http_info(scope, code, access_metadata_operation, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio Access Metadata Rule. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param access_metadata_operation: The Json Patch document (required)
        :type access_metadata_operation: List[AccessMetadataOperation]
        :param effective_at: The date this rule will effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(Dict[str, List[AccessMetadataValue]], status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'access_metadata_operation',
            'effective_at',
            'effective_until'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_portfolio_access_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('effective_until') is not None:  # noqa: E501
            _query_params.append(('effectiveUntil', _params['effective_until']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['access_metadata_operation']:
            _body_params = _params['access_metadata_operation']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "Dict[str, List[AccessMetadataValue]]",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/metadata', 'PATCH',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def update_portfolio(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], update_portfolio_request : Annotated[UpdatePortfolioRequest, Field(..., description="The updated portfolio definition.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to update the definition. Defaults to the current               LUSID system datetime if not specified.")] = None, **kwargs) -> Portfolio:  # noqa: E501
        """UpdatePortfolio: Update portfolio  # noqa: E501

        Update the definition of a particular portfolio.                Note that not all elements of a portfolio definition are  modifiable due to the potential implications for data already stored.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.update_portfolio(scope, code, update_portfolio_request, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param update_portfolio_request: The updated portfolio definition. (required)
        :type update_portfolio_request: UpdatePortfolioRequest
        :param effective_at: The effective datetime or cut label at which to update the definition. Defaults to the current               LUSID system datetime if not specified.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: Portfolio
        """
        kwargs['_return_http_data_only'] = True
        return self.update_portfolio_with_http_info(scope, code, update_portfolio_request, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def update_portfolio_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], update_portfolio_request : Annotated[UpdatePortfolioRequest, Field(..., description="The updated portfolio definition.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to update the definition. Defaults to the current               LUSID system datetime if not specified.")] = None, **kwargs):  # noqa: E501
        """UpdatePortfolio: Update portfolio  # noqa: E501

        Update the definition of a particular portfolio.                Note that not all elements of a portfolio definition are  modifiable due to the potential implications for data already stored.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.update_portfolio_with_http_info(scope, code, update_portfolio_request, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param update_portfolio_request: The updated portfolio definition. (required)
        :type update_portfolio_request: UpdatePortfolioRequest
        :param effective_at: The effective datetime or cut label at which to update the definition. Defaults to the current               LUSID system datetime if not specified.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(Portfolio, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'update_portfolio_request',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method update_portfolio" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['update_portfolio_request']:
            _body_params = _params['update_portfolio_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "Portfolio",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}', 'PUT',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_portfolio_access_metadata(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope to use when updating or inserting the Portfolio Access Metadata Rule.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the access metadata to upsert")], upsert_portfolio_access_metadata_request : Annotated[UpsertPortfolioAccessMetadataRequest, Field(..., description="The Portfolio Access Metadata Rule to update or insert")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs) -> ResourceListOfAccessMetadataValueOf:  # noqa: E501
        """[EARLY ACCESS] UpsertPortfolioAccessMetadata: Upsert a Portfolio Access Metadata Rule associated with specific metadataKey. This creates or updates the data in LUSID.  # noqa: E501

        Update or insert one Portfolio Access Metadata Rule in a single scope. An item will be updated if it already exists  and inserted if it does not.    The response will return the successfully updated or inserted Portfolio Access Metadata Rule or failure message if unsuccessful    It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exists with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_access_metadata(scope, code, metadata_key, upsert_portfolio_access_metadata_request, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope to use when updating or inserting the Portfolio Access Metadata Rule. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param metadata_key: Key of the access metadata to upsert (required)
        :type metadata_key: str
        :param upsert_portfolio_access_metadata_request: The Portfolio Access Metadata Rule to update or insert (required)
        :type upsert_portfolio_access_metadata_request: UpsertPortfolioAccessMetadataRequest
        :param effective_at: The date this rule will effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfAccessMetadataValueOf
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_portfolio_access_metadata_with_http_info(scope, code, metadata_key, upsert_portfolio_access_metadata_request, effective_at, effective_until, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_portfolio_access_metadata_with_http_info(self, scope : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="The scope to use when updating or inserting the Portfolio Access Metadata Rule.")], code : Annotated[constr(strict=True, max_length=64, min_length=1), Field(..., description="Portfolio code")], metadata_key : Annotated[constr(strict=True, max_length=256, min_length=1), Field(..., description="Key of the access metadata to upsert")], upsert_portfolio_access_metadata_request : Annotated[UpsertPortfolioAccessMetadataRequest, Field(..., description="The Portfolio Access Metadata Rule to update or insert")], effective_at : Annotated[Optional[StrictStr], Field(description="The date this rule will effective from")] = None, effective_until : Annotated[Optional[datetime], Field(description="The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] UpsertPortfolioAccessMetadata: Upsert a Portfolio Access Metadata Rule associated with specific metadataKey. This creates or updates the data in LUSID.  # noqa: E501

        Update or insert one Portfolio Access Metadata Rule in a single scope. An item will be updated if it already exists  and inserted if it does not.    The response will return the successfully updated or inserted Portfolio Access Metadata Rule or failure message if unsuccessful    It is important to always check to verify success (or failure).                Multiple rules for a metadataKey can exists with different effective at dates, when resources are accessed the rule that is active for the current time will be fetched  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_access_metadata_with_http_info(scope, code, metadata_key, upsert_portfolio_access_metadata_request, effective_at, effective_until, async_req=True)
        >>> result = thread.get()

        :param scope: The scope to use when updating or inserting the Portfolio Access Metadata Rule. (required)
        :type scope: str
        :param code: Portfolio code (required)
        :type code: str
        :param metadata_key: Key of the access metadata to upsert (required)
        :type metadata_key: str
        :param upsert_portfolio_access_metadata_request: The Portfolio Access Metadata Rule to update or insert (required)
        :type upsert_portfolio_access_metadata_request: UpsertPortfolioAccessMetadataRequest
        :param effective_at: The date this rule will effective from
        :type effective_at: str
        :param effective_until: The effective date until which the Access Metadata is valid. If not supplied this will be valid indefinitely, or until the next 'effectiveAt' date of the Access Metadata
        :type effective_until: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfAccessMetadataValueOf, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'metadata_key',
            'upsert_portfolio_access_metadata_request',
            'effective_at',
            'effective_until'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_portfolio_access_metadata" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['metadata_key']:
            _path_params['metadataKey'] = _params['metadata_key']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('effective_until') is not None:  # noqa: E501
            _query_params.append(('effectiveUntil', _params['effective_until']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['upsert_portfolio_access_metadata_request']:
            _body_params = _params['upsert_portfolio_access_metadata_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfAccessMetadataValueOf",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/metadata/{metadataKey}', 'PUT',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_portfolio_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], request_body : Annotated[Dict[str, ModelProperty], Field(..., description="The properties to be created or updated. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code}, for example               'Portfolio/Manager/Id'.")], **kwargs) -> PortfolioProperties:  # noqa: E501
        """UpsertPortfolioProperties: Upsert portfolio properties  # noqa: E501

        Create or update one or more properties for a particular portfolio. A property is updated if it  already exists and created if it does not. All properties must be from the 'Portfolio' domain.                Properties have an <i>effectiveFrom</i> datetime from which the property is valid, and an <i>effectiveUntil</i>  datetime until which it is valid. Not supplying an <i>effectiveUntil</i> datetime results in the property being  valid indefinitely, or until the next <i>effectiveFrom</i> datetime of the property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_properties(scope, code, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param request_body: The properties to be created or updated. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code}, for example               'Portfolio/Manager/Id'. (required)
        :type request_body: Dict[str, ModelProperty]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioProperties
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_portfolio_properties_with_http_info(scope, code, request_body, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_portfolio_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio. Together with the scope this uniquely identifies the portfolio.")], request_body : Annotated[Dict[str, ModelProperty], Field(..., description="The properties to be created or updated. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code}, for example               'Portfolio/Manager/Id'.")], **kwargs):  # noqa: E501
        """UpsertPortfolioProperties: Upsert portfolio properties  # noqa: E501

        Create or update one or more properties for a particular portfolio. A property is updated if it  already exists and created if it does not. All properties must be from the 'Portfolio' domain.                Properties have an <i>effectiveFrom</i> datetime from which the property is valid, and an <i>effectiveUntil</i>  datetime until which it is valid. Not supplying an <i>effectiveUntil</i> datetime results in the property being  valid indefinitely, or until the next <i>effectiveFrom</i> datetime of the property.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_properties_with_http_info(scope, code, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio. (required)
        :type scope: str
        :param code: The code of the portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param request_body: The properties to be created or updated. Each property in               the request must be keyed by its unique property key. This has the format {domain}/{scope}/{code}, for example               'Portfolio/Manager/Id'. (required)
        :type request_body: Dict[str, ModelProperty]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioProperties, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'request_body'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_portfolio_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['request_body']:
            _body_params = _params['request_body']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioProperties",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/properties', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_portfolio_returns(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], performance_return : Annotated[List[PerformanceReturn], Field(..., description="This contains the Returns which need to be upsert.")], **kwargs) -> UpsertReturnsResponse:  # noqa: E501
        """[EARLY ACCESS] UpsertPortfolioReturns: Upsert Returns  # noqa: E501

        Update or insert returns into the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_returns(scope, code, return_scope, return_code, performance_return, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param performance_return: This contains the Returns which need to be upsert. (required)
        :type performance_return: List[PerformanceReturn]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: UpsertReturnsResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_portfolio_returns_with_http_info(scope, code, return_scope, return_code, performance_return, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_portfolio_returns_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the  Portfolio.")], return_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the Returns.")], return_code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the Returns.")], performance_return : Annotated[List[PerformanceReturn], Field(..., description="This contains the Returns which need to be upsert.")], **kwargs):  # noqa: E501
        """[EARLY ACCESS] UpsertPortfolioReturns: Upsert Returns  # noqa: E501

        Update or insert returns into the specified portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_returns_with_http_info(scope, code, return_scope, return_code, performance_return, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the Portfolio. (required)
        :type scope: str
        :param code: The code of the  Portfolio. (required)
        :type code: str
        :param return_scope: The scope of the Returns. (required)
        :type return_scope: str
        :param return_code: The code of the Returns. (required)
        :type return_code: str
        :param performance_return: This contains the Returns which need to be upsert. (required)
        :type performance_return: List[PerformanceReturn]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(UpsertReturnsResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'return_scope',
            'return_code',
            'performance_return'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_portfolio_returns" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['return_scope']:
            _path_params['returnScope'] = _params['return_scope']
        if _params['return_code']:
            _path_params['returnCode'] = _params['return_code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['performance_return']:
            _body_params = _params['performance_return']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "UpsertReturnsResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/portfolios/{scope}/{code}/returns/{returnScope}/{returnCode}', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))
