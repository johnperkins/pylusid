# coding: utf-8

"""
    LUSID API

    # Introduction  This page documents the [LUSID APIs](https://www.lusid.com/api/swagger), which allows authorised clients to query and update their data within the LUSID platform.  SDKs to interact with the LUSID APIs are available in the following languages and frameworks:  * [C#](https://github.com/finbourne/lusid-sdk-csharp) * [Java](https://github.com/finbourne/lusid-sdk-java) * [JavaScript](https://github.com/finbourne/lusid-sdk-js) * [Python](https://github.com/finbourne/lusid-sdk-python) * [Angular](https://github.com/finbourne/lusid-sdk-angular)  The LUSID platform is made up of a number of sub-applications. You can find the API / swagger documentation by following the links in the table below.   | Application | Description | API / Swagger Documentation | | ----- | ----- | ---- | | LUSID | Open, API-first, developer-friendly investment data platform. | [Swagger](https://www.lusid.com/api/swagger/index.html) | | Web app | User-facing front end for LUSID. | [Swagger](https://www.lusid.com/app/swagger/index.html) | | Scheduler | Automated job scheduler. | [Swagger](https://www.lusid.com/scheduler2/swagger/index.html) | | Insights |Monitoring and troubleshooting service. | [Swagger](https://www.lusid.com/insights/swagger/index.html) | | Identity | Identity management for LUSID (in conjuction with Access) | [Swagger](https://www.lusid.com/identity/swagger/index.html) | | Access | Access control for LUSID (in conjunction with Identity) | [Swagger](https://www.lusid.com/access/swagger/index.html) | | Drive | Secure file repository and manager for collaboration. | [Swagger](https://www.lusid.com/drive/swagger/index.html) | | Luminesce | Data virtualisation service (query data from multiple providers, including LUSID) | [Swagger](https://www.lusid.com/honeycomb/swagger/index.html) | | Notification | Notification service. | [Swagger](https://www.lusid.com/notifications/swagger/index.html) | | Configuration | File store for secrets and other sensitive information. | [Swagger](https://www.lusid.com/configuration/swagger/index.html) |   # Error Codes  | Code|Name|Description | | ---|---|--- | | <a name=\"-10\">-10</a>|Server Configuration Error|  | | <a name=\"-1\">-1</a>|Unknown error|An unexpected error was encountered on our side. | | <a name=\"102\">102</a>|Version Not Found|  | | <a name=\"103\">103</a>|Api Rate Limit Violation|  | | <a name=\"104\">104</a>|Instrument Not Found|  | | <a name=\"105\">105</a>|Property Not Found|  | | <a name=\"106\">106</a>|Portfolio Recursion Depth|  | | <a name=\"108\">108</a>|Group Not Found|  | | <a name=\"109\">109</a>|Portfolio Not Found|  | | <a name=\"110\">110</a>|Property Schema Not Found|  | | <a name=\"111\">111</a>|Portfolio Ancestry Not Found|  | | <a name=\"112\">112</a>|Portfolio With Id Already Exists|  | | <a name=\"113\">113</a>|Orphaned Portfolio|  | | <a name=\"119\">119</a>|Missing Base Claims|  | | <a name=\"121\">121</a>|Property Not Defined|  | | <a name=\"122\">122</a>|Cannot Delete System Property|  | | <a name=\"123\">123</a>|Cannot Modify Immutable Property Field|  | | <a name=\"124\">124</a>|Property Already Exists|  | | <a name=\"125\">125</a>|Invalid Property Life Time|  | | <a name=\"126\">126</a>|Property Constraint Style Excludes Properties|  | | <a name=\"127\">127</a>|Cannot Modify Default Data Type|  | | <a name=\"128\">128</a>|Group Already Exists|  | | <a name=\"129\">129</a>|No Such Data Type|  | | <a name=\"130\">130</a>|Undefined Value For Data Type|  | | <a name=\"131\">131</a>|Unsupported Value Type Defined On Data Type|  | | <a name=\"132\">132</a>|Validation Error|  | | <a name=\"133\">133</a>|Loop Detected In Group Hierarchy|  | | <a name=\"134\">134</a>|Undefined Acceptable Values|  | | <a name=\"135\">135</a>|Sub Group Already Exists|  | | <a name=\"138\">138</a>|Price Source Not Found|  | | <a name=\"139\">139</a>|Analytic Store Not Found|  | | <a name=\"141\">141</a>|Analytic Store Already Exists|  | | <a name=\"143\">143</a>|Client Instrument Already Exists|  | | <a name=\"144\">144</a>|Duplicate In Parameter Set|  | | <a name=\"147\">147</a>|Results Not Found|  | | <a name=\"148\">148</a>|Order Field Not In Result Set|  | | <a name=\"149\">149</a>|Operation Failed|  | | <a name=\"150\">150</a>|Elastic Search Error|  | | <a name=\"151\">151</a>|Invalid Parameter Value|  | | <a name=\"153\">153</a>|Command Processing Failure|  | | <a name=\"154\">154</a>|Entity State Construction Failure|  | | <a name=\"155\">155</a>|Entity Timeline Does Not Exist|  | | <a name=\"156\">156</a>|Concurrency Conflict Failure|  | | <a name=\"157\">157</a>|Invalid Request|  | | <a name=\"158\">158</a>|Event Publish Unknown|  | | <a name=\"159\">159</a>|Event Query Failure|  | | <a name=\"160\">160</a>|Blob Did Not Exist|  | | <a name=\"162\">162</a>|Sub System Request Failure|  | | <a name=\"163\">163</a>|Sub System Configuration Failure|  | | <a name=\"165\">165</a>|Failed To Delete|  | | <a name=\"166\">166</a>|Upsert Client Instrument Failure|  | | <a name=\"167\">167</a>|Illegal As At Interval|  | | <a name=\"168\">168</a>|Illegal Bitemporal Query|  | | <a name=\"169\">169</a>|Invalid Alternate Id|  | | <a name=\"170\">170</a>|Cannot Add Source Portfolio Property Explicitly|  | | <a name=\"171\">171</a>|Entity Already Exists In Group|  | | <a name=\"173\">173</a>|Entity With Id Already Exists|  | | <a name=\"174\">174</a>|Derived Portfolio Details Do Not Exist|  | | <a name=\"175\">175</a>|Entity Not In Group|  | | <a name=\"176\">176</a>|Portfolio With Name Already Exists|  | | <a name=\"177\">177</a>|Invalid Transactions|  | | <a name=\"178\">178</a>|Reference Portfolio Not Found|  | | <a name=\"179\">179</a>|Duplicate Id|  | | <a name=\"180\">180</a>|Command Retrieval Failure|  | | <a name=\"181\">181</a>|Data Filter Application Failure|  | | <a name=\"182\">182</a>|Search Failed|  | | <a name=\"183\">183</a>|Movements Engine Configuration Key Failure|  | | <a name=\"184\">184</a>|Fx Rate Source Not Found|  | | <a name=\"185\">185</a>|Accrual Source Not Found|  | | <a name=\"186\">186</a>|Access Denied|  | | <a name=\"187\">187</a>|Invalid Identity Token|  | | <a name=\"188\">188</a>|Invalid Request Headers|  | | <a name=\"189\">189</a>|Price Not Found|  | | <a name=\"190\">190</a>|Invalid Sub Holding Keys Provided|  | | <a name=\"191\">191</a>|Duplicate Sub Holding Keys Provided|  | | <a name=\"192\">192</a>|Cut Definition Not Found|  | | <a name=\"193\">193</a>|Cut Definition Invalid|  | | <a name=\"194\">194</a>|Time Variant Property Deletion Date Unspecified|  | | <a name=\"195\">195</a>|Perpetual Property Deletion Date Specified|  | | <a name=\"196\">196</a>|Time Variant Property Upsert Date Unspecified|  | | <a name=\"197\">197</a>|Perpetual Property Upsert Date Specified|  | | <a name=\"200\">200</a>|Invalid Unit For Data Type|  | | <a name=\"201\">201</a>|Invalid Type For Data Type|  | | <a name=\"202\">202</a>|Invalid Value For Data Type|  | | <a name=\"203\">203</a>|Unit Not Defined For Data Type|  | | <a name=\"204\">204</a>|Units Not Supported On Data Type|  | | <a name=\"205\">205</a>|Cannot Specify Units On Data Type|  | | <a name=\"206\">206</a>|Unit Schema Inconsistent With Data Type|  | | <a name=\"207\">207</a>|Unit Definition Not Specified|  | | <a name=\"208\">208</a>|Duplicate Unit Definitions Specified|  | | <a name=\"209\">209</a>|Invalid Units Definition|  | | <a name=\"210\">210</a>|Invalid Instrument Identifier Unit|  | | <a name=\"211\">211</a>|Holdings Adjustment Does Not Exist|  | | <a name=\"212\">212</a>|Could Not Build Excel Url|  | | <a name=\"213\">213</a>|Could Not Get Excel Version|  | | <a name=\"214\">214</a>|Instrument By Code Not Found|  | | <a name=\"215\">215</a>|Entity Schema Does Not Exist|  | | <a name=\"216\">216</a>|Feature Not Supported On Portfolio Type|  | | <a name=\"217\">217</a>|Quote Not Found|  | | <a name=\"218\">218</a>|Invalid Quote Identifier|  | | <a name=\"219\">219</a>|Invalid Metric For Data Type|  | | <a name=\"220\">220</a>|Invalid Instrument Definition|  | | <a name=\"221\">221</a>|Instrument Upsert Failure|  | | <a name=\"222\">222</a>|Reference Portfolio Request Not Supported|  | | <a name=\"223\">223</a>|Transaction Portfolio Request Not Supported|  | | <a name=\"224\">224</a>|Invalid Property Value Assignment|  | | <a name=\"230\">230</a>|Transaction Type Not Found|  | | <a name=\"231\">231</a>|Transaction Type Duplication|  | | <a name=\"232\">232</a>|Portfolio Does Not Exist At Given Date|  | | <a name=\"233\">233</a>|Query Parser Failure|  | | <a name=\"234\">234</a>|Duplicate Constituent|  | | <a name=\"235\">235</a>|Unresolved Instrument Constituent|  | | <a name=\"236\">236</a>|Unresolved Instrument In Transition|  | | <a name=\"237\">237</a>|Missing Side Definitions|  | | <a name=\"299\">299</a>|Invalid Recipe|  | | <a name=\"300\">300</a>|Missing Recipe|  | | <a name=\"301\">301</a>|Dependencies|  | | <a name=\"304\">304</a>|Portfolio Preprocess Failure|  | | <a name=\"310\">310</a>|Valuation Engine Failure|  | | <a name=\"311\">311</a>|Task Factory Failure|  | | <a name=\"312\">312</a>|Task Evaluation Failure|  | | <a name=\"313\">313</a>|Task Generation Failure|  | | <a name=\"314\">314</a>|Engine Configuration Failure|  | | <a name=\"315\">315</a>|Model Specification Failure|  | | <a name=\"320\">320</a>|Market Data Key Failure|  | | <a name=\"321\">321</a>|Market Resolver Failure|  | | <a name=\"322\">322</a>|Market Data Failure|  | | <a name=\"330\">330</a>|Curve Failure|  | | <a name=\"331\">331</a>|Volatility Surface Failure|  | | <a name=\"332\">332</a>|Volatility Cube Failure|  | | <a name=\"350\">350</a>|Instrument Failure|  | | <a name=\"351\">351</a>|Cash Flows Failure|  | | <a name=\"352\">352</a>|Reference Data Failure|  | | <a name=\"360\">360</a>|Aggregation Failure|  | | <a name=\"361\">361</a>|Aggregation Measure Failure|  | | <a name=\"370\">370</a>|Result Retrieval Failure|  | | <a name=\"371\">371</a>|Result Processing Failure|  | | <a name=\"372\">372</a>|Vendor Result Processing Failure|  | | <a name=\"373\">373</a>|Vendor Result Mapping Failure|  | | <a name=\"374\">374</a>|Vendor Library Unauthorised|  | | <a name=\"375\">375</a>|Vendor Connectivity Error|  | | <a name=\"376\">376</a>|Vendor Interface Error|  | | <a name=\"377\">377</a>|Vendor Pricing Failure|  | | <a name=\"378\">378</a>|Vendor Translation Failure|  | | <a name=\"379\">379</a>|Vendor Key Mapping Failure|  | | <a name=\"380\">380</a>|Vendor Reflection Failure|  | | <a name=\"381\">381</a>|Vendor Process Failure|  | | <a name=\"382\">382</a>|Vendor System Failure|  | | <a name=\"390\">390</a>|Attempt To Upsert Duplicate Quotes|  | | <a name=\"391\">391</a>|Corporate Action Source Does Not Exist|  | | <a name=\"392\">392</a>|Corporate Action Source Already Exists|  | | <a name=\"393\">393</a>|Instrument Identifier Already In Use|  | | <a name=\"394\">394</a>|Properties Not Found|  | | <a name=\"395\">395</a>|Batch Operation Aborted|  | | <a name=\"400\">400</a>|Invalid Iso4217 Currency Code|  | | <a name=\"401\">401</a>|Cannot Assign Instrument Identifier To Currency|  | | <a name=\"402\">402</a>|Cannot Assign Currency Identifier To Non Currency|  | | <a name=\"403\">403</a>|Currency Instrument Cannot Be Deleted|  | | <a name=\"404\">404</a>|Currency Instrument Cannot Have Economic Definition|  | | <a name=\"405\">405</a>|Currency Instrument Cannot Have Lookthrough Portfolio|  | | <a name=\"406\">406</a>|Cannot Create Currency Instrument With Multiple Identifiers|  | | <a name=\"407\">407</a>|Specified Currency Is Undefined|  | | <a name=\"410\">410</a>|Index Does Not Exist|  | | <a name=\"411\">411</a>|Sort Field Does Not Exist|  | | <a name=\"413\">413</a>|Negative Pagination Parameters|  | | <a name=\"414\">414</a>|Invalid Search Syntax|  | | <a name=\"415\">415</a>|Filter Execution Timeout|  | | <a name=\"420\">420</a>|Side Definition Inconsistent|  | | <a name=\"450\">450</a>|Invalid Quote Access Metadata Rule|  | | <a name=\"451\">451</a>|Access Metadata Not Found|  | | <a name=\"452\">452</a>|Invalid Access Metadata Identifier|  | | <a name=\"460\">460</a>|Standard Resource Not Found|  | | <a name=\"461\">461</a>|Standard Resource Conflict|  | | <a name=\"462\">462</a>|Calendar Not Found|  | | <a name=\"463\">463</a>|Date In A Calendar Not Found|  | | <a name=\"464\">464</a>|Invalid Date Source Data|  | | <a name=\"465\">465</a>|Invalid Timezone|  | | <a name=\"601\">601</a>|Person Identifier Already In Use|  | | <a name=\"602\">602</a>|Person Not Found|  | | <a name=\"603\">603</a>|Cannot Set Identifier|  | | <a name=\"617\">617</a>|Invalid Recipe Specification In Request|  | | <a name=\"618\">618</a>|Inline Recipe Deserialisation Failure|  | | <a name=\"619\">619</a>|Identifier Types Not Set For Entity|  | | <a name=\"620\">620</a>|Cannot Delete All Client Defined Identifiers|  | | <a name=\"650\">650</a>|The Order requested was not found.|  | | <a name=\"654\">654</a>|The Allocation requested was not found.|  | | <a name=\"655\">655</a>|Cannot build the fx forward target with the given holdings.|  | | <a name=\"656\">656</a>|Group does not contain expected entities.|  | | <a name=\"665\">665</a>|Destination directory not found|  | | <a name=\"667\">667</a>|Relation definition already exists|  | | <a name=\"672\">672</a>|Could not retrieve file contents|  | | <a name=\"673\">673</a>|Missing entitlements for entities in Group|  | | <a name=\"674\">674</a>|Next Best Action not found|  | | <a name=\"676\">676</a>|Relation definition not defined|  | | <a name=\"677\">677</a>|Invalid entity identifier for relation|  | | <a name=\"681\">681</a>|Sorting by specified field not supported|One or more of the provided fields to order by were either invalid or not supported. | | <a name=\"682\">682</a>|Too many fields to sort by|The number of fields to sort the data by exceeds the number allowed by the endpoint | | <a name=\"684\">684</a>|Sequence Not Found|  | | <a name=\"685\">685</a>|Sequence Already Exists|  | | <a name=\"686\">686</a>|Non-cycling sequence has been exhausted|  | | <a name=\"687\">687</a>|Legal Entity Identifier Already In Use|  | | <a name=\"688\">688</a>|Legal Entity Not Found|  | | <a name=\"689\">689</a>|The supplied pagination token is invalid|  | | <a name=\"690\">690</a>|Property Type Is Not Supported|  | | <a name=\"691\">691</a>|Multiple Tax-lots For Currency Type Is Not Supported|  | | <a name=\"692\">692</a>|This endpoint does not support impersonation|  | | <a name=\"693\">693</a>|Entity type is not supported for Relationship|  | | <a name=\"694\">694</a>|Relationship Validation Failure|  | | <a name=\"695\">695</a>|Relationship Not Found|  | | <a name=\"697\">697</a>|Derived Property Formula No Longer Valid|  | | <a name=\"698\">698</a>|Story is not available|  | | <a name=\"703\">703</a>|Corporate Action Does Not Exist|  | | <a name=\"720\">720</a>|The provided sort and filter combination is not valid|  | | <a name=\"721\">721</a>|A2B generation failed|  | | <a name=\"722\">722</a>|Aggregated Return Calculation Failure|  | | <a name=\"723\">723</a>|Custom Entity Definition Identifier Already In Use|  | | <a name=\"724\">724</a>|Custom Entity Definition Not Found|  | | <a name=\"725\">725</a>|The Placement requested was not found.|  | | <a name=\"726\">726</a>|The Execution requested was not found.|  | | <a name=\"727\">727</a>|The Block requested was not found.|  | | <a name=\"728\">728</a>|The Participation requested was not found.|  | | <a name=\"729\">729</a>|The Package requested was not found.|  | | <a name=\"730\">730</a>|The OrderInstruction requested was not found.|  | | <a name=\"732\">732</a>|Custom Entity not found.|  | | <a name=\"733\">733</a>|Custom Entity Identifier already in use.|  | | <a name=\"735\">735</a>|Calculation Failed.|  | | <a name=\"736\">736</a>|An expected key on HttpResponse is missing.|  | | <a name=\"737\">737</a>|A required fee detail is missing.|  | | <a name=\"738\">738</a>|Zero rows were returned from Luminesce|  | | <a name=\"739\">739</a>|Provided Weekend Mask was invalid|  | | <a name=\"742\">742</a>|Custom Entity fields do not match the definition|  | | <a name=\"746\">746</a>|The provided sequence is not valid.|  | | <a name=\"751\">751</a>|The type of the Custom Entity is different than the type provided in the definition.|  | | <a name=\"752\">752</a>|Luminesce process returned an error.|  | | <a name=\"753\">753</a>|File name or content incompatible with operation.|  | | <a name=\"755\">755</a>|Schema of response from Drive is not as expected.|  | | <a name=\"757\">757</a>|Schema of response from Luminesce is not as expected.|  | | <a name=\"758\">758</a>|Luminesce timed out.|  | | <a name=\"763\">763</a>|Invalid Lusid Entity Identifier Unit|  | | <a name=\"768\">768</a>|Fee rule not found.|  | | <a name=\"769\">769</a>|Cannot update the base currency of a portfolio with transactions loaded|  | | <a name=\"771\">771</a>|Transaction configuration source not found|  | | <a name=\"774\">774</a>|Compliance rule not found.|  | | <a name=\"775\">775</a>|Fund accounting document cannot be processed.|  | | <a name=\"778\">778</a>|Unable to look up FX rate from trade ccy to portfolio ccy for some of the trades.|  | | <a name=\"782\">782</a>|The Property definition dataType is not matching the derivation formula dataType|  | | <a name=\"783\">783</a>|The Property definition domain is not supported for derived properties|  | | <a name=\"788\">788</a>|Compliance run not found failure.|  | | <a name=\"790\">790</a>|Custom Entity has missing or invalid identifiers|  | | <a name=\"791\">791</a>|Custom Entity definition already exists|  | | <a name=\"792\">792</a>|Compliance PropertyKey is missing.|  | | <a name=\"793\">793</a>|Compliance Criteria Value for matching is missing.|  | | <a name=\"795\">795</a>|Cannot delete identifier definition|  | | <a name=\"796\">796</a>|Tax rule set not found.|  | | <a name=\"797\">797</a>|A tax rule set with this id already exists.|  | | <a name=\"798\">798</a>|Multiple rule sets for the same property key are applicable.|  | | <a name=\"800\">800</a>|Can not upsert an instrument event of this type.|  | | <a name=\"801\">801</a>|The instrument event does not exist.|  | | <a name=\"802\">802</a>|The Instrument event is missing salient information.|  | | <a name=\"803\">803</a>|The Instrument event could not be processed.|  | | <a name=\"804\">804</a>|Some data requested does not follow the order graph assumptions.|  | | <a name=\"811\">811</a>|A price could not be found for an order.|  | | <a name=\"812\">812</a>|A price could not be found for an allocation.|  | | <a name=\"813\">813</a>|Chart of Accounts not found.|  | | <a name=\"814\">814</a>|Account not found.|  |   # noqa: E501

    The version of the OpenAPI document: 0.11.4956
    Contact: info@finbourne.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import re  # noqa: F401

from pydantic import validate_arguments, ValidationError
from typing_extensions import Annotated

from datetime import datetime

from pydantic import Field, StrictBool, StrictInt, StrictStr, constr, validator

from typing import Dict, List, Optional

from pylusid.models.adjust_holding import AdjustHolding
from pylusid.models.adjust_holding_request import AdjustHoldingRequest
from pylusid.models.bucketed_cash_flow_request import BucketedCashFlowRequest
from pylusid.models.bucketed_cash_flow_response import BucketedCashFlowResponse
from pylusid.models.create_portfolio_details import CreatePortfolioDetails
from pylusid.models.create_transaction_portfolio_request import CreateTransactionPortfolioRequest
from pylusid.models.deleted_entity_response import DeletedEntityResponse
from pylusid.models.holdings_adjustment import HoldingsAdjustment
from pylusid.models.lusid_trade_ticket import LusidTradeTicket
from pylusid.models.operation import Operation
from pylusid.models.perpetual_property import PerpetualProperty
from pylusid.models.portfolio import Portfolio
from pylusid.models.portfolio_details import PortfolioDetails
from pylusid.models.resource_list_of_change_history import ResourceListOfChangeHistory
from pylusid.models.resource_list_of_holdings_adjustment_header import ResourceListOfHoldingsAdjustmentHeader
from pylusid.models.resource_list_of_instrument_cash_flow import ResourceListOfInstrumentCashFlow
from pylusid.models.resource_list_of_portfolio_cash_flow import ResourceListOfPortfolioCashFlow
from pylusid.models.resource_list_of_portfolio_cash_ladder import ResourceListOfPortfolioCashLadder
from pylusid.models.resource_list_of_transaction import ResourceListOfTransaction
from pylusid.models.transaction_query_parameters import TransactionQueryParameters
from pylusid.models.transaction_request import TransactionRequest
from pylusid.models.upsert_portfolio_transactions_response import UpsertPortfolioTransactionsResponse
from pylusid.models.upsert_transaction_properties_response import UpsertTransactionPropertiesResponse
from pylusid.models.versioned_resource_list_of_a2_b_data_record import VersionedResourceListOfA2BDataRecord
from pylusid.models.versioned_resource_list_of_a2_b_movement_record import VersionedResourceListOfA2BMovementRecord
from pylusid.models.versioned_resource_list_of_output_transaction import VersionedResourceListOfOutputTransaction
from pylusid.models.versioned_resource_list_of_portfolio_holding import VersionedResourceListOfPortfolioHolding
from pylusid.models.versioned_resource_list_of_transaction import VersionedResourceListOfTransaction
from pylusid.models.versioned_resource_list_with_warnings_of_portfolio_holding import VersionedResourceListWithWarningsOfPortfolioHolding

from pylusid.api_client import ApiClient
from pylusid.exceptions import (  # noqa: F401
    ApiTypeError,
    ApiValueError
)


class TransactionPortfoliosApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient.get_default()
        self.api_client = api_client

    @validate_arguments
    def adjust_holdings(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[StrictStr, Field(..., description="The effective datetime or cut label at which the holdings should be set to the provided targets.")], adjust_holding_request : Annotated[List[AdjustHoldingRequest], Field(..., description="The selected set of holdings to adjust to the provided targets for the              transaction portfolio.")], reconciliation_methods : Annotated[Optional[List[StrictStr]], Field(description="Optional parameter for specifying a reconciliation method: e.g. FxForward.")] = None, **kwargs) -> AdjustHolding:  # noqa: E501
        """AdjustHoldings: Adjust holdings  # noqa: E501

        Adjust one or more holdings of the specified transaction portfolio to the provided targets. LUSID will  automatically construct adjustment transactions to ensure that the holdings which have been adjusted are  always set to the provided targets for the specified effective datetime. Read more about the difference between  adjusting and setting holdings here https://support.lusid.com/how-do-i-adjust-my-holdings.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.adjust_holdings(scope, code, effective_at, adjust_holding_request, reconciliation_methods, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which the holdings should be set to the provided targets. (required)
        :type effective_at: str
        :param adjust_holding_request: The selected set of holdings to adjust to the provided targets for the              transaction portfolio. (required)
        :type adjust_holding_request: List[AdjustHoldingRequest]
        :param reconciliation_methods: Optional parameter for specifying a reconciliation method: e.g. FxForward.
        :type reconciliation_methods: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: AdjustHolding
        """
        kwargs['_return_http_data_only'] = True
        return self.adjust_holdings_with_http_info(scope, code, effective_at, adjust_holding_request, reconciliation_methods, **kwargs)  # noqa: E501

    @validate_arguments
    def adjust_holdings_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[StrictStr, Field(..., description="The effective datetime or cut label at which the holdings should be set to the provided targets.")], adjust_holding_request : Annotated[List[AdjustHoldingRequest], Field(..., description="The selected set of holdings to adjust to the provided targets for the              transaction portfolio.")], reconciliation_methods : Annotated[Optional[List[StrictStr]], Field(description="Optional parameter for specifying a reconciliation method: e.g. FxForward.")] = None, **kwargs):  # noqa: E501
        """AdjustHoldings: Adjust holdings  # noqa: E501

        Adjust one or more holdings of the specified transaction portfolio to the provided targets. LUSID will  automatically construct adjustment transactions to ensure that the holdings which have been adjusted are  always set to the provided targets for the specified effective datetime. Read more about the difference between  adjusting and setting holdings here https://support.lusid.com/how-do-i-adjust-my-holdings.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.adjust_holdings_with_http_info(scope, code, effective_at, adjust_holding_request, reconciliation_methods, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which the holdings should be set to the provided targets. (required)
        :type effective_at: str
        :param adjust_holding_request: The selected set of holdings to adjust to the provided targets for the              transaction portfolio. (required)
        :type adjust_holding_request: List[AdjustHoldingRequest]
        :param reconciliation_methods: Optional parameter for specifying a reconciliation method: e.g. FxForward.
        :type reconciliation_methods: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(AdjustHolding, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'adjust_holding_request',
            'reconciliation_methods'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method adjust_holdings" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('reconciliation_methods') is not None:  # noqa: E501
            _query_params.append(('reconciliationMethods', _params['reconciliation_methods']))
            _collection_formats['reconciliationMethods'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['adjust_holding_request']:
            _body_params = _params['adjust_holding_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "AdjustHolding",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdings', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def build_transactions(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_query_parameters : Annotated[TransactionQueryParameters, Field(..., description="The query queryParameters which control how the output transactions are built.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to build the transactions. Defaults to return the latest              version of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto              the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or              \"Transaction/strategy/quantsignal\".")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to BuildTransactions.")] = None, **kwargs) -> VersionedResourceListOfOutputTransaction:  # noqa: E501
        """BuildTransactions: Build transactions  # noqa: E501

        Builds and returns all transactions that affect the holdings of a portfolio over a given interval of  effective time into a set of output transactions. This includes transactions automatically generated by  LUSID such as holding adjustments.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.build_transactions(scope, code, transaction_query_parameters, as_at, filter, property_keys, limit, page, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_query_parameters: The query queryParameters which control how the output transactions are built. (required)
        :type transaction_query_parameters: TransactionQueryParameters
        :param as_at: The asAt datetime at which to build the transactions. Defaults to return the latest              version of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto              the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or              \"Transaction/strategy/quantsignal\".
        :type property_keys: List[str]
        :param limit: When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.
        :type limit: int
        :param page: The pagination token to use to continue listing transactions from a previous call to BuildTransactions.
        :type page: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfOutputTransaction
        """
        kwargs['_return_http_data_only'] = True
        return self.build_transactions_with_http_info(scope, code, transaction_query_parameters, as_at, filter, property_keys, limit, page, **kwargs)  # noqa: E501

    @validate_arguments
    def build_transactions_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_query_parameters : Annotated[TransactionQueryParameters, Field(..., description="The query queryParameters which control how the output transactions are built.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to build the transactions. Defaults to return the latest              version of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto              the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or              \"Transaction/strategy/quantsignal\".")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to BuildTransactions.")] = None, **kwargs):  # noqa: E501
        """BuildTransactions: Build transactions  # noqa: E501

        Builds and returns all transactions that affect the holdings of a portfolio over a given interval of  effective time into a set of output transactions. This includes transactions automatically generated by  LUSID such as holding adjustments.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.build_transactions_with_http_info(scope, code, transaction_query_parameters, as_at, filter, property_keys, limit, page, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_query_parameters: The query queryParameters which control how the output transactions are built. (required)
        :type transaction_query_parameters: TransactionQueryParameters
        :param as_at: The asAt datetime at which to build the transactions. Defaults to return the latest              version of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Transaction\" domain to decorate onto              the transactions. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or              \"Transaction/strategy/quantsignal\".
        :type property_keys: List[str]
        :param limit: When paginating, limit the number of returned results to this many. Defaults to 100 if not specified.
        :type limit: int
        :param page: The pagination token to use to continue listing transactions from a previous call to BuildTransactions.
        :type page: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfOutputTransaction, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'transaction_query_parameters',
            'as_at',
            'filter',
            'property_keys',
            'limit',
            'page'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method build_transactions" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['transaction_query_parameters']:
            _body_params = _params['transaction_query_parameters']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfOutputTransaction",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions/$build', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def cancel_adjust_holdings(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[StrictStr, Field(..., description="The effective datetime or cut label at which the holding adjustments should be undone.")], **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """CancelAdjustHoldings: Cancel adjust holdings  # noqa: E501

        Cancel all previous holding adjustments made on the specified transaction portfolio for a specified effective  datetime. This should be used to undo holding adjustments made via set holdings or adjust holdings.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.cancel_adjust_holdings(scope, code, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which the holding adjustments should be undone. (required)
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.cancel_adjust_holdings_with_http_info(scope, code, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def cancel_adjust_holdings_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[StrictStr, Field(..., description="The effective datetime or cut label at which the holding adjustments should be undone.")], **kwargs):  # noqa: E501
        """CancelAdjustHoldings: Cancel adjust holdings  # noqa: E501

        Cancel all previous holding adjustments made on the specified transaction portfolio for a specified effective  datetime. This should be used to undo holding adjustments made via set holdings or adjust holdings.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.cancel_adjust_holdings_with_http_info(scope, code, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which the holding adjustments should be undone. (required)
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method cancel_adjust_holdings" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdings', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def cancel_transactions(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_ids : Annotated[List[StrictStr], Field(..., description="The IDs of the transactions to cancel.")], **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """CancelTransactions: Cancel transactions  # noqa: E501

        Cancel one or more transactions from the transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.cancel_transactions(scope, code, transaction_ids, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_ids: The IDs of the transactions to cancel. (required)
        :type transaction_ids: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.cancel_transactions_with_http_info(scope, code, transaction_ids, **kwargs)  # noqa: E501

    @validate_arguments
    def cancel_transactions_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_ids : Annotated[List[StrictStr], Field(..., description="The IDs of the transactions to cancel.")], **kwargs):  # noqa: E501
        """CancelTransactions: Cancel transactions  # noqa: E501

        Cancel one or more transactions from the transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.cancel_transactions_with_http_info(scope, code, transaction_ids, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_ids: The IDs of the transactions to cancel. (required)
        :type transaction_ids: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'transaction_ids'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method cancel_transactions" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('transaction_ids') is not None:  # noqa: E501
            _query_params.append(('transactionIds', _params['transaction_ids']))
            _collection_formats['transactionIds'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def create_portfolio(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope in which to create the transaction portfolio.")], create_transaction_portfolio_request : Annotated[CreateTransactionPortfolioRequest, Field(..., description="The definition of the transaction portfolio.")], **kwargs) -> Portfolio:  # noqa: E501
        """CreatePortfolio: Create portfolio  # noqa: E501

        Create a transaction portfolio in a particular scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_portfolio(scope, create_transaction_portfolio_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope in which to create the transaction portfolio. (required)
        :type scope: str
        :param create_transaction_portfolio_request: The definition of the transaction portfolio. (required)
        :type create_transaction_portfolio_request: CreateTransactionPortfolioRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: Portfolio
        """
        kwargs['_return_http_data_only'] = True
        return self.create_portfolio_with_http_info(scope, create_transaction_portfolio_request, **kwargs)  # noqa: E501

    @validate_arguments
    def create_portfolio_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope in which to create the transaction portfolio.")], create_transaction_portfolio_request : Annotated[CreateTransactionPortfolioRequest, Field(..., description="The definition of the transaction portfolio.")], **kwargs):  # noqa: E501
        """CreatePortfolio: Create portfolio  # noqa: E501

        Create a transaction portfolio in a particular scope.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_portfolio_with_http_info(scope, create_transaction_portfolio_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope in which to create the transaction portfolio. (required)
        :type scope: str
        :param create_transaction_portfolio_request: The definition of the transaction portfolio. (required)
        :type create_transaction_portfolio_request: CreateTransactionPortfolioRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(Portfolio, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'create_transaction_portfolio_request'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_portfolio" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['create_transaction_portfolio_request']:
            _body_params = _params['create_transaction_portfolio_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            201: "Portfolio",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def create_trade_ticket(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], lusid_trade_ticket : Annotated[Optional[LusidTradeTicket], Field(description="the trade ticket to upsert")] = None, **kwargs) -> LusidTradeTicket:  # noqa: E501
        """[EXPERIMENTAL] CreateTradeTicket: Create Trade Ticket  # noqa: E501

        Upsert a trade ticket. This is broadly equivalent to a singular call to upsert an instrument, then a counterparty and finally  a transaction that makes use of the two. It can be viewed as a utility function or part of a workflow more familiar to users  with OTC systems than flow and equity trading ones.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_trade_ticket(scope, code, lusid_trade_ticket, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param lusid_trade_ticket: the trade ticket to upsert
        :type lusid_trade_ticket: LusidTradeTicket
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: LusidTradeTicket
        """
        kwargs['_return_http_data_only'] = True
        return self.create_trade_ticket_with_http_info(scope, code, lusid_trade_ticket, **kwargs)  # noqa: E501

    @validate_arguments
    def create_trade_ticket_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], lusid_trade_ticket : Annotated[Optional[LusidTradeTicket], Field(description="the trade ticket to upsert")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] CreateTradeTicket: Create Trade Ticket  # noqa: E501

        Upsert a trade ticket. This is broadly equivalent to a singular call to upsert an instrument, then a counterparty and finally  a transaction that makes use of the two. It can be viewed as a utility function or part of a workflow more familiar to users  with OTC systems than flow and equity trading ones.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_trade_ticket_with_http_info(scope, code, lusid_trade_ticket, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param lusid_trade_ticket: the trade ticket to upsert
        :type lusid_trade_ticket: LusidTradeTicket
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(LusidTradeTicket, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'lusid_trade_ticket'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_trade_ticket" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['lusid_trade_ticket']:
            _body_params = _params['lusid_trade_ticket']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "LusidTradeTicket",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/$tradeticket', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def delete_properties_from_transaction(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_id : Annotated[Optional[StrictStr], Field(..., description="The unique ID of the transaction from which to delete properties.")], property_keys : Annotated[List[StrictStr], Field(..., description="The property keys of the properties to delete.              These must be from the \"Transaction\" domain and have the format {domain}/{scope}/{code}, for example              \"Transaction/strategy/quantsignal\".")], **kwargs) -> DeletedEntityResponse:  # noqa: E501
        """DeletePropertiesFromTransaction: Delete properties from transaction  # noqa: E501

        Delete one or more properties from a single transaction in a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_properties_from_transaction(scope, code, transaction_id, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_id: The unique ID of the transaction from which to delete properties. (required)
        :type transaction_id: str
        :param property_keys: The property keys of the properties to delete.              These must be from the \"Transaction\" domain and have the format {domain}/{scope}/{code}, for example              \"Transaction/strategy/quantsignal\". (required)
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: DeletedEntityResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.delete_properties_from_transaction_with_http_info(scope, code, transaction_id, property_keys, **kwargs)  # noqa: E501

    @validate_arguments
    def delete_properties_from_transaction_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_id : Annotated[Optional[StrictStr], Field(..., description="The unique ID of the transaction from which to delete properties.")], property_keys : Annotated[List[StrictStr], Field(..., description="The property keys of the properties to delete.              These must be from the \"Transaction\" domain and have the format {domain}/{scope}/{code}, for example              \"Transaction/strategy/quantsignal\".")], **kwargs):  # noqa: E501
        """DeletePropertiesFromTransaction: Delete properties from transaction  # noqa: E501

        Delete one or more properties from a single transaction in a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.delete_properties_from_transaction_with_http_info(scope, code, transaction_id, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_id: The unique ID of the transaction from which to delete properties. (required)
        :type transaction_id: str
        :param property_keys: The property keys of the properties to delete.              These must be from the \"Transaction\" domain and have the format {domain}/{scope}/{code}, for example              \"Transaction/strategy/quantsignal\". (required)
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(DeletedEntityResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'transaction_id',
            'property_keys'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_properties_from_transaction" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['transaction_id']:
            _path_params['transactionId'] = _params['transaction_id']

        # process the query parameters
        _query_params = []
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "DeletedEntityResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions/{transactionId}/properties', 'DELETE',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_a2_b_data(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio to retrieve the A2B report for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs) -> VersionedResourceListOfA2BDataRecord:  # noqa: E501
        """GetA2BData: Get A2B data  # noqa: E501

        Get an A2B report for the given portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_a2_b_data(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio to retrieve the A2B report for. (required)
        :type scope: str
        :param code: The code of the portfolio to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param property_keys: A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".
        :type property_keys: List[str]
        :param filter: Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfA2BDataRecord
        """
        kwargs['_return_http_data_only'] = True
        return self.get_a2_b_data_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, **kwargs)  # noqa: E501

    @validate_arguments
    def get_a2_b_data_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio to retrieve the A2B report for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs):  # noqa: E501
        """GetA2BData: Get A2B data  # noqa: E501

        Get an A2B report for the given portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_a2_b_data_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio to retrieve the A2B report for. (required)
        :type scope: str
        :param code: The code of the portfolio to retrieve the A2B report for. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param property_keys: A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".
        :type property_keys: List[str]
        :param filter: Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfA2BDataRecord, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_effective_at',
            'to_effective_at',
            'as_at',
            'recipe_id_scope',
            'recipe_id_code',
            'property_keys',
            'filter'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_a2_b_data" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfA2BDataRecord",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/a2b', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_a2_b_movements(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio to retrieve the A2B movement report for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio to retrieve the A2B movement report for. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs) -> VersionedResourceListOfA2BMovementRecord:  # noqa: E501
        """GetA2BMovements: Get an A2B report at the movement level for the given portfolio.  # noqa: E501

        Get an A2B report at the movement level for the given portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_a2_b_movements(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio to retrieve the A2B movement report for. (required)
        :type scope: str
        :param code: The code of the portfolio to retrieve the A2B movement report for. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param property_keys: A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".
        :type property_keys: List[str]
        :param filter: Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfA2BMovementRecord
        """
        kwargs['_return_http_data_only'] = True
        return self.get_a2_b_movements_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, **kwargs)  # noqa: E501

    @validate_arguments
    def get_a2_b_movements_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the portfolio to retrieve the A2B movement report for.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the portfolio to retrieve the A2B movement report for. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.")] = None, **kwargs):  # noqa: E501
        """GetA2BMovements: Get an A2B report at the movement level for the given portfolio.  # noqa: E501

        Get an A2B report at the movement level for the given portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_a2_b_movements_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, recipe_id_scope, recipe_id_code, property_keys, filter, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the portfolio to retrieve the A2B movement report for. (required)
        :type scope: str
        :param code: The code of the portfolio to retrieve the A2B movement report for. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param property_keys: A list of property keys from the \"Instrument\" domain to decorate onto              the results. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\".
        :type property_keys: List[str]
        :param filter: Expression to filter the result set.              Read more about filtering results from LUSID here https://support.lusid.com/filtering-results-from-lusid.
        :type filter: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfA2BMovementRecord, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_effective_at',
            'to_effective_at',
            'as_at',
            'recipe_id_scope',
            'recipe_id_code',
            'property_keys',
            'filter'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_a2_b_movements" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfA2BMovementRecord",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/a2bmovements', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_bucketed_cash_flows(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies the portfolio.")], bucketed_cash_flow_request : Annotated[Optional[BucketedCashFlowRequest], Field(description="Request specifying the bucketing of cashflows")] = None, **kwargs) -> BucketedCashFlowResponse:  # noqa: E501
        """[EXPERIMENTAL] GetBucketedCashFlows: Get bucketed cash flows from a list of portfolios  # noqa: E501

        We bucket/aggregate a transaction portfolio's instruments by date or tenor specified in the request.  The cashflows are grouped by both instrumentId and currency.                If you want transactional level cashflow, please use the 'GetUpsertableCashFlows' endpoint.  If you want instrument cashflow, please use the 'GetPortfolioCashFlows' endpoint.  Note that these endpoints do not apply bucketing.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_bucketed_cash_flows(scope, code, bucketed_cash_flow_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param bucketed_cash_flow_request: Request specifying the bucketing of cashflows
        :type bucketed_cash_flow_request: BucketedCashFlowRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: BucketedCashFlowResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.get_bucketed_cash_flows_with_http_info(scope, code, bucketed_cash_flow_request, **kwargs)  # noqa: E501

    @validate_arguments
    def get_bucketed_cash_flows_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies the portfolio.")], bucketed_cash_flow_request : Annotated[Optional[BucketedCashFlowRequest], Field(description="Request specifying the bucketing of cashflows")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] GetBucketedCashFlows: Get bucketed cash flows from a list of portfolios  # noqa: E501

        We bucket/aggregate a transaction portfolio's instruments by date or tenor specified in the request.  The cashflows are grouped by both instrumentId and currency.                If you want transactional level cashflow, please use the 'GetUpsertableCashFlows' endpoint.  If you want instrument cashflow, please use the 'GetPortfolioCashFlows' endpoint.  Note that these endpoints do not apply bucketing.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_bucketed_cash_flows_with_http_info(scope, code, bucketed_cash_flow_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies the portfolio. (required)
        :type code: str
        :param bucketed_cash_flow_request: Request specifying the bucketing of cashflows
        :type bucketed_cash_flow_request: BucketedCashFlowRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(BucketedCashFlowResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'bucketed_cash_flow_request'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_bucketed_cash_flows" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['bucketed_cash_flow_request']:
            _body_params = _params['bucketed_cash_flow_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "BucketedCashFlowResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/bucketedCashFlows', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_details(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the              scope this uniquely identifies the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the details of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the details of the transaction portfolio. Defaults              to returning the latest version of the details if not specified.")] = None, **kwargs) -> PortfolioDetails:  # noqa: E501
        """GetDetails: Get details  # noqa: E501

        Get certain details associated with a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_details(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the              scope this uniquely identifies the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the details of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the details of the transaction portfolio. Defaults              to returning the latest version of the details if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioDetails
        """
        kwargs['_return_http_data_only'] = True
        return self.get_details_with_http_info(scope, code, effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_details_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the              scope this uniquely identifies the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the details of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the details of the transaction portfolio. Defaults              to returning the latest version of the details if not specified.")] = None, **kwargs):  # noqa: E501
        """GetDetails: Get details  # noqa: E501

        Get certain details associated with a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_details_with_http_info(scope, code, effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the              scope this uniquely identifies the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the details of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the details of the transaction portfolio. Defaults              to returning the latest version of the details if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioDetails, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_details" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioDetails",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/details', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_holdings(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, by_taxlots : Annotated[Optional[StrictBool], Field(description="Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.")] = None, **kwargs) -> VersionedResourceListOfPortfolioHolding:  # noqa: E501
        """GetHoldings: Get holdings  # noqa: E501

        Calculate holdings for a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param by_taxlots: Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.
        :type by_taxlots: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfPortfolioHolding
        """
        kwargs['_return_http_data_only'] = True
        return self.get_holdings_with_http_info(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, **kwargs)  # noqa: E501

    @validate_arguments
    def get_holdings_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, by_taxlots : Annotated[Optional[StrictBool], Field(description="Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.")] = None, **kwargs):  # noqa: E501
        """GetHoldings: Get holdings  # noqa: E501

        Calculate holdings for a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings_with_http_info(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param by_taxlots: Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.
        :type by_taxlots: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfPortfolioHolding, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'property_keys',
            'by_taxlots'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_holdings" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('by_taxlots') is not None:  # noqa: E501
            _query_params.append(('byTaxlots', _params['by_taxlots']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfPortfolioHolding",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdings', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_holdings_adjustment(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(..., description="The effective datetime or cut label of the holdings adjustment.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings adjustment. Defaults to the return the latest              version of the holdings adjustment if not specified.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the ‘Instrument' domain to decorate onto holdings adjustments.              These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name'.              Note that properties from the 'Holding’ domain are automatically returned.")] = None, **kwargs) -> HoldingsAdjustment:  # noqa: E501
        """GetHoldingsAdjustment: Get holdings adjustment  # noqa: E501

        Get a holdings adjustment made to a transaction portfolio at a specific effective datetime. Note that a  holdings adjustment will only be returned if one exists for the specified effective datetime.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings_adjustment(scope, code, effective_at, as_at, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label of the holdings adjustment. (required)
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings adjustment. Defaults to the return the latest              version of the holdings adjustment if not specified.
        :type as_at: datetime
        :param property_keys: A list of property keys from the ‘Instrument' domain to decorate onto holdings adjustments.              These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name'.              Note that properties from the 'Holding’ domain are automatically returned.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: HoldingsAdjustment
        """
        kwargs['_return_http_data_only'] = True
        return self.get_holdings_adjustment_with_http_info(scope, code, effective_at, as_at, property_keys, **kwargs)  # noqa: E501

    @validate_arguments
    def get_holdings_adjustment_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(..., description="The effective datetime or cut label of the holdings adjustment.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings adjustment. Defaults to the return the latest              version of the holdings adjustment if not specified.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the ‘Instrument' domain to decorate onto holdings adjustments.              These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name'.              Note that properties from the 'Holding’ domain are automatically returned.")] = None, **kwargs):  # noqa: E501
        """GetHoldingsAdjustment: Get holdings adjustment  # noqa: E501

        Get a holdings adjustment made to a transaction portfolio at a specific effective datetime. Note that a  holdings adjustment will only be returned if one exists for the specified effective datetime.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings_adjustment_with_http_info(scope, code, effective_at, as_at, property_keys, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label of the holdings adjustment. (required)
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings adjustment. Defaults to the return the latest              version of the holdings adjustment if not specified.
        :type as_at: datetime
        :param property_keys: A list of property keys from the ‘Instrument' domain to decorate onto holdings adjustments.              These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name'.              Note that properties from the 'Holding’ domain are automatically returned.
        :type property_keys: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(HoldingsAdjustment, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'property_keys'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_holdings_adjustment" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['effective_at']:
            _path_params['effectiveAt'] = _params['effective_at']

        # process the query parameters
        _query_params = []
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "HoldingsAdjustment",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdingsadjustments/{effectiveAt}', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_holdings_with_orders(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version of the holdings if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, by_taxlots : Annotated[Optional[StrictBool], Field(description="Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, **kwargs) -> VersionedResourceListWithWarningsOfPortfolioHolding:  # noqa: E501
        """[EXPERIMENTAL] GetHoldingsWithOrders: Get holdings with orders  # noqa: E501

        Get the holdings of a transaction portfolio. Create virtual holdings for any outstanding orders,  and account for order state/fulfillment; that is, treat outstanding orders (and related records) as  if they had been realised at moment of query.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings_with_orders(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version of the holdings if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param by_taxlots: Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.
        :type by_taxlots: bool
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListWithWarningsOfPortfolioHolding
        """
        kwargs['_return_http_data_only'] = True
        return self.get_holdings_with_orders_with_http_info(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, recipe_id_scope, recipe_id_code, **kwargs)  # noqa: E501

    @validate_arguments
    def get_holdings_with_orders_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version of the holdings if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, by_taxlots : Annotated[Optional[StrictBool], Field(description="Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeId")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] GetHoldingsWithOrders: Get holdings with orders  # noqa: E501

        Get the holdings of a transaction portfolio. Create virtual holdings for any outstanding orders,  and account for order state/fulfillment; that is, treat outstanding orders (and related records) as  if they had been realised at moment of query.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_holdings_with_orders_with_http_info(scope, code, effective_at, as_at, filter, property_keys, by_taxlots, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version of the holdings if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              the holdings. These take the format {domain}/{scope}/{code} e.g. \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param by_taxlots: Whether or not to expand the holdings to return the underlying tax-lots. Defaults to              False.
        :type by_taxlots: bool
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeId
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListWithWarningsOfPortfolioHolding, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'property_keys',
            'by_taxlots',
            'recipe_id_scope',
            'recipe_id_code'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_holdings_with_orders" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('by_taxlots') is not None:  # noqa: E501
            _query_params.append(('byTaxlots', _params['by_taxlots']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListWithWarningsOfPortfolioHolding",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdingsWithOrders', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_cash_flows(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.")] = None, window_start : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. it is the minimum date.")] = None, window_end : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the data. Defaults to returning the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs) -> ResourceListOfInstrumentCashFlow:  # noqa: E501
        """[BETA] GetPortfolioCashFlows: Get portfolio cash flows  # noqa: E501

        Get the set of cash flows that occur in a window for the transaction portfolio's instruments.                Note that grouping can affect the quantity of information returned; where a holding is an amalgamation of one or more (e.g. cash) instruments, a unique  transaction identifier will not be available. The same may go for diagnostic information (e.g. multiple sources of an aggregate cash amount on a date that is  not split out. Grouping at the transaction and instrument level is recommended for those seeking to attribute individual flows.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_cash_flows(scope, code, effective_at, window_start, window_end, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.
        :type effective_at: str
        :param window_start: The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. it is the minimum date.
        :type window_start: str
        :param window_end: The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified
        :type window_end: str
        :param as_at: The asAt datetime at which to retrieve the data. Defaults to returning the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfInstrumentCashFlow
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_cash_flows_with_http_info(scope, code, effective_at, window_start, window_end, as_at, filter, recipe_id_scope, recipe_id_code, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_cash_flows_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.")] = None, window_start : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. it is the minimum date.")] = None, window_end : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the data. Defaults to returning the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs):  # noqa: E501
        """[BETA] GetPortfolioCashFlows: Get portfolio cash flows  # noqa: E501

        Get the set of cash flows that occur in a window for the transaction portfolio's instruments.                Note that grouping can affect the quantity of information returned; where a holding is an amalgamation of one or more (e.g. cash) instruments, a unique  transaction identifier will not be available. The same may go for diagnostic information (e.g. multiple sources of an aggregate cash amount on a date that is  not split out. Grouping at the transaction and instrument level is recommended for those seeking to attribute individual flows.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_cash_flows_with_http_info(scope, code, effective_at, window_start, window_end, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.
        :type effective_at: str
        :param window_start: The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. it is the minimum date.
        :type window_start: str
        :param window_end: The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified
        :type window_end: str
        :param as_at: The asAt datetime at which to retrieve the data. Defaults to returning the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfInstrumentCashFlow, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'window_start',
            'window_end',
            'as_at',
            'filter',
            'recipe_id_scope',
            'recipe_id_code'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_cash_flows" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('window_start') is not None:  # noqa: E501
            _query_params.append(('windowStart', _params['window_start']))
        if _params.get('window_end') is not None:  # noqa: E501
            _query_params.append(('windowEnd', _params['window_end']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfInstrumentCashFlow",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/cashflows', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_cash_ladder(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], effective_at : Annotated[StrictStr, Field(..., description="The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs) -> ResourceListOfPortfolioCashLadder:  # noqa: E501
        """GetPortfolioCashLadder: Get portfolio cash ladder  # noqa: E501

        Get a cash ladder for a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_cash_ladder(scope, code, from_effective_at, to_effective_at, effective_at, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param effective_at: The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today. (required)
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPortfolioCashLadder
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_cash_ladder_with_http_info(scope, code, from_effective_at, to_effective_at, effective_at, as_at, filter, recipe_id_scope, recipe_id_code, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_cash_ladder_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], effective_at : Annotated[StrictStr, Field(..., description="The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs):  # noqa: E501
        """GetPortfolioCashLadder: Get portfolio cash ladder  # noqa: E501

        Get a cash ladder for a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_cash_ladder_with_http_info(scope, code, from_effective_at, to_effective_at, effective_at, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param effective_at: The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today. (required)
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPortfolioCashLadder, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_effective_at',
            'to_effective_at',
            'effective_at',
            'as_at',
            'filter',
            'recipe_id_scope',
            'recipe_id_code'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_cash_ladder" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPortfolioCashLadder",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/cashladder', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_portfolio_cash_statement(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs) -> ResourceListOfPortfolioCashFlow:  # noqa: E501
        """GetPortfolioCashStatement: Get portfolio cash statement  # noqa: E501

        Get a cash statement for a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_cash_statement(scope, code, from_effective_at, to_effective_at, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfPortfolioCashFlow
        """
        kwargs['_return_http_data_only'] = True
        return self.get_portfolio_cash_statement_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, filter, recipe_id_scope, recipe_id_code, **kwargs)  # noqa: E501

    @validate_arguments
    def get_portfolio_cash_statement_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio.")], from_effective_at : Annotated[StrictStr, Field(..., description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")], to_effective_at : Annotated[StrictStr, Field(..., description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs):  # noqa: E501
        """GetPortfolioCashStatement: Get portfolio cash statement  # noqa: E501

        Get a cash statement for a transaction portfolio.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_portfolio_cash_statement_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this              uniquely identifies the portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified. (required)
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no upper bound if this is not specified. (required)
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to returning the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfPortfolioCashFlow, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_effective_at',
            'to_effective_at',
            'as_at',
            'filter',
            'recipe_id_scope',
            'recipe_id_code'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_portfolio_cash_statement" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfPortfolioCashFlow",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/cashstatement', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_transaction_history(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_id : Annotated[Optional[StrictStr], Field(..., description="The unique ID of the transaction to create or update.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the history of the transaction. Defaults              to return the latest version if not specified.")] = None, **kwargs) -> ResourceListOfChangeHistory:  # noqa: E501
        """[EARLY ACCESS] GetTransactionHistory: Get the history of a transaction  # noqa: E501

        Get all of the changes that have happened to a transaction.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_transaction_history(scope, code, transaction_id, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_id: The unique ID of the transaction to create or update. (required)
        :type transaction_id: str
        :param as_at: The asAt datetime at which to retrieve the history of the transaction. Defaults              to return the latest version if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfChangeHistory
        """
        kwargs['_return_http_data_only'] = True
        return self.get_transaction_history_with_http_info(scope, code, transaction_id, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def get_transaction_history_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_id : Annotated[Optional[StrictStr], Field(..., description="The unique ID of the transaction to create or update.")], as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the history of the transaction. Defaults              to return the latest version if not specified.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] GetTransactionHistory: Get the history of a transaction  # noqa: E501

        Get all of the changes that have happened to a transaction.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_transaction_history_with_http_info(scope, code, transaction_id, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_id: The unique ID of the transaction to create or update. (required)
        :type transaction_id: str
        :param as_at: The asAt datetime at which to retrieve the history of the transaction. Defaults              to return the latest version if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfChangeHistory, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'transaction_id',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_transaction_history" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['transaction_id']:
            _path_params['transactionId'] = _params['transaction_id']

        # process the query parameters
        _query_params = []
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfChangeHistory",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions/{transactionId}/history', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_transactions(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies               the transaction portfolio.")], from_transaction_date : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no lower bound if this is not specified.")] = None, to_transaction_date : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve transactions. Defaults to returning the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression with which to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto               transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or               'Transaction/strategy/quantsignal'.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to GetTransactions.")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. The current behaviour is               to return all transactions if possible, but this will change to defaulting to 1000 if not specified in the future. It is recommended               to populate this field to enable pagination.")] = None, show_cancelled_transactions : Annotated[Optional[StrictBool], Field(description="Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.")] = None, **kwargs) -> VersionedResourceListOfTransaction:  # noqa: E501
        """GetTransactions: Get transactions  # noqa: E501

        Retrieve all the transactions that occurred during a particular time interval.                If the portfolio is a derived transaction portfolio, the transactions returned are the  union set of all transactions of the parent (and any grandparents, etc.) as well as  those of the derived transaction portfolio itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_transactions(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, page, limit, show_cancelled_transactions, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies               the transaction portfolio. (required)
        :type code: str
        :param from_transaction_date: The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no lower bound if this is not specified.
        :type from_transaction_date: str
        :param to_transaction_date: The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.
        :type to_transaction_date: str
        :param as_at: The asAt datetime at which to retrieve transactions. Defaults to returning the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression with which to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto               transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or               'Transaction/strategy/quantsignal'.
        :type property_keys: List[str]
        :param page: The pagination token to use to continue listing transactions from a previous call to GetTransactions.
        :type page: str
        :param limit: When paginating, limit the number of returned results to this many. The current behaviour is               to return all transactions if possible, but this will change to defaulting to 1000 if not specified in the future. It is recommended               to populate this field to enable pagination.
        :type limit: int
        :param show_cancelled_transactions: Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.
        :type show_cancelled_transactions: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfTransaction
        """
        kwargs['_return_http_data_only'] = True
        return self.get_transactions_with_http_info(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, page, limit, show_cancelled_transactions, **kwargs)  # noqa: E501

    @validate_arguments
    def get_transactions_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies               the transaction portfolio.")], from_transaction_date : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no lower bound if this is not specified.")] = None, to_transaction_date : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve transactions. Defaults to returning the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression with which to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto               transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or               'Transaction/strategy/quantsignal'.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to GetTransactions.")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. The current behaviour is               to return all transactions if possible, but this will change to defaulting to 1000 if not specified in the future. It is recommended               to populate this field to enable pagination.")] = None, show_cancelled_transactions : Annotated[Optional[StrictBool], Field(description="Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.")] = None, **kwargs):  # noqa: E501
        """GetTransactions: Get transactions  # noqa: E501

        Retrieve all the transactions that occurred during a particular time interval.                If the portfolio is a derived transaction portfolio, the transactions returned are the  union set of all transactions of the parent (and any grandparents, etc.) as well as  those of the derived transaction portfolio itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_transactions_with_http_info(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, page, limit, show_cancelled_transactions, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies               the transaction portfolio. (required)
        :type code: str
        :param from_transaction_date: The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no lower bound if this is not specified.
        :type from_transaction_date: str
        :param to_transaction_date: The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.               There is no upper bound if this is not specified.
        :type to_transaction_date: str
        :param as_at: The asAt datetime at which to retrieve transactions. Defaults to returning the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression with which to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto               transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or               'Transaction/strategy/quantsignal'.
        :type property_keys: List[str]
        :param page: The pagination token to use to continue listing transactions from a previous call to GetTransactions.
        :type page: str
        :param limit: When paginating, limit the number of returned results to this many. The current behaviour is               to return all transactions if possible, but this will change to defaulting to 1000 if not specified in the future. It is recommended               to populate this field to enable pagination.
        :type limit: int
        :param show_cancelled_transactions: Option to specify whether or not to include cancelled transactions,               including previous versions of transactions which have since been amended.               Defaults to False if not specified.
        :type show_cancelled_transactions: bool
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfTransaction, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_transaction_date',
            'to_transaction_date',
            'as_at',
            'filter',
            'property_keys',
            'page',
            'limit',
            'show_cancelled_transactions'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_transactions" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_transaction_date') is not None:  # noqa: E501
            _query_params.append(('fromTransactionDate', _params['from_transaction_date']))
        if _params.get('to_transaction_date') is not None:  # noqa: E501
            _query_params.append(('toTransactionDate', _params['to_transaction_date']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))
        if _params.get('show_cancelled_transactions') is not None:  # noqa: E501
            _query_params.append(('showCancelledTransactions', _params['show_cancelled_transactions']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfTransaction",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def get_upsertable_portfolio_cash_flows(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.")] = None, window_start : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. uses minimum date-time")] = None, window_end : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs) -> ResourceListOfTransaction:  # noqa: E501
        """[BETA] GetUpsertablePortfolioCashFlows: Get upsertable portfolio cash flows.  # noqa: E501

        Get the set of cash flows that occur in a window for the given portfolio instruments as a set of upsertable transactions (DTOs).                Note that grouping can affect the quantity of information returned; where a holding is an amalgamation of one or more (e.g. cash) instruments, a unique  transaction identifier will not be available. The same may go for diagnostic information (e.g. multiple sources of an aggregate cash amount on a date that is  not split out. Grouping at the transaction and instrument level is recommended for those seeking to attribute individual flows.                In essence this is identical to the 'GetCashFlows' endpoint but returns the cash flows as a set of transactions suitable for directly putting back into LUSID.  There are a couple of important points:  (1) Internally it can not be fully known where the user wishes to insert these transactions, e.g. portfolio and movement type.      These are therefore defaulted to a sensible option; the user will likely need to change these.  (2) Similarly, knowledge of any properties the user might wish to add to a transaction are unknown and consequently left empty.  (3) The transaction id that is added is simply a concatenation of the original transaction id, instrument id and payment date and direction. The user can happily override this.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_upsertable_portfolio_cash_flows(scope, code, effective_at, window_start, window_end, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.
        :type effective_at: str
        :param window_start: The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. uses minimum date-time
        :type window_start: str
        :param window_end: The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified
        :type window_end: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfTransaction
        """
        kwargs['_return_http_data_only'] = True
        return self.get_upsertable_portfolio_cash_flows_with_http_info(scope, code, effective_at, window_start, window_end, as_at, filter, recipe_id_scope, recipe_id_code, **kwargs)  # noqa: E501

    @validate_arguments
    def get_upsertable_portfolio_cash_flows_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.")] = None, window_start : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. uses minimum date-time")] = None, window_end : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version               of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, recipe_id_scope : Annotated[Optional[StrictStr], Field(description="The scope of the given recipeId")] = None, recipe_id_code : Annotated[Optional[StrictStr], Field(description="The code of the given recipeID")] = None, **kwargs):  # noqa: E501
        """[BETA] GetUpsertablePortfolioCashFlows: Get upsertable portfolio cash flows.  # noqa: E501

        Get the set of cash flows that occur in a window for the given portfolio instruments as a set of upsertable transactions (DTOs).                Note that grouping can affect the quantity of information returned; where a holding is an amalgamation of one or more (e.g. cash) instruments, a unique  transaction identifier will not be available. The same may go for diagnostic information (e.g. multiple sources of an aggregate cash amount on a date that is  not split out. Grouping at the transaction and instrument level is recommended for those seeking to attribute individual flows.                In essence this is identical to the 'GetCashFlows' endpoint but returns the cash flows as a set of transactions suitable for directly putting back into LUSID.  There are a couple of important points:  (1) Internally it can not be fully known where the user wishes to insert these transactions, e.g. portfolio and movement type.      These are therefore defaulted to a sensible option; the user will likely need to change these.  (2) Similarly, knowledge of any properties the user might wish to add to a transaction are unknown and consequently left empty.  (3) The transaction id that is added is simply a concatenation of the original transaction id, instrument id and payment date and direction. The user can happily override this.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_upsertable_portfolio_cash_flows_with_http_info(scope, code, effective_at, window_start, window_end, as_at, filter, recipe_id_scope, recipe_id_code, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this               uniquely identifies the portfolio. (required)
        :type code: str
        :param effective_at: The valuation (pricing) effective datetime or cut label (inclusive) at which to evaluate the cashflows.  This determines whether cashflows are evaluated in a historic or forward looking context and will, for certain models, affect where data is looked up.  For example, on a swap if the effectiveAt is in the middle of the window, cashflows before it will be historic and resets assumed to exist where if the effectiveAt  is before the start of the range they are forward looking and will be expectations assuming the model supports that.  There is evidently a presumption here about availability of data and that the effectiveAt is realistically on or before the real-world today.
        :type effective_at: str
        :param window_start: The lower bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               There is no lower bound if this is not specified. i.e. uses minimum date-time
        :type window_start: str
        :param window_end: The upper bound effective datetime or cut label (inclusive) from which to retrieve the cashflows.               The upper bound defaults to 'max date' if it is not specified
        :type window_end: str
        :param as_at: The asAt datetime at which to retrieve the portfolio. Defaults to return the latest version               of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.               For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\".               For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param recipe_id_scope: The scope of the given recipeId
        :type recipe_id_scope: str
        :param recipe_id_code: The code of the given recipeID
        :type recipe_id_code: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfTransaction, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'window_start',
            'window_end',
            'as_at',
            'filter',
            'recipe_id_scope',
            'recipe_id_code'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_upsertable_portfolio_cash_flows" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('window_start') is not None:  # noqa: E501
            _query_params.append(('windowStart', _params['window_start']))
        if _params.get('window_end') is not None:  # noqa: E501
            _query_params.append(('windowEnd', _params['window_end']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('recipe_id_scope') is not None:  # noqa: E501
            _query_params.append(('recipeIdScope', _params['recipe_id_scope']))
        if _params.get('recipe_id_code') is not None:  # noqa: E501
            _query_params.append(('recipeIdCode', _params['recipe_id_code']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfTransaction",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/upsertablecashflows', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def list_holdings_adjustments(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no lower bound if this is not specified.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings adjustments. Defaults to return the              latest version of each holding adjustment if not specified.")] = None, **kwargs) -> ResourceListOfHoldingsAdjustmentHeader:  # noqa: E501
        """ListHoldingsAdjustments: List holdings adjustments  # noqa: E501

        List the holdings adjustments made to the specified transaction portfolio over a specified interval of effective time.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_holdings_adjustments(scope, code, from_effective_at, to_effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no lower bound if this is not specified.
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no upper bound if this is not specified.
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings adjustments. Defaults to return the              latest version of each holding adjustment if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: ResourceListOfHoldingsAdjustmentHeader
        """
        kwargs['_return_http_data_only'] = True
        return self.list_holdings_adjustments_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, **kwargs)  # noqa: E501

    @validate_arguments
    def list_holdings_adjustments_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no lower bound if this is not specified.")] = None, to_effective_at : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings adjustments. Defaults to return the              latest version of each holding adjustment if not specified.")] = None, **kwargs):  # noqa: E501
        """ListHoldingsAdjustments: List holdings adjustments  # noqa: E501

        List the holdings adjustments made to the specified transaction portfolio over a specified interval of effective time.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.list_holdings_adjustments_with_http_info(scope, code, from_effective_at, to_effective_at, as_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no lower bound if this is not specified.
        :type from_effective_at: str
        :param to_effective_at: The upper bound effective datetime or cut label (inclusive) from which to retrieve the holdings              adjustments. There is no upper bound if this is not specified.
        :type to_effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings adjustments. Defaults to return the              latest version of each holding adjustment if not specified.
        :type as_at: datetime
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(ResourceListOfHoldingsAdjustmentHeader, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_effective_at',
            'to_effective_at',
            'as_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_holdings_adjustments" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('to_effective_at') is not None:  # noqa: E501
            _query_params.append(('toEffectiveAt', _params['to_effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "ResourceListOfHoldingsAdjustmentHeader",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdingsadjustments', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def look_through_holdings(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, quotes_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The scope containing the quotes with the FX rates used for currency conversion.")] = None, slice : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, define this slice as the root slice in the portfolio to look through from.")] = None, share_class : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.")] = None, **kwargs) -> VersionedResourceListOfPortfolioHolding:  # noqa: E501
        """[EXPERIMENTAL] LookThroughHoldings: Get LookThrough Holdings  # noqa: E501

        Calculate holdings for a transaction portfolio with lookthrough  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.look_through_holdings(scope, code, effective_at, as_at, filter, property_keys, quotes_scope, slice, share_class, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param quotes_scope: The scope containing the quotes with the FX rates used for currency conversion.
        :type quotes_scope: str
        :param slice: When running LookThrough, define this slice as the root slice in the portfolio to look through from.
        :type slice: str
        :param share_class: When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.
        :type share_class: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfPortfolioHolding
        """
        kwargs['_return_http_data_only'] = True
        return self.look_through_holdings_with_http_info(scope, code, effective_at, as_at, filter, property_keys, quotes_scope, slice, share_class, **kwargs)  # noqa: E501

    @validate_arguments
    def look_through_holdings_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".")] = None, quotes_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The scope containing the quotes with the FX rates used for currency conversion.")] = None, slice : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, define this slice as the root slice in the portfolio to look through from.")] = None, share_class : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] LookThroughHoldings: Get LookThrough Holdings  # noqa: E501

        Calculate holdings for a transaction portfolio with lookthrough  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.look_through_holdings_with_http_info(scope, code, effective_at, as_at, filter, property_keys, quotes_scope, slice, share_class, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which to retrieve the holdings of the transaction              portfolio. Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param as_at: The asAt datetime at which to retrieve the holdings of the transaction portfolio. Defaults              to return the latest version if not specified.
        :type as_at: datetime
        :param filter: Expression to filter the result set.              For example, to filter on the Holding Type, use \"holdingType eq 'p'\".              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the \"Instrument\" or \"Holding\" domain to decorate onto              holdings. These must have the format {domain}/{scope}/{code}, for example \"Instrument/system/Name\" or \"Holding/system/Cost\".
        :type property_keys: List[str]
        :param quotes_scope: The scope containing the quotes with the FX rates used for currency conversion.
        :type quotes_scope: str
        :param slice: When running LookThrough, define this slice as the root slice in the portfolio to look through from.
        :type slice: str
        :param share_class: When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.
        :type share_class: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfPortfolioHolding, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'as_at',
            'filter',
            'property_keys',
            'quotes_scope',
            'slice',
            'share_class'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method look_through_holdings" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('quotes_scope') is not None:  # noqa: E501
            _query_params.append(('quotesScope', _params['quotes_scope']))
        if _params.get('slice') is not None:  # noqa: E501
            _query_params.append(('slice', _params['slice']))
        if _params.get('share_class') is not None:  # noqa: E501
            _query_params.append(('shareClass', _params['share_class']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfPortfolioHolding",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdings/$lookthrough', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def look_through_transactions(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], from_transaction_date : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no lower bound if this is not specified.")] = None, to_transaction_date : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve transactions. Defaults to returning the latest version              of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression with which to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto              transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or              'Transaction/strategy/quantsignal'.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to LookThroughTransactions.")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. This will default to 1000 if not specified.")] = None, quotes_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The scope containing the quotes with the FX rates used for currency conversion.")] = None, slice : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, define this slice as the root slice in the portfolio to look through from.")] = None, share_class : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.")] = None, **kwargs) -> VersionedResourceListOfTransaction:  # noqa: E501
        """[EXPERIMENTAL] LookThroughTransactions: Look through transactions  # noqa: E501

        Retrieve all the transactions that occurred during a particular time interval.    If the portfolio is part of a fund as defined in Fund Accounting documents, the transactions returned are the  union set of all transactions in portfolios of the same type, in any funds invested in by the portfolio's fund  (and any funds invested in from that fund, etc.).  The transactions will be scaled according to the ratio of the value of the investment in the fund to the NAV of the fund itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.look_through_transactions(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, page, limit, quotes_scope, slice, share_class, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param from_transaction_date: The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no lower bound if this is not specified.
        :type from_transaction_date: str
        :param to_transaction_date: The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no upper bound if this is not specified.
        :type to_transaction_date: str
        :param as_at: The asAt datetime at which to retrieve transactions. Defaults to returning the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression with which to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto              transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or              'Transaction/strategy/quantsignal'.
        :type property_keys: List[str]
        :param page: The pagination token to use to continue listing transactions from a previous call to LookThroughTransactions.
        :type page: str
        :param limit: When paginating, limit the number of returned results to this many. This will default to 1000 if not specified.
        :type limit: int
        :param quotes_scope: The scope containing the quotes with the FX rates used for currency conversion.
        :type quotes_scope: str
        :param slice: When running LookThrough, define this slice as the root slice in the portfolio to look through from.
        :type slice: str
        :param share_class: When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.
        :type share_class: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: VersionedResourceListOfTransaction
        """
        kwargs['_return_http_data_only'] = True
        return self.look_through_transactions_with_http_info(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, page, limit, quotes_scope, slice, share_class, **kwargs)  # noqa: E501

    @validate_arguments
    def look_through_transactions_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], from_transaction_date : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no lower bound if this is not specified.")] = None, to_transaction_date : Annotated[Optional[StrictStr], Field(description="The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no upper bound if this is not specified.")] = None, as_at : Annotated[Optional[datetime], Field(description="The asAt datetime at which to retrieve transactions. Defaults to returning the latest version              of each transaction if not specified.")] = None, filter : Annotated[Optional[constr(strict=True, max_length=16384, min_length=0)], Field(description="Expression with which to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.")] = None, property_keys : Annotated[Optional[List[StrictStr]], Field(description="A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto              transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or              'Transaction/strategy/quantsignal'.")] = None, page : Annotated[Optional[constr(strict=True, max_length=500, min_length=1)], Field(description="The pagination token to use to continue listing transactions from a previous call to LookThroughTransactions.")] = None, limit : Annotated[Optional[StrictInt], Field(description="When paginating, limit the number of returned results to this many. This will default to 1000 if not specified.")] = None, quotes_scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="The scope containing the quotes with the FX rates used for currency conversion.")] = None, slice : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, define this slice as the root slice in the portfolio to look through from.")] = None, share_class : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(description="When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] LookThroughTransactions: Look through transactions  # noqa: E501

        Retrieve all the transactions that occurred during a particular time interval.    If the portfolio is part of a fund as defined in Fund Accounting documents, the transactions returned are the  union set of all transactions in portfolios of the same type, in any funds invested in by the portfolio's fund  (and any funds invested in from that fund, etc.).  The transactions will be scaled according to the ratio of the value of the investment in the fund to the NAV of the fund itself.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.look_through_transactions_with_http_info(scope, code, from_transaction_date, to_transaction_date, as_at, filter, property_keys, page, limit, quotes_scope, slice, share_class, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param from_transaction_date: The lower bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no lower bound if this is not specified.
        :type from_transaction_date: str
        :param to_transaction_date: The upper bound effective datetime or cut label (inclusive) from which to retrieve transactions.              There is no upper bound if this is not specified.
        :type to_transaction_date: str
        :param as_at: The asAt datetime at which to retrieve transactions. Defaults to returning the latest version              of each transaction if not specified.
        :type as_at: datetime
        :param filter: Expression with which to filter the result set.              For example, to return only transactions with a transaction type of 'Buy', specify \"type eq 'Buy'\"              For more information about filtering LUSID results, see https://support.lusid.com/knowledgebase/article/KA-01914.
        :type filter: str
        :param property_keys: A list of property keys from the 'Instrument' or 'Transaction' domain to decorate onto              transactions. These must have the format {domain}/{scope}/{code}, for example 'Instrument/system/Name' or              'Transaction/strategy/quantsignal'.
        :type property_keys: List[str]
        :param page: The pagination token to use to continue listing transactions from a previous call to LookThroughTransactions.
        :type page: str
        :param limit: When paginating, limit the number of returned results to this many. This will default to 1000 if not specified.
        :type limit: int
        :param quotes_scope: The scope containing the quotes with the FX rates used for currency conversion.
        :type quotes_scope: str
        :param slice: When running LookThrough, define this slice as the root slice in the portfolio to look through from.
        :type slice: str
        :param share_class: When running LookThrough, use this along with the slice parameter to specify              the root share class in the slice in the portfolio to look through from. The slice parameter is a prerequisite              for this parameter to be valid.
        :type share_class: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(VersionedResourceListOfTransaction, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'from_transaction_date',
            'to_transaction_date',
            'as_at',
            'filter',
            'property_keys',
            'page',
            'limit',
            'quotes_scope',
            'slice',
            'share_class'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method look_through_transactions" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('from_transaction_date') is not None:  # noqa: E501
            _query_params.append(('fromTransactionDate', _params['from_transaction_date']))
        if _params.get('to_transaction_date') is not None:  # noqa: E501
            _query_params.append(('toTransactionDate', _params['to_transaction_date']))
        if _params.get('as_at') is not None:  # noqa: E501
            _query_params.append(('asAt', _params['as_at']))
        if _params.get('filter') is not None:  # noqa: E501
            _query_params.append(('filter', _params['filter']))
        if _params.get('property_keys') is not None:  # noqa: E501
            _query_params.append(('propertyKeys', _params['property_keys']))
            _collection_formats['propertyKeys'] = 'multi'
        if _params.get('page') is not None:  # noqa: E501
            _query_params.append(('page', _params['page']))
        if _params.get('limit') is not None:  # noqa: E501
            _query_params.append(('limit', _params['limit']))
        if _params.get('quotes_scope') is not None:  # noqa: E501
            _query_params.append(('quotesScope', _params['quotes_scope']))
        if _params.get('slice') is not None:  # noqa: E501
            _query_params.append(('slice', _params['slice']))
        if _params.get('share_class') is not None:  # noqa: E501
            _query_params.append(('shareClass', _params['share_class']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "VersionedResourceListOfTransaction",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions/$lookthrough', 'GET',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def patch_portfolio_details(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio.")], operation : Annotated[List[Operation], Field(..., description="The patch document.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.               Note that this will affect all bitemporal entities in the request, but will not be used for any perpetual entities.")] = None, **kwargs) -> PortfolioDetails:  # noqa: E501
        """[EXPERIMENTAL] PatchPortfolioDetails: Patch portfolio details  # noqa: E501

        Create or update certain details for a particular transaction portfolio.  The behaviour is defined by the JSON Patch specification.                Note that not all elements of a transaction portfolio definition are  modifiable once it has been created due to the potential implications for data already stored.  Currently supported properties are: SubHoldingKeys, BaseCurrency  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio_details(scope, code, operation, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio. (required)
        :type code: str
        :param operation: The patch document. (required)
        :type operation: List[Operation]
        :param effective_at: The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.               Note that this will affect all bitemporal entities in the request, but will not be used for any perpetual entities.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioDetails
        """
        kwargs['_return_http_data_only'] = True
        return self.patch_portfolio_details_with_http_info(scope, code, operation, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def patch_portfolio_details_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio.")], operation : Annotated[List[Operation], Field(..., description="The patch document.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.               Note that this will affect all bitemporal entities in the request, but will not be used for any perpetual entities.")] = None, **kwargs):  # noqa: E501
        """[EXPERIMENTAL] PatchPortfolioDetails: Patch portfolio details  # noqa: E501

        Create or update certain details for a particular transaction portfolio.  The behaviour is defined by the JSON Patch specification.                Note that not all elements of a transaction portfolio definition are  modifiable once it has been created due to the potential implications for data already stored.  Currently supported properties are: SubHoldingKeys, BaseCurrency  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.patch_portfolio_details_with_http_info(scope, code, operation, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio. (required)
        :type code: str
        :param operation: The patch document. (required)
        :type operation: List[Operation]
        :param effective_at: The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.               Note that this will affect all bitemporal entities in the request, but will not be used for any perpetual entities.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioDetails, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'operation',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_portfolio_details" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['operation']:
            _body_params = _params['operation']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioDetails",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/details', 'PATCH',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def resolve_instrument(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], instrument_identifier_type : Annotated[StrictStr, Field(..., description="The instrument identifier type.")], instrument_identifier_value : Annotated[StrictStr, Field(..., description="The value for the given instrument identifier.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")] = None, re_resolve : Annotated[Optional[StrictBool], Field(description="When set to true, instrument resolution will be attempted for all transactions and holdings for the given identifier and date range.              When set to false (default behaviour), instrument resolution will only be attempted for those transactions and holdings that were previously unresolved.")] = None, request_body : Annotated[Optional[Dict[str, StrictStr]], Field(description="The dictionary with the instrument identifiers to be updated on the             transaction and holdings.")] = None, **kwargs) -> UpsertPortfolioTransactionsResponse:  # noqa: E501
        """[EARLY ACCESS] ResolveInstrument: Resolve instrument  # noqa: E501

        Try to resolve the instrument for transaction and holdings for a given instrument identifier and a specified  period of time. Also update the instrument identifiers with the given instrument identifiers collection.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.resolve_instrument(scope, code, instrument_identifier_type, instrument_identifier_value, from_effective_at, re_resolve, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param instrument_identifier_type: The instrument identifier type. (required)
        :type instrument_identifier_type: str
        :param instrument_identifier_value: The value for the given instrument identifier. (required)
        :type instrument_identifier_value: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.
        :type from_effective_at: str
        :param re_resolve: When set to true, instrument resolution will be attempted for all transactions and holdings for the given identifier and date range.              When set to false (default behaviour), instrument resolution will only be attempted for those transactions and holdings that were previously unresolved.
        :type re_resolve: bool
        :param request_body: The dictionary with the instrument identifiers to be updated on the             transaction and holdings.
        :type request_body: Dict[str, str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: UpsertPortfolioTransactionsResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.resolve_instrument_with_http_info(scope, code, instrument_identifier_type, instrument_identifier_value, from_effective_at, re_resolve, request_body, **kwargs)  # noqa: E501

    @validate_arguments
    def resolve_instrument_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], instrument_identifier_type : Annotated[StrictStr, Field(..., description="The instrument identifier type.")], instrument_identifier_value : Annotated[StrictStr, Field(..., description="The value for the given instrument identifier.")], from_effective_at : Annotated[Optional[StrictStr], Field(description="The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.")] = None, re_resolve : Annotated[Optional[StrictBool], Field(description="When set to true, instrument resolution will be attempted for all transactions and holdings for the given identifier and date range.              When set to false (default behaviour), instrument resolution will only be attempted for those transactions and holdings that were previously unresolved.")] = None, request_body : Annotated[Optional[Dict[str, StrictStr]], Field(description="The dictionary with the instrument identifiers to be updated on the             transaction and holdings.")] = None, **kwargs):  # noqa: E501
        """[EARLY ACCESS] ResolveInstrument: Resolve instrument  # noqa: E501

        Try to resolve the instrument for transaction and holdings for a given instrument identifier and a specified  period of time. Also update the instrument identifiers with the given instrument identifiers collection.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.resolve_instrument_with_http_info(scope, code, instrument_identifier_type, instrument_identifier_value, from_effective_at, re_resolve, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param instrument_identifier_type: The instrument identifier type. (required)
        :type instrument_identifier_type: str
        :param instrument_identifier_value: The value for the given instrument identifier. (required)
        :type instrument_identifier_value: str
        :param from_effective_at: The lower bound effective datetime or cut label (inclusive) from which to retrieve the data.              There is no lower bound if this is not specified.
        :type from_effective_at: str
        :param re_resolve: When set to true, instrument resolution will be attempted for all transactions and holdings for the given identifier and date range.              When set to false (default behaviour), instrument resolution will only be attempted for those transactions and holdings that were previously unresolved.
        :type re_resolve: bool
        :param request_body: The dictionary with the instrument identifiers to be updated on the             transaction and holdings.
        :type request_body: Dict[str, str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(UpsertPortfolioTransactionsResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'instrument_identifier_type',
            'instrument_identifier_value',
            'from_effective_at',
            're_resolve',
            'request_body'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method resolve_instrument" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('instrument_identifier_type') is not None:  # noqa: E501
            _query_params.append(('instrumentIdentifierType', _params['instrument_identifier_type']))
        if _params.get('instrument_identifier_value') is not None:  # noqa: E501
            _query_params.append(('instrumentIdentifierValue', _params['instrument_identifier_value']))
        if _params.get('from_effective_at') is not None:  # noqa: E501
            _query_params.append(('fromEffectiveAt', _params['from_effective_at']))
        if _params.get('re_resolve') is not None:  # noqa: E501
            _query_params.append(('reResolve', _params['re_resolve']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['request_body']:
            _body_params = _params['request_body']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "UpsertPortfolioTransactionsResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/$resolve', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def set_holdings(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[StrictStr, Field(..., description="The effective datetime or cut label at which the holdings should be set to the provided targets.")], adjust_holding_request : Annotated[List[AdjustHoldingRequest], Field(..., description="The complete set of target holdings for the transaction portfolio.")], reconciliation_methods : Annotated[Optional[List[StrictStr]], Field(description="Optional parameter for specifying a reconciliation method: e.g. FxForward.")] = None, **kwargs) -> AdjustHolding:  # noqa: E501
        """SetHoldings: Set holdings  # noqa: E501

        Set the holdings of the specified transaction portfolio to the provided targets. LUSID will automatically  construct adjustment transactions to ensure that the entire set of holdings for the transaction portfolio  are always set to the provided targets for the specified effective datetime. Read more about the difference between  adjusting and setting holdings here https://support.lusid.com/how-do-i-adjust-my-holdings.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.set_holdings(scope, code, effective_at, adjust_holding_request, reconciliation_methods, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which the holdings should be set to the provided targets. (required)
        :type effective_at: str
        :param adjust_holding_request: The complete set of target holdings for the transaction portfolio. (required)
        :type adjust_holding_request: List[AdjustHoldingRequest]
        :param reconciliation_methods: Optional parameter for specifying a reconciliation method: e.g. FxForward.
        :type reconciliation_methods: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: AdjustHolding
        """
        kwargs['_return_http_data_only'] = True
        return self.set_holdings_with_http_info(scope, code, effective_at, adjust_holding_request, reconciliation_methods, **kwargs)  # noqa: E501

    @validate_arguments
    def set_holdings_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], effective_at : Annotated[StrictStr, Field(..., description="The effective datetime or cut label at which the holdings should be set to the provided targets.")], adjust_holding_request : Annotated[List[AdjustHoldingRequest], Field(..., description="The complete set of target holdings for the transaction portfolio.")], reconciliation_methods : Annotated[Optional[List[StrictStr]], Field(description="Optional parameter for specifying a reconciliation method: e.g. FxForward.")] = None, **kwargs):  # noqa: E501
        """SetHoldings: Set holdings  # noqa: E501

        Set the holdings of the specified transaction portfolio to the provided targets. LUSID will automatically  construct adjustment transactions to ensure that the entire set of holdings for the transaction portfolio  are always set to the provided targets for the specified effective datetime. Read more about the difference between  adjusting and setting holdings here https://support.lusid.com/how-do-i-adjust-my-holdings.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.set_holdings_with_http_info(scope, code, effective_at, adjust_holding_request, reconciliation_methods, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param effective_at: The effective datetime or cut label at which the holdings should be set to the provided targets. (required)
        :type effective_at: str
        :param adjust_holding_request: The complete set of target holdings for the transaction portfolio. (required)
        :type adjust_holding_request: List[AdjustHoldingRequest]
        :param reconciliation_methods: Optional parameter for specifying a reconciliation method: e.g. FxForward.
        :type reconciliation_methods: List[str]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(AdjustHolding, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'effective_at',
            'adjust_holding_request',
            'reconciliation_methods'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method set_holdings" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))
        if _params.get('reconciliation_methods') is not None:  # noqa: E501
            _query_params.append(('reconciliationMethods', _params['reconciliation_methods']))
            _collection_formats['reconciliationMethods'] = 'multi'

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['adjust_holding_request']:
            _body_params = _params['adjust_holding_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "AdjustHolding",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/holdings', 'PUT',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_portfolio_details(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio.")], create_portfolio_details : Annotated[CreatePortfolioDetails, Field(..., description="The details to create or update for the specified transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.")] = None, **kwargs) -> PortfolioDetails:  # noqa: E501
        """UpsertPortfolioDetails: Upsert portfolio details  # noqa: E501

        Create or update certain details for a particular transaction portfolio. The details are updated if they already exist, and inserted if they do not.                Note that not all elements of a transaction portfolio definition are  modifiable once it has been created due to the potential implications for data already stored.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_details(scope, code, create_portfolio_details, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio. (required)
        :type code: str
        :param create_portfolio_details: The details to create or update for the specified transaction portfolio. (required)
        :type create_portfolio_details: CreatePortfolioDetails
        :param effective_at: The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: PortfolioDetails
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_portfolio_details_with_http_info(scope, code, create_portfolio_details, effective_at, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_portfolio_details_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio.")], create_portfolio_details : Annotated[CreatePortfolioDetails, Field(..., description="The details to create or update for the specified transaction portfolio.")], effective_at : Annotated[Optional[StrictStr], Field(description="The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.")] = None, **kwargs):  # noqa: E501
        """UpsertPortfolioDetails: Upsert portfolio details  # noqa: E501

        Create or update certain details for a particular transaction portfolio. The details are updated if they already exist, and inserted if they do not.                Note that not all elements of a transaction portfolio definition are  modifiable once it has been created due to the potential implications for data already stored.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_portfolio_details_with_http_info(scope, code, create_portfolio_details, effective_at, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the               scope this uniquely identifies the transaction portfolio. (required)
        :type code: str
        :param create_portfolio_details: The details to create or update for the specified transaction portfolio. (required)
        :type create_portfolio_details: CreatePortfolioDetails
        :param effective_at: The effective datetime or cut label at which the updated or inserted details should become valid.               Defaults to the current LUSID system datetime if not specified.
        :type effective_at: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(PortfolioDetails, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'create_portfolio_details',
            'effective_at'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_portfolio_details" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []
        if _params.get('effective_at') is not None:  # noqa: E501
            _query_params.append(('effectiveAt', _params['effective_at']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['create_portfolio_details']:
            _body_params = _params['create_portfolio_details']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "PortfolioDetails",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/details', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_transaction_properties(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_id : Annotated[Optional[StrictStr], Field(..., description="The unique ID of the transaction to create or update properties for.")], request_body : Annotated[Dict[str, PerpetualProperty], Field(..., description="The properties and their associated values to create or update.")], **kwargs) -> UpsertTransactionPropertiesResponse:  # noqa: E501
        """UpsertTransactionProperties: Upsert transaction properties  # noqa: E501

        Create or update one or more transaction properties for a single transaction in the transaction portfolio.  Each property will be updated if it already exists and created if it does not.  Both transaction and portfolio must exist at the time when properties are created or updated.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_transaction_properties(scope, code, transaction_id, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_id: The unique ID of the transaction to create or update properties for. (required)
        :type transaction_id: str
        :param request_body: The properties and their associated values to create or update. (required)
        :type request_body: Dict[str, PerpetualProperty]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: UpsertTransactionPropertiesResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_transaction_properties_with_http_info(scope, code, transaction_id, request_body, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_transaction_properties_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_id : Annotated[Optional[StrictStr], Field(..., description="The unique ID of the transaction to create or update properties for.")], request_body : Annotated[Dict[str, PerpetualProperty], Field(..., description="The properties and their associated values to create or update.")], **kwargs):  # noqa: E501
        """UpsertTransactionProperties: Upsert transaction properties  # noqa: E501

        Create or update one or more transaction properties for a single transaction in the transaction portfolio.  Each property will be updated if it already exists and created if it does not.  Both transaction and portfolio must exist at the time when properties are created or updated.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_transaction_properties_with_http_info(scope, code, transaction_id, request_body, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_id: The unique ID of the transaction to create or update properties for. (required)
        :type transaction_id: str
        :param request_body: The properties and their associated values to create or update. (required)
        :type request_body: Dict[str, PerpetualProperty]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(UpsertTransactionPropertiesResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'transaction_id',
            'request_body'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_transaction_properties" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']
        if _params['transaction_id']:
            _path_params['transactionId'] = _params['transaction_id']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['request_body']:
            _body_params = _params['request_body']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "UpsertTransactionPropertiesResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions/{transactionId}/properties', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def upsert_transactions(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_request : Annotated[List[TransactionRequest], Field(..., description="A list of transactions to be created or updated.")], **kwargs) -> UpsertPortfolioTransactionsResponse:  # noqa: E501
        """UpsertTransactions: Upsert transactions  # noqa: E501

        Create or update transactions in the transaction portfolio. A transaction will be updated  if it already exists and created if it does not.  The maximum number of transactions that this method can upsert per request is 10,000.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_transactions(scope, code, transaction_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_request: A list of transactions to be created or updated. (required)
        :type transaction_request: List[TransactionRequest]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: UpsertPortfolioTransactionsResponse
        """
        kwargs['_return_http_data_only'] = True
        return self.upsert_transactions_with_http_info(scope, code, transaction_request, **kwargs)  # noqa: E501

    @validate_arguments
    def upsert_transactions_with_http_info(self, scope : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The scope of the transaction portfolio.")], code : Annotated[Optional[constr(strict=True, max_length=64, min_length=1)], Field(..., description="The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio.")], transaction_request : Annotated[List[TransactionRequest], Field(..., description="A list of transactions to be created or updated.")], **kwargs):  # noqa: E501
        """UpsertTransactions: Upsert transactions  # noqa: E501

        Create or update transactions in the transaction portfolio. A transaction will be updated  if it already exists and created if it does not.  The maximum number of transactions that this method can upsert per request is 10,000.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.upsert_transactions_with_http_info(scope, code, transaction_request, async_req=True)
        >>> result = thread.get()

        :param scope: The scope of the transaction portfolio. (required)
        :type scope: str
        :param code: The code of the transaction portfolio. Together with the scope this uniquely identifies              the transaction portfolio. (required)
        :type code: str
        :param transaction_request: A list of transactions to be created or updated. (required)
        :type transaction_request: List[TransactionRequest]
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(UpsertPortfolioTransactionsResponse, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'scope',
            'code',
            'transaction_request'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method upsert_transactions" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}
        if _params['scope']:
            _path_params['scope'] = _params['scope']
        if _params['code']:
            _path_params['code'] = _params['code']

        # process the query parameters
        _query_params = []

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['transaction_request']:
            _body_params = _params['transaction_request']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['text/plain', 'application/json', 'text/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json-patch+json', 'application/json', 'text/json', 'application/*+json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['oauth2']  # noqa: E501

        _response_types_map = {
            200: "UpsertPortfolioTransactionsResponse",
            400: "LusidValidationProblemDetails",
        }

        return self.api_client.call_api(
            '/api/transactionportfolios/{scope}/{code}/transactions', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))
